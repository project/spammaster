<?php

namespace Drupal\spammaster;

use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use GuzzleHttp\Client;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class service.
 */
class SpamMasterActionService {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The SpamMasterActionService Service.
   *
   * @var \Drupal\spammaster\SpamMasterActionService
   */
  protected $spama;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, ConfigFactoryInterface $configFactory, StateInterface $state, RequestStack $requestStack, Client $httpClient) {
    $this->connection = $connection;
    $this->configFactory = $configFactory;
    $this->state = $state;
    $this->requestStack = $requestStack;
    $this->httpClient = $httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('request_stack'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterAct($spama) {

    // Get variables.
    $spammaster_settings = $this->configFactory->getEditable('spammaster.settings');
    $spammaster_license = $spammaster_settings->get('spammaster.license_key');
    $spammaster_db_protection_hash = $this->state->get('spammaster.spam_master_db_protection_hash');
    $spammaster_new_option = $this->state->get('spammaster.new_options');
    // Are there new options.
    if ('1' === $spammaster_new_option || '1' === $spama) {
      $spammaster_date = date("Y-m-d H:i:s");
      if (isset($_SERVER['SERVER_ADDR'])) {
        $spammaster_ip = substr($_SERVER['SERVER_ADDR'], 0, 48);
        // Should bot be localhost.
        if (empty($spammaster_ip) || '0' === $spammaster_ip || '127.0.0.1' === $spammaster_ip) {
          if (isset($_SERVER['SERVER_NAME'])) {
            $spam_master_ip_gethostbyname = gethostbyname($_SERVER['SERVER_NAME']);
            $spammaster_ip = substr($spam_master_ip_gethostbyname, 0, 48);
            if (empty($spam_master_ip_gethostbyname) || '0' === $spam_master_ip_gethostbyname) {
              $spam_master_urlparts = parse_url($spammaster_site_url);
              $spam_master_hostname_temp = $spam_master_urlparts['host'];
              $spam_master_result = dns_get_record($spam_master_hostname_temp, DNS_A);
              $spammaster_ip = substr($spam_master_result[0]['ip'], 0, 48);
            }
          }
          else {
            $spammaster_ip = 'I 000';
          }
        }
        $spammaster_hostname = substr(gethostbyaddr($_SERVER['SERVER_ADDR']), 0, 256);
        if (empty($spammaster_hostname) || '0' === $spammaster_hostname || '127.0.0.1' === $spammaster_hostname) {
          if (isset($_SERVER['SERVER_NAME'])) {
            $spam_master_ho_gethostbyname = gethostbyname($_SERVER['SERVER_NAME']);
            $spammaster_hostname = substr($spam_master_ho_gethostbyname, 0, 256);
            if (empty($spam_master_ho_gethostbyname) || '0' === $spam_master_ho_gethostbyname) {
              $spam_master_urlparts = parse_url($spammaster_site_url);
              $spam_master_hostname_temp = $spam_master_urlparts['host'];
              $spam_master_result = dns_get_record($spam_master_hostname_temp, DNS_A);
              $spammaster_hostname = substr($spam_master_result[0]['ip'], 0, 256);
            }
          }
          else {
            $spammaster_hostname = 'H 000';
          }
        }
      }
      else {
        if (isset($_SERVER['SERVER_NAME'])) {
          $spam_master_ip_gethostbyname = gethostbyname($_SERVER['SERVER_NAME']);
          $spammaster_ip = substr($spam_master_ip_gethostbyname, 0, 48);
          if (empty($spam_master_ip_gethostbyname) || '0' === $spam_master_ip_gethostbyname) {
            $spam_master_urlparts = parse_url($spammaster_site_url);
            $spam_master_hostname_temp = $spam_master_urlparts['host'];
            $spam_master_result = dns_get_record($spam_master_hostname_temp, DNS_A);
            $spammaster_ip = substr($spam_master_result[0]['ip'], 0, 48);
          }
          else {
            $spammaster_ip = 'I 001';
          }
          $spam_master_ho_gethostbyname = gethostbyname($_SERVER['SERVER_NAME']);
          $spammaster_hostname = substr($spam_master_ho_gethostbyname, 0, 256);
          if (empty($spam_master_ho_gethostbyname) || '0' === $spam_master_ho_gethostbyname) {
            $spam_master_urlparts = parse_url($spammaster_site_url);
            $spam_master_hostname_temp = $spam_master_urlparts['host'];
            $spam_master_result = dns_get_record($spam_master_hostname_temp, DNS_A);
            $spammaster_hostname = substr($spam_master_result[0]['ip'], 0, 256);
          }
          else {
            $spammaster_hostname = 'H 001';
          }
        }
        else {
          $spammaster_ip = 'I 002';
          $spammaster_hostname = 'H 002';
        }
      }

      // Encode ssl post link security.
      $spammaster_learn_act_url = 'https://www.spammaster.org/core/learn/get_learn_act.php';

      // Call drupal hhtpclient.
      $client = $this->httpClient;
      // Post data.
      $request = $client->post($spammaster_learn_act_url, [
        'form_params' => [
          'blog_license_key' => $spammaster_license,
          'blog_hash_key' => $spammaster_db_protection_hash,
        ],
      ]);
      // Decode json data.
      $response = Json::decode($request->getBody());
      if (empty($response)) {
        // Reset new options.
        $this->state->set('spammaster.new_options', '0');
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_date,
          'spamkey' => 'spammaster',
          'spamvalue' => 'Spam Master: Action service finished ok.',
        ])->execute();
        // Set to flush.
        drupal_flush_all_caches();
      }
      else {
        if (empty($response['key']) || empty($response['hash'])) {
          // Reset new options.
          $this->state->set('spammaster.new_options', '0');
          $this->connection->insert('spammaster_keys')->fields([
            'date' => $spammaster_date,
            'spamkey' => 'spammaster',
            'spamvalue' => 'Spam Master: Action service finished.',
          ])->execute();
          // Set to flush.
          drupal_flush_all_caches();
        }
        else {
          // Check key & hash.
          if ($response['key'] == $spammaster_license && $response['hash'] == $spammaster_db_protection_hash) {
            // Misses response type cache and response value 12m.
            if ('Add' === $response['action']) {
              // Remove any database entries.
              $this->connection->delete('spammaster_keys')
                ->condition('spamkey', 'white-transient-haf', '=')
                ->condition('spamvalue', $response['pack'], '=')
                ->execute();
              $this->connection->delete('spammaster_keys')
                ->condition('spamkey', 'white-transient-form', '=')
                ->condition('spamvalue', $response['pack'], '=')
                ->execute();
              $this->connection->delete('spammaster_white')
                ->condition('white', $response['pack'], '=')
                ->execute();
              $this->connection->delete('spammaster_threats')
                ->condition('threat', $response['pack'], '=')
                ->execute();
              if ('Buffer' === $response['where']) {
                $this->connection->insert('spammaster_threats')->fields([
                  'date' => $spammaster_date,
                  'threat' => $response['pack'],
                ])->execute();
              }
              if ('White' === $response['where']) {
                $this->connection->insert('spammaster_white')->fields([
                  'date' => $spammaster_date,
                  'white' => $response['pack'],
                ])->execute();
              }
              if ('exempt-needle-straw' === $response['where']) {
                $this->connection->insert('spammaster_keys')->fields([
                  'date' => $spammaster_date,
                  'spamkey' => 'exempt-needle-straw',
                  'spamvalue' => $response['pack'],
                ])->execute();
              }
              if ('exempt-needle-sig-hide' === $response['where']) {
                $this->connection->insert('spammaster_keys')->fields([
                  'date' => $spammaster_date,
                  'spamkey' => 'exempt-needle-sig-hide',
                  'spamvalue' => $response['pack'],
                ])->execute();
              }
              if ('exempt-needle-sig-show' === $response['where']) {
                $this->connection->insert('spammaster_keys')->fields([
                  'date' => $spammaster_date,
                  'spamkey' => 'exempt-needle-sig-show',
                  'spamvalue' => $response['pack'],
                ])->execute();
              }
              if ('exempt-needle' === $response['where']) {
                $this->connection->insert('spammaster_keys')->fields([
                  'date' => $spammaster_date,
                  'spamkey' => 'exempt-needle',
                  'spamvalue' => $response['pack'],
                ])->execute();
              }
              if ('exempt-key' === $response['where']) {
                $this->connection->insert('spammaster_keys')->fields([
                  'date' => $spammaster_date,
                  'spamkey' => 'exempt-key',
                  'spamvalue' => $response['pack'],
                ])->execute();
              }
              if ('exempt-value' === $response['where']) {
                $this->connection->insert('spammaster_keys')->fields([
                  'date' => $spammaster_date,
                  'spamkey' => 'exempt-value',
                  'spamvalue' => $response['pack'],
                ])->execute();
              }
              if ('white-transient-haf' === $response['where']) {
                $this->connection->insert('spammaster_keys')->fields([
                  'date' => $spammaster_date,
                  'spamkey' => 'white-transient-haf',
                  'spamvalue' => $response['pack'],
                ])->execute();
              }
              if ('white-transient-form' === $response['where']) {
                $this->connection->insert('spammaster_keys')->fields([
                  'date' => $spammaster_date,
                  'spamkey' => 'white-transient-haf',
                  'spamvalue' => $response['pack'],
                ])->execute();
              }
            }
            if ('Remove' === $response['action']) {
              if ('Buffer' === $response['where']) {
                $this->connection->delete('spammaster_threats')
                  ->condition('threat', $response['pack'], '=')
                  ->execute();
              }
              if ('White' === $response['where']) {
                $this->connection->delete('spammaster_white')
                  ->condition('white', $response['pack'], '=')
                  ->execute();
              }
              if ('exempt-needle-straw' === $response['where']) {
                $this->connection->delete('spammaster_keys')
                  ->condition('spamkey', 'exempt-needle-straw', '=')
                  ->condition('spamvalue', $response['pack'], '=')
                  ->execute();
              }
              if ('exempt-needle-sig-hide' === $response['where']) {
                $this->connection->delete('spammaster_keys')
                  ->condition('spamkey', 'exempt-needle-sig-hide', '=')
                  ->condition('spamvalue', $response['pack'], '=')
                  ->execute();
              }
              if ('exempt-needle-sig-show' === $response['where']) {
                $this->connection->delete('spammaster_keys')
                  ->condition('spamkey', 'exempt-needle-sig-show', '=')
                  ->condition('spamvalue', $response['pack'], '=')
                  ->execute();
              }
              if ('exempt-needle' === $response['where']) {
                $this->connection->delete('spammaster_keys')
                  ->condition('spamkey', 'exempt-needle', '=')
                  ->condition('spamvalue', $response['pack'], '=')
                  ->execute();
              }
              if ('exempt-key' === $response['where']) {
                $this->connection->delete('spammaster_keys')
                  ->condition('spamkey', 'exempt-key', '=')
                  ->condition('spamvalue', $response['pack'], '=')
                  ->execute();
              }
              if ('exempt-value' === $response['where']) {
                $this->connection->delete('spammaster_keys')
                  ->condition('spamkey', 'exempt-value', '=')
                  ->condition('spamvalue', $response['pack'], '=')
                  ->execute();
              }
              if ('white-transient-haf' === $response['where']) {
                $this->connection->delete('spammaster_keys')
                  ->condition('spamkey', 'white-transient-haf', '=')
                  ->condition('spamvalue', $response['pack'], '=')
                  ->execute();
              }
              if ('white-transient-form' === $response['where']) {
                $this->connection->delete('spammaster_keys')
                  ->condition('spamkey', 'white-transient-form', '=')
                  ->condition('spamvalue', $response['pack'], '=')
                  ->execute();
              }
            }
            // Change requires type for key name.
            if ('Change' === $response['action']) {
              if ('exempt-needle-straw' === $response['where']) {
                $this->connection->update('spammaster_keys')
                  ->fields([
                    'spamvalue' => $response['pack'],
                  ])
                  ->condition('spamkey', 'exempt-needle-straw', '=')
                  ->condition('spamvalue', $response['type'], '=')
                  ->execute();
              }
              if ('exempt-needle-sig-hide' === $response['where']) {
                $this->connection->update('spammaster_keys')
                  ->fields([
                    'spamvalue' => $response['pack'],
                  ])
                  ->condition('spamkey', 'exempt-needle-sig-hide', '=')
                  ->condition('spamvalue', $response['type'], '=')
                  ->execute();
              }
              if ('exempt-needle-sig-show' === $response['where']) {
                $this->connection->update('spammaster_keys')
                  ->fields([
                    'spamvalue' => $response['pack'],
                  ])
                  ->condition('spamkey', 'exempt-needle-sig-show', '=')
                  ->condition('spamvalue', $response['type'], '=')
                  ->execute();
              }
              if ('exempt-needle' === $response['where']) {
                $this->connection->update('spammaster_keys')
                  ->fields([
                    'spamvalue' => $response['pack'],
                  ])
                  ->condition('spamkey', 'exempt-needle', '=')
                  ->condition('spamvalue', $response['type'], '=')
                  ->execute();
              }
              if ('exempt-key' === $response['where']) {
                $this->connection->update('spammaster_keys')
                  ->fields([
                    'spamvalue' => $response['pack'],
                  ])
                  ->condition('spamkey', 'exempt-key', '=')
                  ->condition('spamvalue', $response['type'], '=')
                  ->execute();
              }
              if ('exempt-value' === $response['where']) {
                $this->connection->update('spammaster_keys')
                  ->fields([
                    'spamvalue' => $response['pack'],
                  ])
                  ->condition('spamkey', 'exempt-value', '=')
                  ->condition('spamvalue', $response['type'], '=')
                  ->execute();
              }
              if ('white-transient-haf' === $response['where']) {
                $this->connection->update('spammaster_keys')
                  ->fields([
                    'spamvalue' => $response['pack'],
                  ])
                  ->condition('spamkey', 'white-transient-haf', '=')
                  ->condition('spamvalue', $response['type'], '=')
                  ->execute();
              }
              if ('white-transient-form' === $response['where']) {
                $this->connection->update('spammaster_keys')
                  ->fields([
                    'spamvalue' => $response['pack'],
                  ])
                  ->condition('spamkey', 'white-transient-form', '=')
                  ->condition('spamvalue', $response['type'], '=')
                  ->execute();
              }
              if ('editable' === $response['where']) {
                $this->configFactory->getEditable('spammaster.settings_protection')
                  ->set($response['type'], $response['pack'])
                  ->save();
              }
              if ('state' === $response['where']) {
                $this->state->set($response['type'], $response['pack']);
              }
            }
            if (empty($response['type']) || 'none' === $response['type']) {
              $spamtype = FALSE;
            }
            else {
              $spamtype = ' -> ' . $response['type'];
            }
            $this->connection->insert('spammaster_keys')->fields([
              'date' => $spammaster_date,
              'spamkey' => 'spammaster',
              'spamvalue' => 'Spam Master: Action service -> ' . $response['action'] . ' -> ' . $response['where'] . $spamtype . ' -> ' . $response['pack'],
            ])->execute();
            // Set new_options on.
            $this->state->set('spammaster.new_options', '1');
            // Spam action service.
            $spammaster_action_service = new SpamMasterActionService($this->connection, $this->configFactory, $this->state, $this->requestStack, $this->httpClient);
            $spammaster_action_service->spamMasterActMore();
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterActMore() {

    // Get variables.
    $spammaster_new_option = $this->state->get('spammaster.new_options');
    // Are there new options.
    if ('1' === $spammaster_new_option) {
      $spammaster_date = date('Y-m-d H:i:s');
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spammaster_date,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Action service called via act more.',
      ])->execute();
      // Spam action service.
      $spama = '1';
      $spammaster_action_service = new SpamMasterActionService($this->connection, $this->configFactory, $this->state, $this->requestStack, $this->httpClient);
      $spammaster_action_service->spamMasterAct($spama);
    }
  }

}
