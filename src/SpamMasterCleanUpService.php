<?php

namespace Drupal\spammaster;

use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class service.
 */
class SpamMasterCleanUpService {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The spammasterWhiteTransient.
   *
   * @var \Drupal\spammmaster\SpamMasterCleanUpService
   */
  protected $spammasterWhiteTransient;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, ConfigFactoryInterface $configFactory, StateInterface $state) {
    $this->connection = $connection;
    $this->configFactory = $configFactory;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterCleanUpTransients() {
    $spammaster_status = $this->state->get('spammaster.license_status');
    if ('VALID' === $spammaster_status || 'MALFUNCTION_1' === $spammaster_status || 'MALFUNCTION_2' === $spammaster_status) {
      $spammaster_dt = date('Y-m-d H:i:s');
      $minus_hour = date('Y-m-d H:i:s', strtotime('-10 minutes', strtotime($spammaster_dt)));
      // Delete data older than input months.
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'white-transient-haf', '=')
        ->condition('date', $minus_hour, '<=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'white-transient-form', '=')
        ->condition('date', $minus_hour, '<=')
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterCleanUpKeys() {

    // Get variables.
    $spammaster_settings_protection = $this->configFactory->getEditable('spammaster.settings_protection');
    $spammaster_status = $this->state->get('spammaster.license_status');
    $spamsenddb = $this->state->get('spammaster.spam_master_disc_not');
    $spamsenddbdatepre = $this->state->get('spammaster.spam_master_disc_not_date');
    $spammaster_type = $this->state->get('spammaster.type');
    $spammaster_white_transient = $this->state->get('spammaster.white_transient');
    // Prepare date time.
    $spammaster_dt = date('Y-m-d H:i:s');
    $spam_master_short_date = date('Y-m-d');
    if ('VALID' === $spammaster_status || 'MALFUNCTION_1' === $spammaster_status || 'MALFUNCTION_2' === $spammaster_status) {
      // Process system log.
      $spammaster_cleanup_system = $spammaster_settings_protection->get('spammaster.cleanup_system');
      if ('0' === $spammaster_cleanup_system) {
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: System weekly clean-up cron did not run. Is set to infinite.',
        ])->execute();
      }
      else {
        $minus_sys_months_time = date('Y-m-d H:i:s', strtotime('-' . $spammaster_cleanup_system . ' days', strtotime($spammaster_dt)));
        // Delete data older than input months.
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'spammaster', '=')
          ->condition('date', $minus_sys_months_time, '<=')
          ->execute();
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'spammaster-license', '=')
          ->condition('date', $minus_sys_months_time, '<=')
          ->execute();
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: System weekly clean-up cron successfully run. Deleted older than ' . $spammaster_cleanup_system . ' days.',
        ])->execute();
      }
      // Process cron log.
      $spammaster_cleanup_cron = $spammaster_settings_protection->get('spammaster.cleanup_cron');
      if ('0' === $spammaster_cleanup_cron) {
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: Cron weekly clean-up cron did not run. Is set to infinite.',
        ])->execute();
      }
      else {
        $minus_cron_months_time = date('Y-m-d H:i:s', strtotime('-' . $spammaster_cleanup_cron . ' days', strtotime($spammaster_dt)));
        // Delete data older than input months.
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'spammaster-cron', '=')
          ->condition('date', $minus_cron_months_time, '<=')
          ->execute();
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: Cron weekly clean-up cron successfully run. Deleted older than ' . $spammaster_cleanup_cron . ' days.',
        ])->execute();
      }

      // Process mail log.
      $spammaster_cleanup_mail = $spammaster_settings_protection->get('spammaster.cleanup_mail');
      if ('0' === $spammaster_cleanup_mail) {
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: Mail weekly clean-up cron did not run. Is set to infinite.',
        ])->execute();
      }
      else {
        $minus_mail_months_time = date('Y-m-d H:i:s', strtotime('-' . $spammaster_cleanup_mail . ' days', strtotime($spammaster_dt)));
        // Delete data older than input months.
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'spammaster-mail', '=')
          ->condition('date', $minus_mail_months_time, '<=')
          ->execute();
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: Mail weekly clean-up cron successfully run. Deleted older than ' . $spammaster_cleanup_mail . ' days.',
        ])->execute();
      }

      // Process whitelist log.
      $spammaster_cleanup_whitelist = $spammaster_settings_protection->get('spammaster.cleanup_whitelist');
      if ('0' === $spammaster_cleanup_whitelist) {
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: Whitelist weekly clean-up cron did not run. Is set to infinite.',
        ])->execute();
      }
      else {
        $minus_white_months_time = date('Y-m-d H:i:s', strtotime('-' . $spammaster_cleanup_whitelist . ' days', strtotime($spammaster_dt)));
        // Delete data older than input months.
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'spammaster-whitelist', '=')
          ->condition('date', $minus_white_months_time, '<=')
          ->execute();
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: Whitelist weekly clean-up cron successfully run. Deleted older than ' . $spammaster_cleanup_whitelist . ' days.',
        ])->execute();
      }

      // Process firewall log.
      $spammaster_cleanup_firewall = $spammaster_settings_protection->get('spammaster.cleanup_firewall');
      if ('0' === $spammaster_cleanup_firewall) {
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: Firewall weekly clean-up cron did not run. Is set to infinite.',
        ])->execute();
      }
      else {
        $minus_fire_months_time = date('Y-m-d H:i:s', strtotime('-' . $spammaster_cleanup_firewall . ' days', strtotime($spammaster_dt)));
        // Delete data older than input months.
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'spammaster-firewall', '=')
          ->condition('date', $minus_fire_months_time, '<=')
          ->execute();
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: Firewall weekly clean-up cron successfully run. Deleted older than ' . $spammaster_cleanup_firewall . ' days.',
        ])->execute();
      }

      // Process honeypot log.
      $spammaster_cleanup_honeypot = $spammaster_settings_protection->get('spammaster.cleanup_honeypot');
      if ('0' === $spammaster_cleanup_honeypot) {
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: Honeypot weekly clean-up cron did not run. Is set to infinite.',
        ])->execute();
      }
      else {
        $minus_hon_months_time = date('Y-m-d H:i:s', strtotime('-' . $spammaster_cleanup_honeypot . ' days', strtotime($spammaster_dt)));
        // Delete data older than input months.
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'spammaster-honeypot', '=')
          ->condition('date', $minus_hon_months_time, '<=')
          ->execute();
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: Honeypot weekly clean-up cron successfully run. Deleted older than ' . $spammaster_cleanup_honeypot . ' days.',
        ])->execute();
      }

      // Delete white transients if off.
      if ('0' === $spammaster_white_transient) {
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'white-transient-haf', '=')
          ->execute();
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'white-transient-form', '=')
          ->execute();
      }

      // Process default values.
      if ('FULL' !== $spammaster_type) {
        // Store state values.
        $default_values = [
          'spammaster.spam_master_is_cloudflare' => '0',
          'spammaster.signature' => '1',
        ];
        $this->state->setMultiple($default_values);
        $basic_firewall_rules_set = $this->state->get('spammaster.basic_firewall_rules_set');
        if ('1' === $basic_firewall_rules_set) {
          $spammaster_settings_protection->set('spammaster.basic_firewall_rules', '3');
        }
      }

      // Process default exempt values if empty by zeroing state.
      $spammaster_exempt_count = $this->connection->select('spammaster_keys', 'u');
      $spammaster_exempt_count->condition('u.spamkey', '%' . $this->connection->escapeLike('exempt') . '%', 'LIKE');
      $spammaster_exempt_count->fields('u', ['spamkey']);
      $spammaster_exempt_query = $spammaster_exempt_count->countQuery()->execute()->fetchField();
      if (empty($spammaster_exempt_query) || '0' === $spammaster_exempt_query || NULL === $spammaster_exempt_query) {
        $db_values = [
          'spammaster.db_install_version_new' => '000',
          'spammaster.db_install_version_221' => '0',
          'spammaster.db_install_version_223' => '0',
          'spammaster.db_install_version_227' => '0',
          'spammaster.db_install_version_229' => '0',
          'spammaster.db_install_version_231' => '0',
          'spammaster.db_install_version_238' => '0',
          'spammaster.db_install_version_242' => '0',
          'spammaster.db_install_version_243' => '0',
          'spammaster.db_install_version_244' => '0',
          'spammaster.db_install_version_246' => '0',
          'spammaster.db_install_version_249' => '0',
          'spammaster.db_install_version_253' => '0',
          'spammaster.db_install_version_254' => '0',
          'spammaster.db_install_version_255' => '0',
          'spammaster.db_install_version_256' => '0',
          'spammaster.db_install_version_257' => '0',
        ];
        $this->state->setMultiple($db_values);
      }
    }
    else {
      // Log message.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spammaster_dt,
        'spamkey' => 'spammaster-cron',
        'spamvalue' => 'Spam Master: Weekly logs clean-up cron did not run, check your license status.',
      ])->execute();
    }
    if ($spam_master_short_date >= $spamsenddbdatepre && '1' === $spamsenddb) {
      // Update state value.
      $this->state->set('spammaster.spam_master_disc_not', '0');
    }

  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterCleanUpBuffer() {

    // Get variables.
    $spammaster_status = $this->state->get('spammaster.license_status');
    $spamsenddb = $this->state->get('spammaster.spam_master_disc_not');
    $spamsenddbdatepre = $this->state->get('spammaster.spam_master_disc_not_date');
    $spammaster_settings_protection = $this->configFactory->getEditable('spammaster.settings_protection');
    $spam_master_is_cloudflare = $spammaster_settings_protection->get('spammaster.spam_master_is_cloudflare');
    // Prepare date.
    $spammaster_dt = date('Y-m-d H:i:s');
    $spam_master_short_date = date('Y-m-d');
    if ('VALID' === $spammaster_status || 'MALFUNCTION_1' === $spammaster_status || 'MALFUNCTION_2' === $spammaster_status) {
      // Set 180 days time.
      $time_expires = date('Y-m-d H:i:s', strtotime($spammaster_dt . '-180 days'));

      if ('1' === $spam_master_is_cloudflare) {
        $spammaster_buffer_ips = $this->connection->query("SELECT * FROM {spammaster_threats}")->fetchAll();
        if (!empty($spammaster_buffer_ips)) {
          foreach ($spammaster_buffer_ips as $del_ip) {
            $spam_ip = $del_ip->threat;
            if (filter_var($spam_ip, FILTER_VALIDATE_IP)) {
              $spam_id = $del_ip->id;
              // Delete cdn data except email.
              $this->connection->delete('spammaster_threats')
                ->condition('id', $spam_id)
                ->execute();
            }
          }
        }
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master:  Buffer Cloudflare clean-up cron successful run.',
        ])->execute();
      }
      else {
        // Delete data older than 180 days.
        $this->connection->delete('spammaster_threats')
          ->condition('date', $time_expires, '<=')
          ->execute();

        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master:  Buffer weekly clean-up cron successful run.',
        ])->execute();
      }
      // Compare whitelist against buffer and clean buffer.
      $spammaster_white_clean_query = $this->connection->query("SELECT * FROM {spammaster_white}")->fetchAll();
      if (!empty($spammaster_white_clean_query)) {
        foreach ($spammaster_white_clean_query as $to_del) {
          $white_del = $to_del->white;
          $this->connection->delete('spammaster_threats')
            ->condition('threat', $white_del)
            ->execute();
        }
      }
    }
    else {
      // Log message.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spammaster_dt,
        'spamkey' => 'spammaster-cron',
        'spamvalue' => 'Spam Master:  Buffer weekly clean-up cron did not run, check your license status.',
      ])->execute();
    }
    if ($spam_master_short_date >= $spamsenddbdatepre && '1' === $spamsenddb) {
      // Update state value.
      $this->state->set('spammaster.spam_master_disc_not', '0');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterCleanUpLazy() {

    // Get variables.
    $spammaster_status = $this->state->get('spammaster.license_status');
    $spamsenddb = $this->state->get('spammaster.spam_master_disc_not');
    $spamsenddbdatepre = $this->state->get('spammaster.spam_master_disc_not_date');
    // Prepare short date.
    $spam_master_short_date = date('Y-m-d');
    if ('VALID' === $spammaster_status || 'MALFUNCTION_1' === $spammaster_status || 'MALFUNCTION_2' === $spammaster_status) {
      $updated_values = [
        'spammaster.spam_master_license_sync_date' => $spam_master_short_date,
        'spammaster.spam_master_license_sync_run' => '0',
      ];
      $this->state->setMultiple($updated_values);
    }
    if ($spam_master_short_date >= $spamsenddbdatepre && '1' === $spamsenddb) {
      // Update state value.
      $this->state->set('spammaster.spam_master_disc_not', '0');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterCleanUpTransNow($spammasterWhiteTransient) {
    $spammaster_status = $this->state->get('spammaster.license_status');
    if ('VALID' === $spammaster_status || 'MALFUNCTION_1' === $spammaster_status || 'MALFUNCTION_2' === $spammaster_status) {
      $spammaster_dt = date('Y-m-d H:i:s');
      $minus_minutes = date('Y-m-d H:i:s', strtotime('-5 minutes', strtotime($spammaster_dt)));
      // Delete data older than input months.
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'white-transient-haf', '=')
        ->condition('date', $minus_minutes, '<=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'white-transient-form', '=')
        ->condition('date', $minus_minutes, '<=')
        ->execute();
    }
  }

}
