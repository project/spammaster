<?php

namespace Drupal\spammaster\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\spammaster\SpamMasterCollectService;

/**
 * Class controller.
 */
class SpamMasterFirewallController extends ControllerBase {

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The SpamMasterCollectService Service.
   *
   * @var \Drupal\spammaster\SpamMasterCollectService
   */
  protected $collectService;

  /**
   * Page cache kill switch.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $requestStack, SpamMasterCollectService $collectService) {
    $this->requestStack = $requestStack;
    $this->collectService = $collectService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('spammaster.collect_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterFirewall() {

    // Collect data.
    $collectnow = $this->requestStack->getCurrentRequest()->getClientIp();
    $spammaster_collect_service = $this->collectService;
    $is_collected = $spammaster_collect_service->spamMasterGetCollect($collectnow);
    $spamcollection = Json::decode($is_collected);
    // Set firewall ip.
    $spam_master_firewall_ip = $spamcollection['spammasterip'];
    // Set firewall browser.
    $spam_master_firewall_browser = $spamcollection['spammasteragent'];

    return [
      '#theme' => 'firewall',
      '#type' => 'page',
      '#attached' => [
        'library' => [
          'spammaster/spammaster-styles',
        ],
      ],
      '#spam_master_firewall_ip' => $spam_master_firewall_ip,
      '#spam_master_firewall_browser' => $spam_master_firewall_browser,
    ];
  }

}
