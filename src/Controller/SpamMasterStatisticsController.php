<?php

namespace Drupal\spammaster\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class controller.
 */
class SpamMasterStatisticsController extends ControllerBase {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, StateInterface $state) {
    $this->connection = $connection;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterStatistics() {

    // Prepare 5 Days.
    $spam_master_today_minus_1 = date('Y-m-d');
    $spam_master_today_minus_2 = date('Y-m-d', strtotime($spam_master_today_minus_1 . '-1 days'));
    $spam_master_today_minus_3 = date('Y-m-d', strtotime($spam_master_today_minus_1 . '-2 days'));
    $spam_master_today_minus_4 = date('Y-m-d', strtotime($spam_master_today_minus_1 . '-3 days'));
    $spam_master_today_minus_5 = date('Y-m-d', strtotime($spam_master_today_minus_1 . '-4 days'));

    // Prepare 3 months.
    $spam_master_month_minus_1 = date('Y-m');
    $spam_master_month_minus_2 = date('Y-m', strtotime($spam_master_today_minus_1 . '-1 months'));
    $spam_master_month_minus_3 = date('Y-m', strtotime($spam_master_today_minus_1 . '-2 months'));

    // Get count last day of all logs from spammaster_keys.
    $spammaster_spam_all_logs_query1 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_all_logs_query1->fields('u', ['spamkey']);
    $spammaster_spam_all_logs_query1->where('(date LIKE :date)', [':date' => $spam_master_today_minus_1 . '%']);
    $spammaster_spam_all_logs_result1 = $spammaster_spam_all_logs_query1->countQuery()->execute()->fetchField();
    // Get count last 2 days of all logs from spammaster_keys.
    $spammaster_spam_all_logs_query2 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_all_logs_query2->fields('u', ['spamkey']);
    $spammaster_spam_all_logs_query2->where('(date LIKE :date)', [':date' => $spam_master_today_minus_2 . '%']);
    $spammaster_spam_all_logs_result2 = $spammaster_spam_all_logs_query2->countQuery()->execute()->fetchField();
    // Get count last 3 days of all logs from spammaster_keys.
    $spammaster_spam_all_logs_query3 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_all_logs_query3->fields('u', ['spamkey']);
    $spammaster_spam_all_logs_query3->where('(date LIKE :date)', [':date' => $spam_master_today_minus_3 . '%']);
    $spammaster_spam_all_logs_result3 = $spammaster_spam_all_logs_query3->countQuery()->execute()->fetchField();
    // Get count last 4 days of all logs from spammaster_keys.
    $spammaster_spam_all_logs_query4 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_all_logs_query4->fields('u', ['spamkey']);
    $spammaster_spam_all_logs_query4->where('(date LIKE :date)', [':date' => $spam_master_today_minus_4 . '%']);
    $spammaster_spam_all_logs_result4 = $spammaster_spam_all_logs_query4->countQuery()->execute()->fetchField();
    // Get count last 5 days of all logs from spammaster_keys.
    $spammaster_spam_all_logs_query5 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_all_logs_query5->fields('u', ['spamkey']);
    $spammaster_spam_all_logs_query5->where('(date LIKE :date)', [':date' => $spam_master_today_minus_5 . '%']);
    $spammaster_spam_all_logs_result5 = $spammaster_spam_all_logs_query5->countQuery()->execute()->fetchField();

    // Get count last day of system logs from spammaster_keys.
    $spammaster_spam_system_logs_query1 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_system_logs_query1->fields('u', ['spamkey']);
    $spammaster_spam_system_logs_query1->where('(date LIKE :date)', [':date' => $spam_master_today_minus_1 . '%']);
    $spammaster_spam_system_logs_query1->where('(spamkey != :firewall AND spamkey != :honeypot)', [
      ':firewall' => 'spammaster-firewall',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_spam_system_logs_result1 = $spammaster_spam_system_logs_query1->countQuery()->execute()->fetchField();
    // Get count last 2 days of system logs from spammaster_keys.
    $spammaster_spam_system_logs_query2 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_system_logs_query2->fields('u', ['spamkey']);
    $spammaster_spam_system_logs_query2->where('(date LIKE :date)', [':date' => $spam_master_today_minus_2 . '%']);
    $spammaster_spam_system_logs_query2->where('(spamkey != :firewall AND spamkey != :honeypot)', [
      ':firewall' => 'spammaster-firewall',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_spam_system_logs_result2 = $spammaster_spam_system_logs_query2->countQuery()->execute()->fetchField();
    // Get count last 3 days of system logs from spammaster_keys.
    $spammaster_spam_system_logs_query3 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_system_logs_query3->fields('u', ['spamkey']);
    $spammaster_spam_system_logs_query3->where('(date LIKE :date)', [':date' => $spam_master_today_minus_3 . '%']);
    $spammaster_spam_system_logs_query3->where('(spamkey != :firewall AND spamkey != :honeypot)', [
      ':firewall' => 'spammaster-firewall',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_spam_system_logs_result3 = $spammaster_spam_system_logs_query3->countQuery()->execute()->fetchField();
    // Get count last 4 days of system logs from spammaster_keys.
    $spammaster_spam_system_logs_query4 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_system_logs_query4->fields('u', ['spamkey']);
    $spammaster_spam_system_logs_query4->where('(date LIKE :date)', [':date' => $spam_master_today_minus_4 . '%']);
    $spammaster_spam_system_logs_query4->where('(spamkey != :firewall AND spamkey != :honeypot)', [
      ':firewall' => 'spammaster-firewall',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_spam_system_logs_result4 = $spammaster_spam_system_logs_query4->countQuery()->execute()->fetchField();
    // Get count last 5 days of system logs from spammaster_keys.
    $spammaster_spam_system_logs_query5 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_system_logs_query5->fields('u', ['spamkey']);
    $spammaster_spam_system_logs_query5->where('(date LIKE :date)', [':date' => $spam_master_today_minus_5 . '%']);
    $spammaster_spam_system_logs_query5->where('(spamkey != :firewall AND spamkey != :honeypot)', [
      ':firewall' => 'spammaster-firewall',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_spam_system_logs_result5 = $spammaster_spam_system_logs_query5->countQuery()->execute()->fetchField();

    // Get count last day of firewall blocks from spammaster_keys.
    $spammaster_spam_firewall_query1 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_firewall_query1->fields('u', ['spamkey']);
    $spammaster_spam_firewall_query1->where('(date LIKE :date AND spamkey = :firewall)', [
      ':date' => $spam_master_today_minus_1 . '%',
      ':firewall' => 'spammaster-firewall',
    ]);
    $spammaster_spam_firewall_result1 = $spammaster_spam_firewall_query1->countQuery()->execute()->fetchField();
    // Get count last 2 days of firewall blocks from spammaster_keys.
    $spammaster_spam_firewall_query2 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_firewall_query2->fields('u', ['spamkey']);
    $spammaster_spam_firewall_query2->where('(date LIKE :date AND spamkey = :firewall)', [
      ':date' => $spam_master_today_minus_2 . '%',
      ':firewall' => 'spammaster-firewall',
    ]);
    $spammaster_spam_firewall_result2 = $spammaster_spam_firewall_query2->countQuery()->execute()->fetchField();
    // Get count last 3 days of firewall blocks from spammaster_keys.
    $spammaster_spam_firewall_query3 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_firewall_query3->fields('u', ['spamkey']);
    $spammaster_spam_firewall_query3->where('(date LIKE :date AND spamkey = :firewall)', [
      ':date' => $spam_master_today_minus_3 . '%',
      ':firewall' => 'spammaster-firewall',
    ]);
    $spammaster_spam_firewall_result3 = $spammaster_spam_firewall_query3->countQuery()->execute()->fetchField();
    // Get count last 4 days of firewall blocks from spammaster_keys.
    $spammaster_spam_firewall_query4 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_firewall_query4->fields('u', ['spamkey']);
    $spammaster_spam_firewall_query4->where('(date LIKE :date AND spamkey = :firewall)', [
      ':date' => $spam_master_today_minus_4 . '%',
      ':firewall' => 'spammaster-firewall',
    ]);
    $spammaster_spam_firewall_result4 = $spammaster_spam_firewall_query4->countQuery()->execute()->fetchField();
    // Get count last 5 days of firewall blocks from spammaster_keys.
    $spammaster_spam_firewall_query5 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_firewall_query5->fields('u', ['spamkey']);
    $spammaster_spam_firewall_query5->where('(date LIKE :date AND spamkey = :firewall)', [
      ':date' => $spam_master_today_minus_5 . '%',
      ':firewall' => 'spammaster-firewall',
    ]);
    $spammaster_spam_firewall_result5 = $spammaster_spam_firewall_query5->countQuery()->execute()->fetchField();

    // Get count last day of honeypot blocks from spammaster_keys.
    $spammaster_spam_honeypot_query1 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_honeypot_query1->fields('u', ['spamkey']);
    $spammaster_spam_honeypot_query1->where('(date LIKE :date AND spamkey = :honeypot)', [
      ':date' => $spam_master_today_minus_1 . '%',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_spam_honeypot_result1 = $spammaster_spam_honeypot_query1->countQuery()->execute()->fetchField();
    // Get count last 2 days of honeypot blocks from spammaster_keys.
    $spammaster_spam_honeypot_query2 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_honeypot_query2->fields('u', ['spamkey']);
    $spammaster_spam_honeypot_query2->where('(date LIKE :date AND spamkey = :honeypot)', [
      ':date' => $spam_master_today_minus_2 . '%',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_spam_honeypot_result2 = $spammaster_spam_honeypot_query2->countQuery()->execute()->fetchField();
    // Get count last 3 days of honeypot blocks from spammaster_keys.
    $spammaster_spam_honeypot_query3 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_honeypot_query3->fields('u', ['spamkey']);
    $spammaster_spam_honeypot_query3->where('(date LIKE :date AND spamkey = :honeypot)', [
      ':date' => $spam_master_today_minus_3 . '%',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_spam_honeypot_result3 = $spammaster_spam_honeypot_query3->countQuery()->execute()->fetchField();
    // Get count last 4 days of honeypot blocks from spammaster_keys.
    $spammaster_spam_honeypot_query4 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_honeypot_query4->fields('u', ['spamkey']);
    $spammaster_spam_honeypot_query4->where('(date LIKE :date AND spamkey = :honeypot)', [
      ':date' => $spam_master_today_minus_4 . '%',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_spam_honeypot_result4 = $spammaster_spam_honeypot_query4->countQuery()->execute()->fetchField();
    // Get count last 5 days of honeypot blocks from spammaster_keys.
    $spammaster_spam_honeypot_query5 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_honeypot_query5->fields('u', ['spamkey']);
    $spammaster_spam_honeypot_query5->where('(date LIKE :date AND spamkey = :honeypot)', [
      ':date' => $spam_master_today_minus_5 . '%',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_spam_honeypot_result5 = $spammaster_spam_honeypot_query5->countQuery()->execute()->fetchField();

    // Get total count from module settings.
    $total_count = $this->state->get('spammaster.total_block_count');
    if (empty($total_count)) {
      $total_count = '0';
    }

    // Get total count all logs.
    $spammaster_total_all_logs = $this->connection->select('spammaster_keys', 'u');
    $spammaster_total_all_logs->fields('u', ['spamkey']);
    $total_count_all_logs = $spammaster_total_all_logs->countQuery()->execute()->fetchField();
    // Get total count system logs.
    $spammaster_total_system_logs = $this->connection->select('spammaster_keys', 'u');
    $spammaster_total_system_logs->fields('u', ['spamkey']);
    $spammaster_total_system_logs->where('(spamkey != :firewall AND spamkey != :honeypot)', [
      ':firewall' => 'spammaster-firewall',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $total_count_system_logs = $spammaster_total_system_logs->countQuery()->execute()->fetchField();
    // Get total count firewall.
    $spammaster_total_firewall = $this->connection->select('spammaster_keys', 'u');
    $spammaster_total_firewall->fields('u', ['spamkey']);
    $spammaster_total_firewall->where('(spamkey = :firewall)', [':firewall' => 'spammaster-firewall']);
    $total_count_firewall = $spammaster_total_firewall->countQuery()->execute()->fetchField();
    // Get total count honeypot.
    $spammaster_total_honeypot = $this->connection->select('spammaster_keys', 'u');
    $spammaster_total_honeypot->fields('u', ['spamkey']);
    $spammaster_total_honeypot->where('(spamkey = :honeypot)', [':honeypot' => 'spammaster-honeypot']);
    $total_count_honeypot = $spammaster_total_honeypot->countQuery()->execute()->fetchField();

    // Get month total all logs.
    $spammaster_spam_all_logs_month1 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_all_logs_month1->fields('u', ['spamkey']);
    $spammaster_spam_all_logs_month1->where('(date LIKE :date)', [':date' => $spam_master_month_minus_1 . '%']);
    $spammaster_all_logs_month_result1 = $spammaster_spam_all_logs_month1->countQuery()->execute()->fetchField();
    // Get -1 month total all logs.
    $spammaster_spam_all_logs_month2 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_all_logs_month2->fields('u', ['spamkey']);
    $spammaster_spam_all_logs_month2->where('(date LIKE :date)', [':date' => $spam_master_month_minus_2 . '%']);
    $spammaster_all_logs_month_result2 = $spammaster_spam_all_logs_month2->countQuery()->execute()->fetchField();
    // Get -2 month total all logs.
    $spammaster_spam_all_logs_month3 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_all_logs_month3->fields('u', ['spamkey']);
    $spammaster_spam_all_logs_month3->where('(date LIKE :date)', [':date' => $spam_master_month_minus_3 . '%']);
    $spammaster_all_logs_month_result3 = $spammaster_spam_all_logs_month3->countQuery()->execute()->fetchField();
    // Get month total system logs.
    $spammaster_spam_system_logs_month1 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_system_logs_month1->fields('u', ['spamkey']);
    $spammaster_spam_system_logs_month1->where('(date LIKE :date)', [':date' => $spam_master_month_minus_1 . '%']);
    $spammaster_spam_system_logs_month1->where('(spamkey != :firewall AND spamkey != :honeypot)', [
      ':firewall' => 'spammaster-firewall',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_system_logs_month_result1 = $spammaster_spam_system_logs_month1->countQuery()->execute()->fetchField();
    // Get -1 month total system logs.
    $spammaster_spam_system_logs_month2 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_system_logs_month2->fields('u', ['spamkey']);
    $spammaster_spam_system_logs_month2->where('(date LIKE :date)', [':date' => $spam_master_month_minus_2 . '%']);
    $spammaster_spam_system_logs_month2->where('(spamkey != :firewall AND spamkey != :honeypot)', [
      ':firewall' => 'spammaster-firewall',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_system_logs_month_result2 = $spammaster_spam_system_logs_month2->countQuery()->execute()->fetchField();
    // Get -2 month total system logs.
    $spammaster_spam_system_logs_month3 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_system_logs_month3->fields('u', ['spamkey']);
    $spammaster_spam_system_logs_month3->where('(date LIKE :date)', [':date' => $spam_master_month_minus_3 . '%']);
    $spammaster_spam_system_logs_month3->where('(spamkey != :firewall AND spamkey != :honeypot)', [
      ':firewall' => 'spammaster-firewall',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_system_logs_month_result3 = $spammaster_spam_system_logs_month3->countQuery()->execute()->fetchField();
    // Get month total firewall.
    $spammaster_spam_firewall_month1 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_firewall_month1->fields('u', ['spamkey']);
    $spammaster_spam_firewall_month1->where('(date LIKE :date AND spamkey = :firewall)', [
      ':date' => $spam_master_month_minus_1 . '%',
      ':firewall' => 'spammaster-firewall',
    ]);
    $spammaster_firewall_month_result1 = $spammaster_spam_firewall_month1->countQuery()->execute()->fetchField();
    // Get -1 month total firewall.
    $spammaster_spam_firewall_month2 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_firewall_month2->fields('u', ['spamkey']);
    $spammaster_spam_firewall_month2->where('(date LIKE :date AND spamkey = :firewall)', [
      ':date' => $spam_master_month_minus_2 . '%',
      ':firewall' => 'spammaster-firewall',
    ]);
    $spammaster_firewall_month_result2 = $spammaster_spam_firewall_month2->countQuery()->execute()->fetchField();
    // Get -2 month total firewall.
    $spammaster_spam_firewall_month3 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_firewall_month3->fields('u', ['spamkey']);
    $spammaster_spam_firewall_month3->where('(date LIKE :date AND spamkey = :firewall)', [
      ':date' => $spam_master_month_minus_3 . '%',
      ':firewall' => 'spammaster-firewall',
    ]);
    $spammaster_firewall_month_result3 = $spammaster_spam_firewall_month3->countQuery()->execute()->fetchField();
    // Get month total honeypot.
    $spammaster_spam_honeypot_month1 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_honeypot_month1->fields('u', ['spamkey']);
    $spammaster_spam_honeypot_month1->where('(date LIKE :date AND spamkey = :honeypot)', [
      ':date' => $spam_master_month_minus_1 . '%',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_honeypot_month_result1 = $spammaster_spam_honeypot_month1->countQuery()->execute()->fetchField();
    // Get -1 month total honeypot.
    $spammaster_spam_honeypot_month2 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_honeypot_month2->fields('u', ['spamkey']);
    $spammaster_spam_honeypot_month2->where('(date LIKE :date AND spamkey = :honeypot)', [
      ':date' => $spam_master_month_minus_2 . '%',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_honeypot_month_result2 = $spammaster_spam_honeypot_month2->countQuery()->execute()->fetchField();
    // Get -2 month total honeypot.
    $spammaster_spam_honeypot_month3 = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_honeypot_month3->fields('u', ['spamkey']);
    $spammaster_spam_honeypot_month3->where('(date LIKE :date AND spamkey = :honeypot)', [
      ':date' => $spam_master_month_minus_3 . '%',
      ':honeypot' => 'spammaster-honeypot',
    ]);
    $spammaster_honeypot_month_result3 = $spammaster_spam_honeypot_month3->countQuery()->execute()->fetchField();

    return [
      '#theme' => 'statistics',
      '#attached' => [
        'library' => [
          'spammaster/spammaster-styles',
        ],
      ],
      '#spam_master_today_minus_1' => $spam_master_today_minus_1,
      '#spam_master_today_minus_2' => $spam_master_today_minus_2,
      '#spam_master_today_minus_3' => $spam_master_today_minus_3,
      '#spam_master_today_minus_4' => $spam_master_today_minus_4,
      '#spam_master_today_minus_5' => $spam_master_today_minus_5,
      '#totalitems_all_logs_1' => $spammaster_spam_all_logs_result1,
      '#totalitems_all_logs_2' => $spammaster_spam_all_logs_result2,
      '#totalitems_all_logs_3' => $spammaster_spam_all_logs_result3,
      '#totalitems_all_logs_4' => $spammaster_spam_all_logs_result4,
      '#totalitems_all_logs_5' => $spammaster_spam_all_logs_result5,
      '#totalitems_system_logs_1' => $spammaster_spam_system_logs_result1,
      '#totalitems_system_logs_2' => $spammaster_spam_system_logs_result2,
      '#totalitems_system_logs_3' => $spammaster_spam_system_logs_result3,
      '#totalitems_system_logs_4' => $spammaster_spam_system_logs_result4,
      '#totalitems_system_logs_5' => $spammaster_spam_system_logs_result5,
      '#totalitems_firewall_blocked_1' => $spammaster_spam_firewall_result1,
      '#totalitems_firewall_blocked_2' => $spammaster_spam_firewall_result2,
      '#totalitems_firewall_blocked_3' => $spammaster_spam_firewall_result3,
      '#totalitems_firewall_blocked_4' => $spammaster_spam_firewall_result4,
      '#totalitems_firewall_blocked_5' => $spammaster_spam_firewall_result5,
      '#totalitems_honeypot_blocked_1' => $spammaster_spam_honeypot_result1,
      '#totalitems_honeypot_blocked_2' => $spammaster_spam_honeypot_result2,
      '#totalitems_honeypot_blocked_3' => $spammaster_spam_honeypot_result3,
      '#totalitems_honeypot_blocked_4' => $spammaster_spam_honeypot_result4,
      '#totalitems_honeypot_blocked_5' => $spammaster_spam_honeypot_result5,
      '#total_count' => $total_count,
      '#total_count_all_logs' => $total_count_all_logs,
      '#total_count_system_logs' => $total_count_system_logs,
      '#total_count_firewall' => $total_count_firewall,
      '#total_count_honeypot' => $total_count_honeypot,
      '#spam_master_month_minus_1' => $spam_master_month_minus_1,
      '#spam_master_month_minus_2' => $spam_master_month_minus_2,
      '#spam_master_month_minus_3' => $spam_master_month_minus_3,
      '#total_month_all_logs_1' => $spammaster_all_logs_month_result1,
      '#total_month_all_logs_2' => $spammaster_all_logs_month_result2,
      '#total_month_all_logs_3' => $spammaster_all_logs_month_result3,
      '#total_month_system_logs_1' => $spammaster_system_logs_month_result1,
      '#total_month_system_logs_2' => $spammaster_system_logs_month_result2,
      '#total_month_system_logs_3' => $spammaster_system_logs_month_result3,
      '#total_month_firewall_1' => $spammaster_firewall_month_result1,
      '#total_month_firewall_2' => $spammaster_firewall_month_result2,
      '#total_month_firewall_3' => $spammaster_firewall_month_result3,
      '#total_month_honeypot_1' => $spammaster_honeypot_month_result1,
      '#total_month_honeypot_2' => $spammaster_honeypot_month_result2,
      '#total_month_honeypot_3' => $spammaster_honeypot_month_result3,
    ];
  }

}
