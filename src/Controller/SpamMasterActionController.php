<?php

namespace Drupal\spammaster\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\spammaster\SpamMasterActionService;

/**
 * Class controller.
 */
class SpamMasterActionController extends ControllerBase {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The SpamMasterActionService Service.
   *
   * @var \Drupal\spammaster\SpamMasterActionService
   */
  protected $actionService;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, RequestStack $requestStack, ConfigFactoryInterface $configFactory, StateInterface $state, SpamMasterActionService $actionService) {
    $this->connection = $connection;
    $this->requestStack = $requestStack;
    $this->configFactory = $configFactory;
    $this->state = $state;
    $this->actionService = $actionService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('request_stack'),
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('spammaster.action_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterToAct() {

    $spammaster_settings = $this->configFactory->getEditable('spammaster.settings');
    $spammaster_license = $spammaster_settings->get('spammaster.license_key');
    $spammaster_db_protection_hash = $this->state->get('spammaster.spam_master_db_protection_hash');
    $this->state->set('spammaster.new_options', '1');
    // Get current request.
    $spamPost = $this->requestStack->getCurrentRequest();
    // Check method.
    $spamMethod = $spamPost->getMethod();
    if ('POST' === $spamMethod) {
      // Get post content, fosdasse este caralho partiu-me a tola.
      $spamContent = $spamPost->getContent();
      $spamParams = json_decode($spamContent, TRUE);
      // Check first parameter.
      if (isset($spamParams['k']) && isset($spamParams['h'])) {
        $spammasterOne = htmlentities($spamParams['k'], ENT_QUOTES, 'UTF-8');
        $spammasterTwo = htmlentities($spamParams['h'], ENT_QUOTES, 'UTF-8');
        if (isset($spamParams['v'])) {
          $spammasterThree = htmlentities($spamParams['v'], ENT_QUOTES, 'UTF-8');
        }
        else {
          $spammasterThree = '0';
        }
        if (empty($spammasterOne)) {
          return new JsonResponse([
            'error' => $this->t('Silence is Golden. Request k.'),
            'status' => 401,
          ]);
        }
        if (empty($spammasterTwo)) {
          return new JsonResponse([
            'error' => $this->t('Silence is Golden. Request h.'),
            'status' => 401,
          ]);
        }
        if (!empty($spammasterOne) && !empty($spammasterTwo) && '0' === $spammasterThree) {
          if ($spammasterOne == $spammaster_license && $spammasterTwo == $spammaster_db_protection_hash) {
            // Call action service.
            $spama = '1';
            $spammaster_action_service = $this->actionService;
            $spammaster_action_service->spamMasterAct($spama);
            return new JsonResponse([
              'message' => $this->t('Successful Transfer.'),
              'status' => 200,
            ]);
          }
          else {
            return new JsonResponse([
              'error' => $this->t('Not able to proceed in second order.'),
              'status' => 401,
            ]);
          }
        }
        if (!empty($spammasterOne) && !empty($spammasterTwo) && '1' === $spammasterThree) {
          if ($spammasterOne == $spammaster_license && $spammasterTwo == $spammaster_db_protection_hash) {
            // Process stats.
            $exempt_count = [];
            // Process version.
            $spammaster_version = $this->state->get('spammaster.version');
            $db_install_version = $this->state->get('spammaster.db_install_version');
            $spammaster_subtype = $spammaster_settings->get('spammaster.subtype');
            $exempt_count['Statistics'][] = [
              'Version'  => $spammaster_version . '-' . $db_install_version . '-' . $spammaster_subtype,
            ];
            // Process status.
            $spammaster_status = $this->state->get('spammaster.license_status');
            $exempt_count['Statistics'][] = [
              'Status'  => $spammaster_status,
            ];
            // Get editable.
            $spammaster_settings_protection = $this->configFactory->getEditable('spammaster.settings_protection');
            // Process firewall rules.
            $basic_firewall_rules = $spammaster_settings_protection->get('spammaster.basic_firewall_rules');
            $exempt_count['Statistics'][] = [
              'Firewall'  => $basic_firewall_rules,
            ];
            // Process cdn status.
            $spam_master_is_cloudflare = $spammaster_settings_protection->get('spammaster.spam_master_is_cloudflare');
            $exempt_count['Statistics'][] = [
              'CDN'  => $spam_master_is_cloudflare,
            ];
            // Process buffer count.
            $spammaster_buffer_count = $this->connection->select('spammaster_threats', 'u');
            $spammaster_buffer_count->fields('u', ['threat']);
            $spammaster_buffer_query = $spammaster_buffer_count->countQuery()->execute()->fetchField();
            $exempt_count['Statistics'][] = [
              'Buffer'  => $spammaster_buffer_query,
            ];
            // Process white count.
            $spammaster_white_count = $this->connection->select('spammaster_white', 'u');
            $spammaster_white_count->fields('u', ['white']);
            $spammaster_white_query = $spammaster_white_count->countQuery()->execute()->fetchField();
            $exempt_count['Statistics'][] = [
              'White'  => $spammaster_white_query,
            ];
            // Process needles count.
            $spammaster_exempt_count = $this->connection->select('spammaster_keys', 'u');
            $spammaster_exempt_count->condition('u.spamkey', '%' . $this->connection->escapeLike('exempt') . '%', 'LIKE');
            $spammaster_exempt_count->fields('u', ['spamkey']);
            $spammaster_exempt_query = $spammaster_exempt_count->countQuery()->execute()->fetchField();
            $exempt_count['Statistics'][] = [
              'Needles'  => $spammaster_exempt_query,
            ];
            // Process all keys count.
            $spammaster_keys_count = $this->connection->select('spammaster_keys', 'u');
            $spammaster_keys_count->condition('u.spamkey', '%' . $this->connection->escapeLike('exempt') . '%', 'NOT LIKE');
            $spammaster_keys_count->fields('u', ['spamkey']);
            $spammaster_keys_query = $spammaster_keys_count->countQuery()->execute()->fetchField();
            $exempt_count['Statistics'][] = [
              'Keys'  => $spammaster_keys_query,
            ];
            // Process white transient.
            $spammaster_white_transient = $this->state->get('spammaster.white_transient');
            // Process white transient haf.
            $spammaster_wth_count = $this->connection->select('spammaster_keys', 'u');
            $spammaster_wth_count->condition('u.spamkey', 'white-transient-haf', '=');
            $spammaster_wth_count->fields('u', ['spamkey']);
            $spammaster_wth_query = $spammaster_wth_count->countQuery()->execute()->fetchField();
            // Process white transient form.
            $spammaster_wtf_count = $this->connection->select('spammaster_keys', 'u');
            $spammaster_wtf_count->condition('u.spamkey', 'white-transient-form', '=');
            $spammaster_wtf_count->fields('u', ['spamkey']);
            $spammaster_wtf_query = $spammaster_wtf_count->countQuery()->execute()->fetchField();
            $exempt_count['Statistics'][] = [
              'White Tran'  => $spammaster_white_transient,
              'White TranH' => $spammaster_wth_query,
              'White TranF' => $spammaster_wtf_query,
            ];
            // Process firewall retention days.
            $cleanup_firewall = $spammaster_settings_protection->get('spammaster.cleanup_firewall');
            // Process firewall retention total.
            $cleanup_firewall_count = $this->connection->select('spammaster_keys', 'u');
            $cleanup_firewall_count->condition('u.spamkey', 'spammaster-firewall', '=');
            $cleanup_firewall_count->fields('u', ['spamkey']);
            $cleanup_firewall_query = $cleanup_firewall_count->countQuery()->execute()->fetchField();
            // Process honeypot retention days.
            $cleanup_honeypot = $spammaster_settings_protection->get('spammaster.cleanup_honeypot');
            // Process honeypot retention total.
            $cleanup_honeypot_count = $this->connection->select('spammaster_keys', 'u');
            $cleanup_honeypot_count->condition('u.spamkey', 'spammaster-honeypot', '=');
            $cleanup_honeypot_count->fields('u', ['spamkey']);
            $cleanup_honeypot_query = $cleanup_honeypot_count->countQuery()->execute()->fetchField();
            // Process whitelist retention days.
            $cleanup_whitelist = $spammaster_settings_protection->get('spammaster.cleanup_whitelist');
            // Process whitelist retention total.
            $cleanup_whitelist_count = $this->connection->select('spammaster_keys', 'u');
            $cleanup_whitelist_count->condition('u.spamkey', 'spammaster-whitelist', '=');
            $cleanup_whitelist_count->fields('u', ['spamkey']);
            $cleanup_whitelist_query = $cleanup_whitelist_count->countQuery()->execute()->fetchField();
            // Process system retention days.
            $cleanup_system = $spammaster_settings_protection->get('spammaster.cleanup_system');
            // Process system retention total.
            $cleanup_system_count = $this->connection->select('spammaster_keys', 'u');
            $cleanup_system_count->condition('u.spamkey', 'spammaster', '=');
            $cleanup_system_count->fields('u', ['spamkey']);
            $cleanup_system_query = $cleanup_system_count->countQuery()->execute()->fetchField();
            // Process license retention total.
            $cleanup_license_count = $this->connection->select('spammaster_keys', 'u');
            $cleanup_license_count->condition('u.spamkey', 'spammaster-license', '=');
            $cleanup_license_count->fields('u', ['spamkey']);
            $cleanup_license_query = $cleanup_license_count->countQuery()->execute()->fetchField();
            // Process mail retention days.
            $cleanup_mail = $spammaster_settings_protection->get('spammaster.cleanup_mail');
            // Process mail retention total.
            $cleanup_mail_count = $this->connection->select('spammaster_keys', 'u');
            $cleanup_mail_count->condition('u.spamkey', 'spammaster-mail', '=');
            $cleanup_mail_count->fields('u', ['spamkey']);
            $cleanup_mail_query = $cleanup_mail_count->countQuery()->execute()->fetchField();
            // Process cron retention days.
            $cleanup_cron = $spammaster_settings_protection->get('spammaster.cleanup_cron');
            // Process cron retention total.
            $cleanup_cron_count = $this->connection->select('spammaster_keys', 'u');
            $cleanup_cron_count->condition('u.spamkey', 'spammaster-cron', '=');
            $cleanup_cron_count->fields('u', ['spamkey']);
            $cleanup_cron_query = $cleanup_cron_count->countQuery()->execute()->fetchField();
            $exempt_count['Statistics'][] = [
              'Clean FirD'  => $cleanup_firewall,
              'Clean FirT'  => $cleanup_firewall_query,
              'Clean HonD'  => $cleanup_honeypot,
              'Clean HonT'  => $cleanup_honeypot_query,
              'Clean WhiD'  => $cleanup_whitelist,
              'Clean WhiT'  => $cleanup_whitelist_query,
              'Clean SysD'  => $cleanup_system,
              'Clean SysT'  => $cleanup_system_query,
              'Clean LicT'  => $cleanup_license_query,
              'Clean MaiD'  => $cleanup_mail,
              'Clean MaiT'  => $cleanup_mail_query,
              'Clean CroD'  => $cleanup_cron,
              'Clean CroT'  => $cleanup_cron_query,
            ];
            // Process exempt needles.
            $exempt_needle = [];
            $spamFormIdnee = 'search_form';
            $spammaster_test_exempt_needle = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND POSITION(spamvalue IN :exclude) > :value", [
              ':key' => 'exempt-needle',
              ':exclude' => $spamFormIdnee,
              ':value' => 0,
            ])->fetchField();
            if (!empty($spammaster_test_exempt_needle)) {
              $exempt_needle['Exempt-Needles']['Locate'] = [
                'Value'  => 'search',
                'String' => $spamFormIdnee,
                'Result' => 'Found: ' . $spammaster_test_exempt_needle,
              ];
            }
            else {
              $exempt_needle['Exempt-Needles']['Locate'] = [
                'Value'  => 'search',
                'String' => $spamFormIdnee,
                'Result' => 'Not Found needle search',
              ];
            }
            $spammaster_exempt_needle = $this->connection->query("SELECT * FROM {spammaster_keys} WHERE spamkey = :key", [
              ':key' => 'exempt-needle',
            ])->fetchAll();
            if (!empty($spammaster_exempt_needle)) {
              foreach ($spammaster_exempt_needle as $needle) {
                $spam_id = $needle->id;
                $spam_time = $needle->date;
                $spam_key = $needle->spamkey;
                $spam_value = $needle->spamvalue;
                $exempt_needle['Exempt-Needles'][] = [
                  'id'        => $spam_id,
                  'date'      => $spam_time,
                  'spamkey'   => $spam_key,
                  'spamvalue' => $spam_value,
                ];
              }
            }
            // Process exempt keys.
            $exempt_key = [];
            $spampostarrkey = [
              'edit_quantity' => [
                '1',
                '2',
                '1',
              ],
              'form_build_id' => 'form-PSKMo16jLsqm_h6fhl35FI8BacqHmrX4jf3j_zhQ8do',
              'form_id' => 'views_form_commerce_cart_form_default_19728',
              'coupon_redemption' => [
                'code' => 'WBqFg65oFd',
                'url' => '',
                '_triggering_element_name' => 'apply_coupon',
              ],
              '_drupal_ajax' => '1',
            ];
            $spampoststrkey = str_replace('=', ' ', urldecode(http_build_query($spampostarrkey, '', ' ')));
            $spammaster_test_exempt_key = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND POSITION(spamvalue IN :exclude) > :value", [
              ':key' => 'exempt-key',
              ':exclude' => $spampoststrkey,
              ':value' => 0,
            ])->fetchField();
            if (!empty($spammaster_test_exempt_key)) {
              $exempt_key['Exempt-Keys']['Locate'] = [
                'Value'  => 'edit_quantity',
                'String' => $spampoststrkey,
                'Result' => 'Found: ' . $spammaster_test_exempt_key,
              ];
            }
            else {
              $exempt_key['Exempt-Keys']['Locate'] = [
                'Value'  => 'edit_quantity',
                'String' => $spampoststrkey,
                'Result' => 'Not Found key edit_quantity',
              ];
            }
            $spammaster_exempt_key = $this->connection->query("SELECT * FROM {spammaster_keys} WHERE spamkey = :key", [
              ':key' => 'exempt-key',
            ])->fetchAll();
            if (!empty($spammaster_exempt_key)) {
              foreach ($spammaster_exempt_key as $key) {
                $spam_id = $key->id;
                $spam_time = $key->date;
                $spam_key = $key->spamkey;
                $spam_value = $key->spamvalue;
                $exempt_key['Exempt-Keys'][] = [
                  'id'        => $spam_id,
                  'date'      => $spam_time,
                  'spamkey'   => $spam_key,
                  'spamvalue' => $spam_value,
                ];
              }
            }
            // Process exempt values.
            $exempt_value = [];
            $spampostarrval = [
              'edit_quantity' => [
                '1',
                '2',
                '1',
              ],
              'form_build_id' => 'form-PSKMo16jLsqm_h6fhl35FI8BacqHmrX4jf3j_zhQ8do',
              'form_id' => 'views_form_commerce_cart_form_default_19728',
              'coupon_redemption' => [
                'code' => 'WBqFg65oFd',
                'url' => '',
                '_triggering_element_name' => 'commerce_wishlist',
              ],
              '_drupal_ajax' => '1',
            ];
            $spampoststrval = str_replace('=', ' ', urldecode(http_build_query($spampostarrval, '', ' ')));
            $spammaster_test_exempt_value = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND POSITION(spamvalue IN :exclude) > :value", [
              ':key' => 'exempt-value',
              ':exclude' => $spampoststrval,
              ':value' => 0,
            ])->fetchField();
            if (!empty($spammaster_test_exempt_value)) {
              $exempt_value['Exempt-Values']['Locate'] = [
                'Value'  => 'commerce_wishlist',
                'String' => $spampoststrval,
                'Result' => 'Found: ' . $spammaster_test_exempt_value,
              ];
            }
            else {
              $exempt_value['Exempt-Values']['Locate'] = [
                'Value'  => 'commerce_wishlist',
                'String' => $spampoststrval,
                'Result' => 'Not Found key edit_quantity',
              ];
            }
            $spammaster_exempt_value = $this->connection->query("SELECT * FROM {spammaster_keys} WHERE spamkey = :key", [
              ':key' => 'exempt-value',
            ])->fetchAll();
            if (!empty($spammaster_exempt_value)) {
              foreach ($spammaster_exempt_value as $value) {
                $spam_id = $value->id;
                $spam_time = $value->date;
                $spam_key = $value->spamkey;
                $spam_value = $value->spamvalue;
                $exempt_value['Exempt-Values'][] = [
                  'id'        => $spam_id,
                  'date'      => $spam_time,
                  'spamkey'   => $spam_key,
                  'spamvalue' => $spam_value,
                ];
              }
            }

            $exempt_result = [
              $exempt_count,
              $exempt_needle,
              $exempt_key,
              $exempt_value,
            ];
            return new JsonResponse([
              'Exemptions' => $exempt_result,
              'status' => 200,
            ]);
          }
          else {
            return new JsonResponse([
              'error' => $this->t('Not able to proceed in third order.'),
              'status' => 401,
            ]);
          }
        }
      }
      else {
        return new JsonResponse([
          'error' => $this->t('Not able to proceed in forth order.'),
          'status' => 401,
        ]);
      }
    }
    else {
      return new JsonResponse([
        'error' => $this->t('Not able to proceed in fifth order.'),
        'status' => 401,
      ]);
    }
  }

}
