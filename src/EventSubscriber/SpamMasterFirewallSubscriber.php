<?php

namespace Drupal\spammaster\EventSubscriber;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\spammaster\SpamMasterKeyService;
use Drupal\spammaster\SpamMasterCollectService;
use Drupal\spammaster\SpamMasterHoneypotService;
use Drupal\spammaster\SpamMasterElusiveService;
use Drupal\spammaster\SpamMasterUpdaterService;

/**
 * Class firewall subscriber.
 */
class SpamMasterFirewallSubscriber implements EventSubscriberInterface {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The SpamMasterKeyService Service.
   *
   * @var \Drupal\spammaster\SpamMasterKeyService
   */
  protected $keyService;

  /**
   * The SpamMasterCollectService Service.
   *
   * @var \Drupal\spammaster\SpamMasterCollectService
   */
  protected $collectService;

  /**
   * The SpamMasterHoneypotService Service.
   *
   * @var \Drupal\spammaster\SpamMasterHoneypotService
   */
  protected $honeyService;

  /**
   * The SpamMasterElusiveService Service.
   *
   * @var \Drupal\spammaster\SpamMasterElusiveService
   */
  protected $elusiveService;

  /**
   * The SpamMasterUpdaterService Service.
   *
   * @var \Drupal\spammaster\SpamMasterUpdaterService
   */
  protected $updaterService;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, RequestStack $requestStack, AccountProxyInterface $currentUser, MessengerInterface $messenger, StateInterface $state, ConfigFactoryInterface $configFactory, SpamMasterKeyService $keyService, SpamMasterCollectService $collectService, SpamMasterHoneypotService $honeyService, SpamMasterElusiveService $elusiveService, SpamMasterUpdaterService $updaterService) {
    $this->connection = $connection;
    $this->requestStack = $requestStack;
    $this->currentUser = $currentUser;
    $this->messenger = $messenger;
    $this->state = $state;
    $this->configFactory = $configFactory;
    $this->keyService = $keyService;
    $this->collectService = $collectService;
    $this->honeyService = $honeyService;
    $this->elusiveService = $elusiveService;
    $this->updaterService = $updaterService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('request_stack'),
      $container->get('messenger'),
      $container->get('current_user'),
      $container->get('state'),
      $container->get('config.factory'),
      $container->get('spammaster.key_service'),
      $container->get('spammaster.collect_service'),
      $container->get('spammaster.honeypot_service'),
      $container->get('spammaster.elusive_service'),
      $container->get('spammaster.updater_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function checkForRedirection(RequestEvent $event) {

    $spammaster_ip = $this->requestStack->getCurrentRequest()->getClientIp();
    $spammaster_date = date("Y-m-d H:i:s");
    $spammaster_date_short = date("Y-m-d");
    // Set spam master state version.
    $spammaster_version = $this->state->get('spammaster.version');
    if (NULL === $spammaster_version || '2.59' !== $spammaster_version) {
      $spammaster_version = $this->state->set('spammaster.version', '2.59');
    }
    // Set db_install_version_new. Compare with updater set db_install_version.
    $db_install_version_new = $this->state->get('spammaster.db_install_version_new');
    if (NULL === $db_install_version_new || '257' !== $db_install_version_new) {
      $db_install_version_new = $this->state->set('spammaster.db_install_version_new', '257');
    }
    $db_install_version = $this->state->get('spammaster.db_install_version');
    // Take a look at updater service.
    if (NULL === $db_install_version || $db_install_version_new !== $db_install_version) {
      $spamUpDb = TRUE;
      $spammaster_updater_service = $this->updaterService;
      $spammaster_updater_service->spamMasterUpDb($spamUpDb);
    }
    // Prepare disc not.
    $spammaster_type = $this->state->get('spammaster.type');
    if ('FREE' === $spammaster_type) {
      $spammasterDiscDate = $spammaster_date_short;
      $spammaster_updater_service = $this->updaterService;
      $spammaster_updater_service->spamMasterDateCheck($spammasterDiscDate);
    }
    $spammaster_settings = $this->configFactory->getEditable('spammaster.settings');
    $spammaster_subtype = $spammaster_settings->get('spammaster.subtype');
    $spammaster_status = $this->state->get('spammaster.license_status');
    if (('VALID' === $spammaster_status || 'MALFUNCTION_1' === $spammaster_status || 'MALFUNCTION_2' === $spammaster_status) && 'prod' === $spammaster_subtype) {
      $spammaster_settings_protection = $this->configFactory->getEditable('spammaster.settings_protection');
      $basic_firewall_rules = $spammaster_settings_protection->get('spammaster.basic_firewall_rules');
      // Lets lazy check key and run clean up.
      $spam_master_license_sync_date = $this->state->get('spammaster.spam_master_license_sync_date');
      $spam_master_license_sync_run = $this->state->get('spammaster.spam_master_license_sync_run');
      $spam_master_sync_date_plus_days = date('Y-m-d', strtotime('+2 days', strtotime($spam_master_license_sync_date)));
      if ($spammaster_date_short >= $spam_master_sync_date_plus_days && '1' !== $spam_master_license_sync_run) {
        $updated_values = [
          'spammaster.spam_master_license_sync_date' => $spammaster_date_short,
          'spammaster.spam_master_license_sync_run' => '1',
        ];
        $this->state->setMultiple($updated_values);
        // Spam key controller.
        $spamKey = TRUE;
        $spamCron = 'LAZY';
        $spammaster_key_service = $this->keyService;
        $spammaster_key_service->spamMasterKeyLazy($spamKey, $spamCron);
      }

      // Do we have form_id.
      $spamelusive = $this->requestStack->getCurrentRequest()->request;
      if (NULL !== $spamelusive->get('form_id')) {
        $spamFormId = $spamelusive->get('form_id');
      }
      else {
        $spamFormId = FALSE;
      }
      // Exempt whitelist.
      $spammaster_white_query = $this->connection->query("SELECT white FROM {spammaster_white} WHERE white = :ip", [
        ':ip' => $spammaster_ip,
      ])->fetchField();
      $spammaster_wformid__query = $this->connection->query("SELECT white FROM {spammaster_white} WHERE white = :formid", [
        ':formid' => $spamFormId,
      ])->fetchField();
      if (!empty($spammaster_white_query) || !empty($spammaster_wformid__query)) {

        return;

      }
      else {
        $page = '/firewall';
        // White negative, proceed to buffer.
        $spammaster_db_ip = $this->connection->query("SELECT threat FROM {spammaster_threats} WHERE threat = :ip", [
          ':ip' => $spammaster_ip,
        ])->fetchField();
        $spammaster_anonymous = $this->currentUser->isAnonymous();
        if (!empty($spammaster_db_ip) && $spammaster_anonymous) {
          $this->connection->insert('spammaster_keys')->fields([
            'date' => $spammaster_date,
            'spamkey' => 'spammaster-firewall',
            'spamvalue' => 'Spam Master: firewall BUFFER BLOCK, Ip: ' . $spammaster_ip,
          ])->execute();

          $spammaster_total_block_count = $this->state->get('spammaster.total_block_count');
          $spammaster_total_block_count_1 = ++$spammaster_total_block_count;
          $this->state->set('spammaster.total_block_count', $spammaster_total_block_count_1);

          $this->requestStack->getCurrentRequest()->query->set('destination', $page);
          exit;

        }
        else {
          if (!empty($spamelusive)) {
            // Collect service call.
            $collectnow = $spammaster_ip;
            $spammaster_collect_service = $this->collectService;
            $is_collected = $spammaster_collect_service->spamMasterGetCollect($collectnow);
            $spamcollection = Json::decode($is_collected);
            $spammasterip = $spamcollection['spammasterip'];
            $spammasteragent = $spamcollection['spammasteragent'];
            $spammasterreferer = $spamcollection['spammasterreferer'];
            $spammasterurl = $spamcollection['spammasterurl'];
            $spammasteruserid = 'none';
            if (NULL !== $spamelusive->get('spammaster_extra_field_1')) {
              $spammasterextrafield1 = $spamelusive->get('spammaster_extra_field_1');
            }
            else {
              $spammasterextrafield1 = FALSE;
            }
            if (NULL !== $spamelusive->get('spammaster_extra_field_2')) {
              $spammasterextrafield2 = $spamelusive->get('spammaster_extra_field_2');
            }
            else {
              $spammasterextrafield2 = FALSE;
            }
            if (!empty($spammasterextrafield1) || !empty($spammasterextrafield2)) {
              if (empty($spammasterextrafield1)) {
                $spammasterextrafield1 = 'empty';
              }
              if (empty($spammasterextrafield2)) {
                $spammasterextrafield2 = 'empty';
              }
              // Honeypot service call.
              $spammaster_honeypot_service = $this->honeyService;
              $spammaster_honeypot_service->spamMasterHoneypotCheck($spamFormId, $spammasterip, $spammasteragent, $spammasterextrafield1, $spammasterextrafield2, $spammasterreferer, $spammasterurl, $spammasteruserid);

              $this->requestStack->getCurrentRequest()->query->set('destination', $page);
              exit;
            }
            if ('1' === $basic_firewall_rules || '2' === $basic_firewall_rules) {
              $spammaster_elusive_service = $this->elusiveService;
              $isElusive = $spammaster_elusive_service->spamMasterElusiveCheck($spamelusive);
              if ('ELUSIVE' === $isElusive) {

                $this->requestStack->getCurrentRequest()->query->set('destination', $page);
                exit;

              }
              else {
                return;
              }
            }
            else {
              return;
            }
          }
          else {
            return;
          }
        }
      }
    }
    else {
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {

    $events[KernelEvents::REQUEST][] = ['checkForRedirection'];

    return $events;
  }

}
