<?php

namespace Drupal\spammaster;

use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;

/**
 * Class service.
 */
class SpamMasterCollectService {

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The form class.
   *
   * @var \Drupal\Core\Form\FormInterface
   */
  protected $collectnow;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $requestStack, ConfigFactoryInterface $configFactory) {
    $this->requestStack = $requestStack;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterGetCollect($collectnow) {

    $this->collectnow = $collectnow;
    $spammaster_settings_protection = $this->configFactory->getEditable('spammaster.settings_protection');
    $basic_firewall_rules = $spammaster_settings_protection->get('spammaster.basic_firewall_rules');
    // Get remote ip.
    $spammasterip = substr($this->requestStack->getCurrentRequest()->getClientIp(), 0, 48);
    // Get remote agent.
    if ('1' === $basic_firewall_rules) {
      if (isset($_SERVER['HTTP_USER_AGENT'])) {
        $spammasteragent = substr(htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8'), 0, 360);
      }
      else {
        $spammasteragent = 'Sniffer';
      }
    }
    else {
      if (isset($_SERVER['HTTP_USER_AGENT'])) {
        $spammasteragent = substr('Relaxed - ' . htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8'), 0, 360);
      }
      else {
        $spammasteragent = 'Relaxed - Sniffer';
      }
    }
    // Get remote referrer.
    if (isset($_SERVER['HTTP_REFERER'])) {
      $spammasterreferer = substr(htmlentities($_SERVER['HTTP_REFERER'], ENT_QUOTES, 'UTF-8'), 0, 360);
    }
    else {
      $spammasterreferer = 'Direct';
    }
    // Get destination url.
    if (isset($_SERVER['REQUEST_SCHEME'])) {
      $spam_request_scheme = htmlentities($_SERVER['REQUEST_SCHEME'], ENT_QUOTES, 'UTF-8');
    }
    else {
      $spam_request_scheme = 'https';
    }
    if (isset($_SERVER['SERVER_NAME'])) {
      $spam_server_name = htmlentities($_SERVER['SERVER_NAME'], ENT_QUOTES, 'UTF-8');
    }
    else {
      $spam_server_name = 'false';
    }
    if (isset($_SERVER['REQUEST_URI'])) {
      $spam_request_uri = htmlentities($_SERVER['REQUEST_URI'], ENT_QUOTES, 'UTF-8');
    }
    else {
      $spam_request_uri = '/false';
    }
    $spammasterurl = substr($spam_request_scheme . '://' . $spam_server_name . $spam_request_uri, 0, 360);

    $prespamcollection = [
      'spammasterip'      => $spammasterip,
      'spammasteragent'   => $spammasteragent,
      'spammasterreferer' => $spammasterreferer,
      'spammasterurl'     => $spammasterurl,
    ];
    $spamcollection = Json::encode($prespamcollection);

    return $spamcollection;

  }

}
