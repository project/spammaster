<?php

namespace Drupal\spammaster;

use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxy;
use GuzzleHttp\Client;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class service.
 */
class SpamMasterElusiveService {
  use StringTranslationTrait;

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The session account interface.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The String Translation..
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The SpamMasterElusiveService service.
   *
   * @var \Drupal\spammmaster\SpamMasterElusiveService
   */
  protected $spamelusive;

  /**
   * The SpamMasterCollectService Service.
   *
   * @var \Drupal\spammaster\SpamMasterCollectService
   */
  protected $collectService;

  /**
   * The SpamMasterUserService Service.
   *
   * @var \Drupal\spammaster\SpamMasterUserService
   */
  protected $userService;

  /**
   * The form class.
   *
   * @var \Drupal\Core\Form\FormInterface
   */
  protected $form;

  /**
   * The form class.
   *
   * @var \Drupal\Core\Form\FormInterface
   */
  protected $formstate;

  /**
   * The form class.
   *
   * @var \Drupal\Core\Form\FormInterface
   */
  protected $spamFormId;

  /**
   * The SpamMasterCleanUpService Service.
   *
   * @var \Drupal\spammaster\SpamMasterCleanUpService
   */
  protected $cleanUpService;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, MessengerInterface $messenger, AccountInterface $account, RequestStack $requestStack, Client $httpClient, ConfigFactoryInterface $configFactory, StateInterface $state, TranslationInterface $stringTranslation, AccountProxy $current_user, SpamMasterCollectService $collectService, SpamMasterUserService $userService, SpamMasterCleanUpService $cleanUpService) {
    $this->connection = $connection;
    $this->messenger = $messenger;
    $this->account = $account;
    $this->requestStack = $requestStack;
    $this->httpClient = $httpClient;
    $this->configFactory = $configFactory;
    $this->state = $state;
    $this->stringTranslation = $stringTranslation;
    $this->currentUser = $current_user;
    $this->collectService = $collectService;
    $this->userService = $userService;
    $this->cleanUpService = $cleanUpService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('messenger'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('string_translation'),
      $container->get('current_user'),
      $container->get('spammaster.collect_service'),
      $container->get('spammaster.user_service'),
      $container->get('spammaster.clean_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterElusiveCheck($spamelusive) {

    $this->spamelusive = $spamelusive;
    $spammaster_date = date("Y-m-d H:i:s");
    $spammaster_settings = $this->configFactory->getEditable('spammaster.settings');
    $spammaster_license = $spammaster_settings->get('spammaster.license_key');
    $spammaster_subtype = $spammaster_settings->get('spammaster.subtype');
    $spammaster_status = $this->state->get('spammaster.license_status');
    $spammaster_total_block_count = $this->state->get('spammaster.total_block_count');
    $spammasterWhiteTransient = $this->state->get('spammaster.white_transient');
    if (('VALID' === $spammaster_status || 'MALFUNCTION_1' === $spammaster_status || 'MALFUNCTION_2' === $spammaster_status) && 'prod' === $spammaster_subtype) {

      // Call collect service.
      $collectnow = $this->requestStack->getCurrentRequest()->getClientIp();
      $spammaster_collect_service = $this->collectService;
      $is_collected = $spammaster_collect_service->spamMasterGetCollect($collectnow);
      $spamcollection = Json::decode($is_collected);
      $spammasterip = $spamcollection['spammasterip'];
      $spammasteragent = $spamcollection['spammasteragent'];
      $spammasterreferer = $spamcollection['spammasterreferer'];
      $spammasterurl = $spamcollection['spammasterurl'];

      // User service call.
      $spamuser = $this->currentUser->id();
      $spammaster_user_service = $this->userService;
      $is_user = $spammaster_user_service->spamMasterGetUser($spamuser);
      $spamuserdata = Json::decode($is_user);
      $spammasteruserid = $spamuserdata['ID'];
      $spammasterusername = $spamuserdata['username'];

      // Do we have form_id.
      if (NULL !== $spamelusive->get('form_id')) {
        $spammasterIdForm = $spamelusive->get('form_id');
      }
      else {
        $spammasterIdForm = FALSE;
      }
      // Exempt whitelist.
      $spammaster_white_query = $this->connection->query("SELECT white FROM {spammaster_white} WHERE white = :ip", [
        ':ip' => $spammasterip,
      ])->fetchField();
      $spammaster_wformid__query = $this->connection->query("SELECT white FROM {spammaster_white} WHERE white = :formid", [
        ':formid' => $spammasterIdForm,
      ])->fetchField();
      // Check white transient if on.
      if ('1' === $spammasterWhiteTransient) {
        // Call clean-up service to delete old transients.
        $spammaster_cleanup_service = $this->cleanUpService;
        $spammaster_cleanup_service->spamMasterCleanUpTransNow($spammasterWhiteTransient);
        // Exempt transient whitelist.
        $spammaster_white_trans_query = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
          ':key' => 'white-transient-haf',
          ':value' => $spammasterip,
        ])->fetchField();
      }
      else {
        $spammaster_white_trans_query = FALSE;
      }
      // White positive, or administrator log insert.
      $user_roles = $this->account->getRoles();
      if (!empty($spammaster_white_query) || !empty($spammaster_wformid__query) || !empty($spammaster_white_trans_query) || in_array('administrator', $user_roles)) {
        return 'BAIL';
      }
      else {
        // Buffer db check.
        $spammaster_spam_buffer_query = $this->connection->query("SELECT threat FROM {spammaster_threats} WHERE threat = :ip", [
          ':ip' => $spammasterip,
        ])->fetchField();
        // Buffer db positive, throw error, log insert.
        if (!empty($spammaster_spam_buffer_query)) {
          $spammaster_total_block_count_1 = ++$spammaster_total_block_count;
          $this->state->set('spammaster.total_block_count', $spammaster_total_block_count_1);
          $this->connection->insert('spammaster_keys')->fields([
            'date' => $spammaster_date,
            'spamkey' => 'spammaster-firewall',
            'spamvalue' => 'Spam Master: elusive BUFFER BLOCK, Ip: ' . $spammasterip . ', Agent: ' . $spammasteragent . ', User ID: ' . $spammasteruserid . ', Username: ' . $spammasterusername,
          ])->execute();
          return 'ELUSIVE';
        }
        else {

          // Exclude malformed or internal links.
          if (!empty($spammasterurl)) {
            if (stripos($spammasterurl, 'autodiscover.xml') !== FALSE) {
              return 'BAIL';
            }
          }

          // phpcs:ignore DrupalPractice.Variables.GetRequestData.SuperglobalAccessed
          $spampost = $_POST;
          if (!empty($spampost) && is_array($spampost)) {
            // Exclude system, search and views.
            if (preg_match('/[^a-zA-Z]system_/', $spammasterIdForm) !== 0 && preg_match('/[^a-zA-Z]search_/', $spammasterIdForm) !== 0 && preg_match('/[^a-zA-Z]views_exposed_form_/', $spammasterIdForm) !== 0) {
              return 'BAIL';
            }
            // Exempt needle.
            $spammaster_exempt_needle = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND POSITION(spamvalue IN :exclude) > :value", [
              ':key' => 'exempt-needle',
              ':exclude' => $spammasterIdForm,
              ':value' => 0,
            ])->fetchField();
            if (!empty($spammaster_exempt_needle)) {
              return 'BAIL';
            }

            $spampoststr = str_replace('=', ' ', urldecode(http_build_query($spampost, '', ' ')));
            // Exclusions by key.
            $spammaster_exempt_key = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND POSITION(spamvalue IN :exclude) > :value", [
              ':key' => 'exempt-key',
              ':exclude' => $spampoststr,
              ':value' => 0,
            ])->fetchField();
            if (!empty($spammaster_exempt_key)) {
              return 'BAIL';
            }
            // Exclusions by value.
            $spammaster_exempt_value = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND POSITION(spamvalue IN :exclude) > :value", [
              ':key' => 'exempt-value',
              ':exclude' => $spampoststr,
              ':value' => 0,
            ])->fetchField();
            if (!empty($spammaster_exempt_value)) {
              return 'BAIL';
            }
            // Web api check. Create data to be posted.
            $blog_threat_type = 'HAF';
            $result_post_content_json  = Json::encode($spampost);
            $result_post_content_trim  = substr($result_post_content_json, 0, 963);
            $result_post_content_clean = strip_tags($result_post_content_trim);
            if (empty($result_post_content_clean)) {
              $result_post_content_clean = 'is_elusive_d';
            }
            // Collect email to scan.
            preg_match('/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/i', $spampoststr, $matches);
            if ($matches) {
              foreach ($matches as $key => $val) {
                $resultKey = $key;
                if ($resultKey) {
                }
                if (filter_var($val, FILTER_VALIDATE_EMAIL)) {
                  $spammasteremail = substr($val, 0, 256);
                  break;
                }
                else {
                  $spammasteremail = 'haf@' . rand(10000000, 99999999) . '.drup';
                }
              }
            }
            else {
              $spammasteremail = 'haf@' . rand(10000000, 99999999) . '.drup';
            }
            // Proceed with post.
            $blog_web_address = $this->requestStack->getCurrentRequest()->getHost();
            $address_unclean = $blog_web_address;
            $address = preg_replace('#^https?://#', '', $address_unclean);
            @$blog_server_ip = $_SERVER['SERVER_ADDR'];
            // If empty ip.
            if (empty($blog_server_ip) || $blog_server_ip == '0') {
              @$blog_server_ip = 'I ' . gethostbyname($_SERVER['SERVER_NAME']);
            }
            $spam_master_leaning_url = 'https://www.spammaster.org/core/learn/get_learn_haf.php';

            // Call drupal hhtpclient.
            $client = $this->httpClient;
            // Post data.
            $request = $client->post($spam_master_leaning_url, [
              'form_params' => [
                'blog_license_key'    => $spammaster_license,
                'blog_threat_ip'      => $spammasterip,
                'blog_threat_user'    => $is_user,
                'blog_threat_type'    => $blog_threat_type,
                'blog_threat_email'   => $spammasteremail,
                'blog_threat_content' => 'is_elusive_d - ' . $result_post_content_clean,
                'blog_threat_agent'   => $spammasteragent,
                'blog_threat_refe'    => $spammasterreferer,
                'blog_threat_dest'    => $spammasterurl,
                'blog_web_adress'     => $address,
                'blog_server_ip'      => $blog_server_ip,
              ],
            ]);
            // Decode json data or $request->getBody()->getContents().
            $response = Json::decode($request->getBody());
            if (empty($response['threat'])) {
              // Set white transient if on.
              if ('1' === $spammasterWhiteTransient) {
                $this->connection->insert('spammaster_keys')->fields([
                  'date' => $spammaster_date,
                  'spamkey' => 'white-transient-haf',
                  'spamvalue' => $spammasterip,
                ])->execute();
              }
              return 'BAIL';
            }
            else {
              // Insert ip into buffer db.
              $this->connection->insert('spammaster_threats')->fields([
                'date' => $spammaster_date,
                'threat' => $spammasterip,
              ])->execute();
              // Web positive, return.
              $spammaster_total_block_count_1 = ++$spammaster_total_block_count;
              $this->state->set('spammaster.total_block_count', $spammaster_total_block_count_1);
              $this->connection->insert('spammaster_keys')->fields([
                'date' => $spammaster_date,
                'spamkey' => 'spammaster-firewall',
                'spamvalue' => 'Spam Master: firewall RBL BLOCK, Ip: ' . $spammasterip . ', Agent: ' . $spammasteragent . ', User ID: ' . $spammasteruserid . ', Username: ' . $spammasterusername,
              ])->execute();
              return 'ELUSIVE';
            }
          }
          else {
            return 'BAIL';
          }
        }
      }
    }
    else {
      return 'BAIL';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterIsNeedle($spamFormId) {

    // Force needle straw.
    $spammaster_exempt_needle_straw = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND POSITION(spamvalue IN :straw) > :value", [
      ':key' => 'exempt-needle-straw',
      ':straw' => $spamFormId,
      ':value' => 0,
    ])->fetchField();
    if (!empty($spammaster_exempt_needle_straw)) {
      return "STRAW";
    }
    else {
      // Hide sig but force needle.
      $spammaster_exempt_needle_hide_sig = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND POSITION(spamvalue IN :sighide) > :value", [
        ':key' => 'exempt-needle-sig-hide',
        ':sighide' => $spamFormId,
        ':value' => 0,
      ])->fetchField();
      if (!empty($spammaster_exempt_needle_hide_sig)) {
        return "SIGHIDDEN";
      }
      else {
        // Show sig but exempt needle.
        $spammaster_exempt_needle_show_sig = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND POSITION(spamvalue IN :sigshow) > :value", [
          ':key' => 'exempt-needle-sig-show',
          ':sigshow' => $spamFormId,
          ':value' => 0,
        ])->fetchField();
        if (!empty($spammaster_exempt_needle_show_sig)) {
          return "SIGSHOW";
        }
        else {
          // Exempt needle.
          $spammaster_exempt_needle = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND POSITION(spamvalue IN :exclude) > :value", [
            ':key' => 'exempt-needle',
            ':exclude' => $spamFormId,
            ':value' => 0,
          ])->fetchField();
          if (!empty($spammaster_exempt_needle)) {
            return "NEEDLE";
          }
          else {
            return "STRAW";
          }
        }
      }
    }
  }

}
