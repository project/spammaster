<?php

namespace Drupal\spammaster;

use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use GuzzleHttp\Client;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class service.
 */
class SpamMasterLicService {
  use StringTranslationTrait;

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The SpamMasterMailService Service.
   *
   * @var \Drupal\spammaster\SpamMasterMailService
   */
  protected $mailService;

  /**
   * The SpamMasterLicService service.
   *
   * @var \Drupal\spammmaster\SpamMasterLicService
   */
  protected $spamKey;

  /**
   * The SpamMasterLicService service.
   *
   * @var \Drupal\spammmaster\SpamMasterLicService
   */
  protected $spamCron;

  /**
   * The SpamMasterActionService Service.
   *
   * @var \Drupal\spammaster\SpamMasterActionService
   */
  protected $actionService;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, MessengerInterface $messenger, RequestStack $requestStack, Client $httpClient, ConfigFactoryInterface $configFactory, StateInterface $state, LanguageManagerInterface $language_manager, SpamMasterMailService $mailService, SpamMasterActionService $actionService) {
    $this->connection = $connection;
    $this->messenger = $messenger;
    $this->requestStack = $requestStack;
    $this->httpClient = $httpClient;
    $this->configFactory = $configFactory;
    $this->state = $state;
    $this->languageManager = $language_manager;
    $this->mailService = $mailService;
    $this->actionService = $actionService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('language_manager'),
      $container->get('spammaster.mail_service'),
      $container->get('spammaster.action_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterLicManualCreation() {

    // Process key data.
    $keyData = $this->processKeyData();
    // Prepare date.
    $spammaster_dt = date('Y-m-d H:i:s');
    $spamCron = "MAN";

    // Encode ssl post link security.
    $spammaster_license_url = 'https://www.spammaster.org/core/lic/get_lic.php';

    // Call drupal hhtpclient.
    $client = $this->httpClient;
    // Post data.
    $request = $client->post($spammaster_license_url, [
      'form_params' => [
        'spam_license_key'    => $keyData['system_data']['spammaster_license'],
        'platform'            => $keyData['system_data']['spammaster_platform'],
        'platform_version'    => $keyData['system_data']['spammaster_platform_version'],
        'platform_type'       => $keyData['system_data']['spammaster_multisite_joined'],
        'spam_master_version' => $keyData['system_data']['spammaster_version'],
        'spam_master_type'    => $keyData['system_data']['spammaster_type'],
        'blog_name'           => $keyData['system_data']['spammaster_site_name'],
        'blog_address'        => $keyData['system_data']['address'],
        'blog_email'          => $keyData['system_data']['spammaster_admin_email'],
        'blog_hostname'       => $keyData['soft_hard']['spammaster_hostname'],
        'blog_ip'             => $keyData['soft_hard']['spammaster_ip'],
        'blog_up'             => $keyData['system_data']['spam_master_auto_update'],
        'blog_cloud'          => $keyData['system_data']['spam_master_is_cloudflare'],
        'spam_master_db'      => $keyData['hash_protection'],
        'spam_master_logs'    => $keyData['health_view'],
        'spam_master_cron'    => $spamCron,
      ],
    ]);
    // Decode json data.
    $response = Json::decode($request->getBody());
    if (empty($response)) {
      $spammaster_status = FALSE;
    }
    else {
      $spammaster_status = $response['status'];
      if ('MALFUNCTION_3' === $spammaster_status) {
        $spammaster_type_set = 'MALFUNCTION_3';
        $spammaster_protection_total_number = 'MALFUNCTION_3';
        $spammaster_alert_level_received = 'MALFUNCTION_3';
        $spammaster_alert_level_p_text = 'MALFUNCTION_3';
        $spam_master_attached = '';
      }
      else {
        $spammaster_type_set = $response['type'];
        $spammaster_protection_total_number = $response['threats'];
        $spammaster_alert_level_received = $response['alert'];
        $spammaster_alert_level_p_text = $response['percent'];
        $spam_master_attached = $response['attached'];

        $spama = $response['a'];
        if ('1' === $spama) {
          $this->state->set('spammaster.new_options', '1');
          $this->connection->insert('spammaster_keys')->fields([
            'date' => $spammaster_dt,
            'spamkey' => 'spammaster',
            'spamvalue' => 'Spam Master: Action service called via license manual run.',
          ])->execute();
          // Spam action service.
          $spammaster_action_service = $this->actionService;
          $spammaster_action_service->spamMasterAct($spama);
        }
      }
      // Store state values.
      $manual_values = [
        'spammaster.type' => $spammaster_type_set,
        'spammaster.license_status' => $spammaster_status,
        'spammaster.license_alert_level' => $spammaster_alert_level_received,
        'spammaster.license_protection' => $spammaster_protection_total_number,
        'spammaster.license_probability' => $spammaster_alert_level_p_text,
        'spammaster.spam_master_attached' => $spam_master_attached,
      ];
      $this->state->setMultiple($manual_values);
    }

    // Display status to user.
    if ('INACTIVE' === $spammaster_status || 'MALFUNCTION_1' === $spammaster_status || 'MALFUNCTION_2' === $spammaster_status || 'MALFUNCTION_3' === $spammaster_status || 'MALFUNCTION_4' === $spammaster_status || 'MALFUNCTION_5' === $spammaster_status || 'MALFUNCTION_6' === $spammaster_status || 'MALFUNCTION_7' === $spammaster_status || 'MALFUNCTION_8' === $spammaster_status || 'UNSTABLE' === $spammaster_status || 'HIGH_VOLUME' === $spammaster_status || 'DISCONNECTED' === $spammaster_status || 'EXPIRED' === $spammaster_status || 'CODE_TAMPER_1' === $spammaster_status) {
      $this->messenger->addError($this->t('License key @spammaster_license status is: @spammaster_status. Check Spam Master configuration page and read more about statuses.', [
        '@spammaster_license' => $keyData['system_data']['spammaster_license'],
        '@spammaster_status' => $spammaster_status,
      ]));
      // Spam Master log.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spammaster_dt,
        'spamkey' => 'spammaster-license',
        'spamvalue' => 'Spam Master: license manual status check: ' . $spammaster_status,
      ])->execute();
    }
    else {
      // Log message.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spammaster_dt,
        'spamkey' => 'spammaster-license',
        'spamvalue' => 'Spam Master: license manual status check: ' . $spammaster_status,
      ])->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterLicDaily($spamKey, $spamCron) {

    // Process key data.
    $spammaster_license_status = $this->state->get('spammaster.license_status');
    $spammaster_settings = $this->configFactory->getEditable('spammaster.settings');
    $spammaster_subtype = $spammaster_settings->get('spammaster.subtype');

    if (('VALID' === $spammaster_license_status || 'MALFUNCTION_1' === $spammaster_license_status || 'MALFUNCTION_2' === $spammaster_license_status || 'UNSTABLE' === $spammaster_license_status || 'HIGH_VOLUME' === $spammaster_license_status || 'CODE_TAMPER_1' === $spammaster_license_status) && 'prod' === $spammaster_subtype) {
      // Colect data.
      $keyData = $this->processKeyData();
      // Prepare date.
      $spammaster_dt = date('Y-m-d H:i:s');
      if (empty($spamCron)) {
        $spamCron = "EMP";
      }

      // Encode ssl post link security.
      $spammaster_license_url = 'https://www.spammaster.org/core/lic/get_lic.php';

      // Call drupal hhtpclient.
      $client = $this->httpClient;
      // Post data.
      $request = $client->post($spammaster_license_url, [
        'form_params' => [
          'spam_license_key'    => $keyData['system_data']['spammaster_license'],
          'platform'            => $keyData['system_data']['spammaster_platform'],
          'platform_version'    => $keyData['system_data']['spammaster_platform_version'],
          'platform_type'       => $keyData['system_data']['spammaster_multisite_joined'],
          'spam_master_version' => $keyData['system_data']['spammaster_version'],
          'spam_master_type'    => $keyData['system_data']['spammaster_type'],
          'blog_name'           => $keyData['system_data']['spammaster_site_name'],
          'blog_address'        => $keyData['system_data']['address'],
          'blog_email'          => $keyData['system_data']['spammaster_admin_email'],
          'blog_hostname'       => $keyData['soft_hard']['spammaster_hostname'],
          'blog_ip'             => $keyData['soft_hard']['spammaster_ip'],
          'blog_up'             => $keyData['system_data']['spam_master_auto_update'],
          'blog_cloud'          => $keyData['system_data']['spam_master_is_cloudflare'],
          'spam_master_db'      => $keyData['hash_protection'],
          'spam_master_logs'    => $keyData['health_view'],
          'spam_master_cron'    => $spamCron,
        ],
      ]);
      // Decode json data.
      $response = Json::decode($request->getBody());
      if (empty($response)) {
        $spammaster_status = FALSE;
      }
      else {
        $spammaster_status = $response['status'];
        if ('MALFUNCTION_3' === $spammaster_status) {
          $spammaster_type_set = 'MALFUNCTION_3';
          $spammaster_protection_total_number = 'MALFUNCTION_3';
          $spammaster_alert_level_received = 'MALFUNCTION_3';
          $spammaster_alert_level_p_text = 'MALFUNCTION_3';
          $spam_master_attached = '';
        }
        else {
          $spammaster_type_set = $response['type'];
          $spammaster_protection_total_number = $response['threats'];
          $spammaster_alert_level_received = $response['alert'];
          $spammaster_alert_level_p_text = $response['percent'];
          $spam_master_attached = $response['attached'];

          $spama = $response['a'];
          if ('1' === $spama) {
            $this->state->set('spammaster.new_options', '1');
            $this->connection->insert('spammaster_keys')->fields([
              'date' => $spammaster_dt,
              'spamkey' => 'spammaster',
              'spamvalue' => 'Spam Master: Action service called via license cron.',
            ])->execute();
            // Spam action service.
            $spammaster_action_service = $this->actionService;
            $spammaster_action_service->spamMasterAct($spama);
          }
        }
        // Store state values.
        $daily_values = [
          'spammaster.type' => $spammaster_type_set,
          'spammaster.license_status' => $spammaster_status,
          'spammaster.license_alert_level' => $spammaster_alert_level_received,
          'spammaster.license_protection' => $spammaster_protection_total_number,
          'spammaster.license_probability' => $spammaster_alert_level_p_text,
          'spammaster.spam_master_attached' => $spam_master_attached,
        ];
        $this->state->setMultiple($daily_values);
      }

      // Call mail service for all requests.
      $spammaster_mail_service = $this->mailService;

      // Display status to user.
      if ('VALID' === $spammaster_status) {
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: cron license success. Status: ' . $spammaster_status,
        ])->execute();
      }
      else {
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: cron license warning. Status: ' . $spammaster_status,
        ])->execute();

        // Call mail service function.
        $spammaster_mail_service->spamMasterLicMalfunctions();
      }
      $spammaster_license_alert_level = $this->state->get('spammaster.license_alert_level');
      if ('ALERT_3' === $spammaster_license_alert_level) {
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_dt,
          'spamkey' => 'spammaster-cron',
          'spamvalue' => 'Spam Master: cron alert level 3 detected.',
        ])->execute();

        // Call mail service function.
        $spammaster_mail_service->spamMasterLicAlertLevel3();
      }
    }
  }

  /**
   * Process SpamMaster data.
   *
   * This includes server info, key and hash data.
   *
   * @return array
   *   SpamMaster data information.
   */
  private function processKeyData(): array {
    return [
      'system_data'     => $this->getSystemData(),
      'hash_protection' => $this->getHashKey(),
      'soft_hard'       => $this->getSoftHard(),
      'health_view'     => $this->getHealthView(),
    ];
  }

  /**
   * Get SpamMaster system data.
   *
   * @return array
   *   System information.
   */
  private function getSystemData(): array {
    $site_settings = $this->configFactory->getEditable('system.site');
    $spammaster_settings = $this->configFactory->getEditable('spammaster.settings');
    $spammaster_settings_protection = $this->configFactory->getEditable('spammaster.settings_protection');
    $spam_master_is_cloudflare = $spammaster_settings_protection->get('spammaster.spam_master_is_cloudflare');
    if (empty($spam_master_is_cloudflare) || '0' === $spam_master_is_cloudflare) {
      $spam_master_is_cloudflare = 'false';
    }
    if ('1' === $spam_master_is_cloudflare) {
      $spam_master_is_cloudflare = 'true';
    }
    $spam_master_auto_update = $this->state->get('spammaster.spam_master_auto_update');
    if (empty($spam_master_auto_update)) {
      $spam_master_auto_update = 'false';
      $this->state->set('spammaster.spam_master_auto_update', $spam_master_auto_update);
    }
    // Prepare lazy values.
    $spam_master_short_date = date('Y-m-d');
    $lazy_values = [
      'spammaster.spam_master_license_sync_date' => $spam_master_short_date,
      'spammaster.spam_master_license_sync_run' => '0',
    ];
    $this->state->setMultiple($lazy_values);
    // Prepare platform.
    $spammaster_platform_type = 'NO';
    $spammaster_n_websites = '0';
    $spammaster_multisite_joined = $spammaster_platform_type . ' - ' . $spammaster_n_websites;
    // Prepare address.
    $url_options = [
      'absolute' => TRUE,
      'language' => $this->languageManager->getCurrentLanguage(),
    ];
    $address_un = Url::fromRoute('<front>', [], $url_options)->toString();
    $address = rtrim($address_un, '/');
    if (empty($address)) {
      $address = 'https://' . date('YmdHis') . '.drup';
    }
    $basic_data = [
      'spammaster_license'              => $spammaster_settings->get('spammaster.license_key'),
      'spammaster_platform'             => 'Drupal',
      'spammaster_platform_version'     => \Drupal::VERSION,
      'spammaster_multisite_joined'     => $spammaster_multisite_joined,
      'spammaster_site_name'            => $site_settings->get('name'),
      'address'                         => $address,
      'spammaster_admin_email'          => $site_settings->get('mail'),
      'spammaster_version'              => $this->state->get('spammaster.version'),
      'spammaster_type'                 => $this->state->get('spammaster.type'),
      'spam_master_auto_update'         => $spam_master_auto_update,
      'spam_master_is_cloudflare'       => $spam_master_is_cloudflare,
    ];
    return $basic_data;
  }

  /**
   * Get SpamMaster encryption hash.
   *
   * @return string
   *   Gathered hash.
   */
  private function getHashKey() {
    $spammaster_dt = date('Y-m-d H:i:s');
    $spam_master_db_protection_hash_dt = $this->state->get('spammaster.spam_master_db_protection_hash_dt');
    $spam_master_db_protection_hash = $this->state->get('spammaster.spam_master_db_protection_hash');
    if (empty($spam_master_db_protection_hash_dt)) {
      $this->state->set('spammaster.spam_master_db_protection_hash_dt', $spammaster_dt);
      $spam_master_db_protection_hash_dt = $spammaster_dt;
    }
    // Process db protection hash date time.
    $plus_hash_dt = date('Y-m-d H:i:s', strtotime('+7 days', strtotime($spam_master_db_protection_hash_dt)));
    if ($spammaster_dt >= $plus_hash_dt) {
      // Refresh db protection hash.
      $spam_master_db_protection_hash = substr(md5(uniqid(mt_rand(), TRUE)), 0, 64);
      if (empty($spam_master_db_protection_hash)) {
        $spam_master_db_protection_hash = 'md5-' . date('YmdHis');
      }
      $new_hash_values = [
        'spammaster.spam_master_db_protection_hash_dt' => $spammaster_dt,
        'spammaster.spam_master_db_protection_hash' => $spam_master_db_protection_hash,
      ];
      $this->state->setMultiple($new_hash_values);
    }
    return $spam_master_db_protection_hash;
  }

  /**
   * Get Hardware, software information.
   *
   * @return array
   *   Hardware, software information.
   */
  private function getSoftHard(): array {
    if (isset($_SERVER['SERVER_ADDR'])) {
      $spammaster_ip = substr($_SERVER['SERVER_ADDR'], 0, 48);
      // Should bot be localhost.
      if (empty($spammaster_ip) || '0' === $spammaster_ip || '127.0.0.1' === $spammaster_ip) {
        if (isset($_SERVER['SERVER_NAME'])) {
          $spam_master_ip_gethostbyname = gethostbyname($_SERVER['SERVER_NAME']);
          $spammaster_ip = substr($spam_master_ip_gethostbyname, 0, 48);
          if (empty($spam_master_ip_gethostbyname) || '0' === $spam_master_ip_gethostbyname) {
            $spam_master_urlparts = parse_url($spammaster_site_url);
            $spam_master_hostname_temp = $spam_master_urlparts['host'];
            $spam_master_result = dns_get_record($spam_master_hostname_temp, DNS_A);
            $spammaster_ip = substr($spam_master_result[0]['ip'], 0, 48);
          }
        }
        else {
          $spammaster_ip = 'I 000';
        }
      }
      $spammaster_hostname = substr(gethostbyaddr($_SERVER['SERVER_ADDR']), 0, 256);
      if (empty($spammaster_hostname) || '0' === $spammaster_hostname || '127.0.0.1' === $spammaster_hostname) {
        if (isset($_SERVER['SERVER_NAME'])) {
          $spam_master_ho_gethostbyname = gethostbyname($_SERVER['SERVER_NAME']);
          $spammaster_hostname = substr($spam_master_ho_gethostbyname, 0, 256);
          if (empty($spam_master_ho_gethostbyname) || '0' === $spam_master_ho_gethostbyname) {
            $spam_master_urlparts = parse_url($spammaster_site_url);
            $spam_master_hostname_temp = $spam_master_urlparts['host'];
            $spam_master_result = dns_get_record($spam_master_hostname_temp, DNS_A);
            $spammaster_hostname = substr($spam_master_result[0]['ip'], 0, 256);
          }
        }
        else {
          $spammaster_hostname = 'H 000';
        }
      }
    }
    else {
      if (isset($_SERVER['SERVER_NAME'])) {
        $spam_master_ip_gethostbyname = gethostbyname($_SERVER['SERVER_NAME']);
        $spammaster_ip = substr($spam_master_ip_gethostbyname, 0, 48);
        if (empty($spam_master_ip_gethostbyname) || '0' === $spam_master_ip_gethostbyname) {
          $spam_master_urlparts = parse_url($spammaster_site_url);
          $spam_master_hostname_temp = $spam_master_urlparts['host'];
          $spam_master_result = dns_get_record($spam_master_hostname_temp, DNS_A);
          $spammaster_ip = substr($spam_master_result[0]['ip'], 0, 48);
        }
        else {
          $spammaster_ip = 'I 001';
        }
        $spam_master_ho_gethostbyname = gethostbyname($_SERVER['SERVER_NAME']);
        $spammaster_hostname = substr($spam_master_ho_gethostbyname, 0, 256);
        if (empty($spam_master_ho_gethostbyname) || '0' === $spam_master_ho_gethostbyname) {
          $spam_master_urlparts = parse_url($spammaster_site_url);
          $spam_master_hostname_temp = $spam_master_urlparts['host'];
          $spam_master_result = dns_get_record($spam_master_hostname_temp, DNS_A);
          $spammaster_hostname = substr($spam_master_result[0]['ip'], 0, 256);
        }
        else {
          $spammaster_hostname = 'H 001';
        }
      }
      else {
        $spammaster_ip = 'I 002';
        $spammaster_hostname = 'H 002';
      }
    }
    $softhard_data = [
      'spammaster_ip'       => $spammaster_ip,
      'spammaster_hostname' => $spammaster_hostname,
    ];
    return $softhard_data;
  }

  /**
   * Get SpamMaster light health view data.
   *
   * @return string
   *   SpamMaster light health information.
   */
  private function getHealthView() {
    // Get buffer count.
    $spammaster_buffer_size = $this->connection->select('spammaster_threats', 'u');
    $spammaster_buffer_size->fields('u', ['threat']);
    $spammaster_buffer_size_result = $spammaster_buffer_size->countQuery()->execute()->fetchField();
    if (empty($spammaster_buffer_size_result)) {
      $spammaster_buffer_size_result = '1';
    }
    // Get white count.
    $spammaster_white_size = $this->connection->select('spammaster_white', 'u');
    $spammaster_white_size->fields('u', ['white_']);
    $spammaster_white_size_result = $spammaster_white_size->countQuery()->execute()->fetchField();
    if (empty($spammaster_white_size_result)) {
      $spammaster_white_size_result = '0';
    }
    // Get logs count.
    $spammaster_spam_watch_query = $this->connection->select('spammaster_keys', 'u');
    $spammaster_spam_watch_query->fields('u', ['spamkey']);
    $spammaster_spam_watch_result = $spammaster_spam_watch_query->countQuery()->execute()->fetchField();
    if (empty($spammaster_spam_watch_result)) {
      $spammaster_spam_watch_result = '0';
    }
    // Get exempt count.
    $spammaster_exempt_count = $this->connection->select('spammaster_keys', 'u');
    $spammaster_exempt_count->condition('u.spamkey', '%' . $this->connection->escapeLike('exempt') . '%', 'LIKE');
    $spammaster_exempt_count->fields('u', ['spamkey']);
    $spammaster_exempt_query = $spammaster_exempt_count->countQuery()->execute()->fetchField();
    if (empty($spammaster_exempt_query)) {
      $spammaster_exempt_query = '0';
    }
    // Process other health values.
    $spammaster_settings = $this->configFactory->getEditable('spammaster.settings');
    $spammaster_subtype = $spammaster_settings->get('spammaster.subtype');
    $spammaster_white_transient = $this->state->get('spammaster.white_transient');
    $spammaster_settings_protection = $this->configFactory->getEditable('spammaster.settings_protection');
    $spammaster_basic_firewall_rules = $spammaster_settings_protection->get('spammaster.basic_firewall_rules');
    // Prepare light health view.
    $spam_count_pre_ar = [
      'buf' => $spammaster_buffer_size_result,
      'whi' => $spammaster_white_size_result,
      'log' => $spammaster_spam_watch_result,
      'exe' => $spammaster_exempt_query,
      'sub' => $spammaster_subtype,
      'whf' => $spammaster_white_transient,
      'fir' => $spammaster_basic_firewall_rules,
    ];
    $healtview_data = Json::encode($spam_count_pre_ar);
    return $healtview_data;
  }

}
