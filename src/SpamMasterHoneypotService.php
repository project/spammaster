<?php

namespace Drupal\spammaster;

use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use GuzzleHttp\Client;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;

/**
 * Class service.
 */
class SpamMasterHoneypotService {
  use StringTranslationTrait;

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The String Translation..
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The form class.
   *
   * @var \Drupal\Core\Form\FormInterface
   */
  protected $spamFormId;

  /**
   * The SpamMasterHoneypotService service.
   *
   * @var \Drupal\spammmaster\SpamMasterHoneypotService
   */
  protected $spammasterip;

  /**
   * The SpamMasterHoneypotService service.
   *
   * @var \Drupal\spammmaster\SpamMasterHoneypotService
   */
  protected $spammasteragent;

  /**
   * The SpamMasterHoneypotService service.
   *
   * @var \Drupal\spammmaster\SpamMasterHoneypotService
   */
  protected $spammasterextrafield1;

  /**
   * The SpamMasterHoneypotService service.
   *
   * @var \Drupal\spammmaster\SpamMasterHoneypotService
   */
  protected $spammasterextrafield2;

  /**
   * The SpamMasterHoneypotService service.
   *
   * @var \Drupal\spammmaster\SpamMasterHoneypotService
   */
  protected $spammasterreferer;

  /**
   * The SpamMasterHoneypotService service.
   *
   * @var \Drupal\spammmaster\SpamMasterHoneypotService
   */
  protected $spammasterurl;

  /**
   * The SpamMasterHoneypotService service.
   *
   * @var \Drupal\spammmaster\SpamMasterHoneypotService
   */
  protected $spammasteruserid;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, MessengerInterface $messenger, RequestStack $requestStack, Client $httpClient, ConfigFactoryInterface $configFactory, StateInterface $state, TranslationInterface $stringTranslation) {
    $this->connection = $connection;
    $this->messenger = $messenger;
    $this->requestStack = $requestStack;
    $this->httpClient = $httpClient;
    $this->configFactory = $configFactory;
    $this->state = $state;
    $this->stringTranslation = $stringTranslation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterHoneypotCheck($spamFormId, $spammasterip, $spammasteragent, $spammasterextrafield1, $spammasterextrafield2, $spammasterreferer, $spammasterurl, $spammasteruserid) {

    $this->spamFormId = $spamFormId;
    $this->spammasterip = $spammasterip;
    $this->spammasteragent = $spammasteragent;
    $this->spammasterextrafield1 = $spammasterextrafield1;
    $this->spammasterextrafield2 = $spammasterextrafield2;
    $this->spammasterreferer = $spammasterreferer;
    $this->spammasterurl = $spammasterurl;
    $this->spammasteruserid = $spammasteruserid;
    $spammaster_date = date('Y-m-d H:i:s');
    $spammaster_settings = $this->configFactory->getEditable('spammaster.settings');
    $spammaster_license = $spammaster_settings->get('spammaster.license_key');
    $spammaster_status = $this->state->get('spammaster.license_status');
    $spammaster_total_block_count = $this->state->get('spammaster.total_block_count');
    if ('VALID' === $spammaster_status || 'MALFUNCTION_1' === $spammaster_status || 'MALFUNCTION_2' === $spammaster_status) {
      // phpcs:ignore DrupalPractice.Variables.GetRequestData.SuperglobalAccessed
      $spampost = $_POST;
      if (!empty($spampost) && is_array($spampost)) {
        $result_post_content_json = Json::encode($spampost);
        $result_post_content_trim = substr($result_post_content_json, 0, 963);
        $result_post_content_clean = strip_tags($result_post_content_trim);
        $result_message_content_trim = 'Form: ' . substr($spamFormId, 0, 85) . ', Field 1: ' . substr($spammasterextrafield1, 0, 85) . ', Field 2: ' . substr($spammasterextrafield2, 0, 85) . ', MSG: ' . substr($result_post_content_clean, 0, 623);
        $result_message_content_clean = strip_tags($result_message_content_trim);
        $spampoststr = str_replace('=', ' ', urldecode(http_build_query($spampost, '', ' ')));
        // Collect email to scan.
        preg_match('/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/i', $spampoststr, $matches);
        if ($matches) {
          foreach ($matches as $key => $val) {
            $resultKey = $key;
            if ($resultKey) {
            }
            if (filter_var($val, FILTER_VALIDATE_EMAIL)) {
              $spammasteremail = substr($val, 0, 256);
              break;
            }
            else {
              $spammasteremail = 'haf@' . rand(10000000, 99999999) . '.drup';
            }
          }
        }
        else {
          $spammasteremail = 'haf@' . rand(10000000, 99999999) . '.drup';
        }
      }
      else {
        $result_post_content_clean = 'empty';
        $result_message_content_clean = 'Form: ' . substr($spamFormId, 0, 85) . ', Field 1: ' . strip_tags(substr($spammasterextrafield1, 0, 85)) . ', Field 2: ' . strip_tags(substr($spammasterextrafield2, 0, 85));
        $spammasteremail = 'haf@' . rand(10000000, 99999999) . '.drup';
      }
      // No whitelisting needed via honeypot fields check buffer.
      $spammaster_spam_buffer_query = $this->connection->query("SELECT threat FROM {spammaster_threats} WHERE threat = :ip", [
        ':ip' => $spammasterip,
      ])->fetchField();
      // Buffer db positive.
      if (empty($spammaster_spam_buffer_query)) {
        $this->connection->insert('spammaster_threats')->fields([
          'date' => $spammaster_date,
          'threat' => $spammasterip,
        ])->execute();

        // Web api check. Create data to be posted to verify rbl listings.
        $blog_threat_type = 'honeypot';
        $blog_web_address = $this->requestStack->getCurrentRequest()->getHost();
        $address_unclean = $blog_web_address;
        $address = preg_replace('#^https?://#', '', $address_unclean);
        @$blog_server_ip = $_SERVER['SERVER_ADDR'];
        // If empty ip.
        if (empty($blog_server_ip) || $blog_server_ip == '0') {
          @$blog_server_ip = 'I ' . gethostbyname($_SERVER['SERVER_NAME']);
        }
        $spam_master_leaning_url = 'https://www.spammaster.org/core/learn/get_learn_honey_2.php';
        // Call drupal hhtpclient.
        $client = $this->httpClient;
        // Post data.
        $client->post($spam_master_leaning_url, [
          'form_params' => [
            'blog_license_key'    => $spammaster_license,
            'blog_threat_ip'      => $spammasterip,
            'blog_threat_user'    => $spammasteruserid,
            'blog_threat_type'    => $blog_threat_type,
            'blog_threat_email'   => $spammasteremail,
            'blog_threat_content' => $result_message_content_clean,
            'blog_threat_agent'   => $spammasteragent,
            'blog_threat_refe'    => $spammasterreferer,
            'blog_threat_dest'    => $spammasterurl,
            'blog_web_adress'     => $address,
            'blog_server_ip'      => $blog_server_ip,
          ],
        ]);

        $spammaster_total_block_count_1 = ++$spammaster_total_block_count;
        $this->state->set('spammaster.total_block_count', $spammaster_total_block_count_1);

        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_date,
          'spamkey' => 'spammaster-honeypot',
          'spamvalue' => 'Spam Master honeypot V2 block IP: ' . $spammasterip . ', Field 1: ' . $spammasterextrafield1 . ', Field 2: ' . $spammasterextrafield2 . ', Agent: ' . $spammasteragent . ', Email: ' . $spammasteremail,
        ])->execute();
        throw new AccessDeniedHttpException();
      }
    }
  }

}
