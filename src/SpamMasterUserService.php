<?php

namespace Drupal\spammaster;

use Drupal\Core\Session\AccountProxy;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;

/**
 * Class service.
 */
class SpamMasterUserService {

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The form class.
   *
   * @var \Drupal\Core\Form\FormInterface
   */
  protected $spamuser;

  /**
   * {@inheritdoc}
   */
  public function __construct(AccountProxy $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterGetUser($spamuser) {

    $this->spamuser = $spamuser;
    $spam_user_id = $this->currentUser->id();

    if (isset($spam_user_id)) {
      $spam_username = $this->currentUser->getDisplayName();
      if (empty($spam_username)) {
        $spam_username = '';
      }
      $prespamuser = [
        'ID'       => $spam_user_id,
        'username' => $spam_username,
        'avatar'   => '',
      ];
    }
    else {
      $prespamuser = [
        'ID'       => 'none',
        'username' => 'none',
        'avatar'   => '',
      ];
    }
    $spamuserdata = Json::encode($prespamuser);

    return $spamuserdata;

  }

}
