<?php

namespace Drupal\spammaster;

use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class service.
 */
class SpamMasterUpdaterService {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The SpamMasterMailService Service.
   *
   * @var \Drupal\spammaster\SpamMasterMailService
   */
  protected $mailService;

  /**
   * The SpamMasterUpdaterService Service.
   *
   * @var \Drupal\spammaster\SpamMasterUpdaterService
   */
  protected $spamUpDb;

  /**
   * The SpamMasterUpdaterService Service.
   *
   * @var \Drupal\spammaster\SpamMasterUpdaterService
   */
  protected $spammasterDiscDate;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, ConfigFactoryInterface $configFactory, StateInterface $state, SpamMasterMailService $mailService) {
    $this->connection = $connection;
    $this->configFactory = $configFactory;
    $this->state = $state;
    $this->mailService = $mailService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('spammaster.mail_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterDateCheck($spammasterDiscDate) {

    $spammaster_send = $this->state->get('spammaster.spam_master_disc_not');
    $spammaster_senddate = $this->state->get('spammaster.spam_master_disc_not_date');
    $spamdesc = FALSE;
    $spamdescper = FALSE;
    $spamcode = FALSE;
    if ('2026-01-01' === $spammasterDiscDate) {
      $spamdesc = 'New Year';
      $spamdescper = '25%';
      $spamcode = 'NEWYPRO25';
    }
    if ('2025-02-14' === $spammasterDiscDate) {
      $spamdesc = 'Valentines Day';
      $spamdescper = '25%';
      $spamcode = 'VALENTINEPRO25';
    }
    if ('2025-03-20' === $spammasterDiscDate) {
      $spamdesc = 'Spring';
      $spamdescper = '25%';
      $spamcode = 'SPRINGRO25';
    }
    if ('2025-06-20' === $spammasterDiscDate) {
      $spamdesc = 'Summer';
      $spamdescper = '25%';
      $spamcode = 'SUMMERPRO25';
    }
    if ('2025-08-01' === $spammasterDiscDate) {
      $spamdesc = 'August';
      $spamdescper = '25%';
      $spamcode = 'AUGUSTPRO25';
    }
    if ('2025-10-31' === $spammasterDiscDate) {
      $spamdesc = 'Halloween';
      $spamdescper = '25%';
      $spamcode = 'HALLOWEENPRO25';
    }
    if ('2025-11-28' === $spammasterDiscDate) {
      $spamdesc = 'Black Friday';
      $spamdescper = '20%';
      $spamcode = 'BLACKPRO20';
    }
    if ('2025-12-01' === $spammasterDiscDate) {
      $spamdesc = 'Cyber Monday';
      $spamdescper = '25%';
      $spamcode = 'CYBERPRO25';
    }
    if ('2025-12-25' === $spammasterDiscDate) {
      $spamdesc = 'Christmas';
      $spamdescper = '20%';
      $spamcode = 'XMASPRO20';
    }
    $spammaster_discdate = $spammasterDiscDate;
    if ('0' === $spammaster_send && $spammasterDiscDate !== $spammaster_senddate && !empty($spamdesc) && !empty($spamdescper) && !empty($spamcode)) {
      $spammaster_mail_service = $this->mailService;
      $spammaster_mail_service->spamMasterDiscNotify($spammaster_discdate, $spamdesc, $spamdescper, $spamcode, $spammaster_send);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterUpDb($spamUpDb) {

    // Set flush to false.
    $spam_master_flush = FALSE;
    $spam_timenow = date('Y-m-d H:i:s');
    // Get db versions.
    $db_install_version_221 = $this->state->get('spammaster.db_install_version_221');
    $db_install_version_223 = $this->state->get('spammaster.db_install_version_223');
    $db_install_version_227 = $this->state->get('spammaster.db_install_version_227');
    $db_install_version_229 = $this->state->get('spammaster.db_install_version_229');
    $db_install_version_231 = $this->state->get('spammaster.db_install_version_231');
    $db_install_version_238 = $this->state->get('spammaster.db_install_version_238');
    $db_install_version_242 = $this->state->get('spammaster.db_install_version_242');
    $db_install_version_243 = $this->state->get('spammaster.db_install_version_243');
    $db_install_version_244 = $this->state->get('spammaster.db_install_version_244');
    $db_install_version_246 = $this->state->get('spammaster.db_install_version_246');
    $db_install_version_249 = $this->state->get('spammaster.db_install_version_249');
    $db_install_version_253 = $this->state->get('spammaster.db_install_version_253');
    $db_install_version_254 = $this->state->get('spammaster.db_install_version_254');
    $db_install_version_255 = $this->state->get('spammaster.db_install_version_255');
    $db_install_version_256 = $this->state->get('spammaster.db_install_version_256');
    $db_install_version_257 = $this->state->get('spammaster.db_install_version_257');

    // Process all legacy changes.
    if ('1' !== $db_install_version_221) {
      // State values, since each state is checked, can not use setmultiple.
      $spam_master_auto_update = $this->state->get('spammaster.spam_master_auto_update');
      if (empty($spam_master_auto_update)) {
        $this->state->set('spammaster.spam_master_auto_update', 'false');
      }
      $spam_master_db_protection_hash = $this->state->get('spammaster.spam_master_db_protection_hash');
      if (empty($spam_master_db_protection_hash)) {
        $spam_master_try_db_protection_hash = substr(md5(uniqid(mt_rand(), TRUE)), 0, 64);
        if (!empty($spam_master_try_db_protection_hash)) {
          $this->state->set('spammaster.spam_master_db_protection_hash', $spam_master_try_db_protection_hash);
        }
        else {
          $spam_master_again_db_protection_hash = 'md5-' . date('YmdHis');
          $this->state->set('spammaster.spam_master_db_protection_hash', $spam_master_again_db_protection_hash);
        }
      }
      $spam_master_license_sync_date = $this->state->get('spammaster.spam_master_license_sync_date');
      if (empty($spam_master_license_sync_date)) {
        $this->state->set('spammaster.spam_master_license_sync_date', date('Y-m-d'));
      }
      $spam_master_license_sync_run = $this->state->get('spammaster.spam_master_license_sync_run');
      if (empty($spam_master_license_sync_run)) {
        $this->state->set('spammaster.spam_master_license_sync_run', '0');
      }
      $spam_master_new_options = $this->state->get('spammaster.new_options');
      if (empty($spam_master_new_options)) {
        $this->state->set('spammaster.new_options', '0');
      }
      $spam_master_disc_not_date = $this->state->get('spammaster.spam_master_disc_not_date');
      if (empty($spam_master_disc_not_date)) {
        $this->state->set('spammaster.spam_master_disc_not_date', '1970-01-01');
      }
      $spam_master_disc_not = $this->state->get('spammaster.spam_master_disc_not');
      if (empty($spam_master_disc_not)) {
        $this->state->set('spammaster.spam_master_disc_not', '0');
      }
      $this->state->set('spammaster.white_transient', '0');
      // Delete state values.
      $delete_state_ar = [
        'spammaster.hourly_con',
        'spammaster.daily_con',
        'spammaster.weekly_con',
      ];
      $this->state->deleteMultiple($delete_state_ar);

      // Editable values, single checks entries and multi without entries check.
      $spammaster_settings_protection = $this->configFactory->getEditable('spammaster.settings_protection');
      $spam_master_is_cloudflare = $spammaster_settings_protection->get('spammaster.spam_master_is_cloudflare');
      if (empty($spam_master_is_cloudflare)) {
        $spammaster_settings_protection
          ->set('spammaster.spam_master_is_cloudflare', '0')
          ->save();
      }
      $spam_master_pre_signature = $spammaster_settings_protection->get('spammaster.signature');
      if (empty($spam_master_pre_signature)) {
        $spammaster_settings_protection
          ->set('spammaster.signature', '1')
          ->save();
      }
      $this->configFactory->getEditable('spammaster.settings_protection')
        ->set('spammaster.block_message', 'Your Email, Domain, or Ip are banned.')
        ->set('spammaster.basic_firewall', '1')
        ->set('spammaster.extra_honeypot', '1')
        ->set('spammaster.email_alert_3', '1')
        ->set('spammaster.email_daily_report', '0')
        ->set('spammaster.email_weekly_report', '0')
        ->set('spammaster.email_improve', '1')
        ->set('spammaster.cleanup_firewall', '15')
        ->set('spammaster.cleanup_honeypot', '15')
        ->set('spammaster.cleanup_whitelist', '15')
        ->set('spammaster.cleanup_system', '15')
        ->set('spammaster.cleanup_mail', '15')
        ->set('spammaster.cleanup_cron', '15')
        ->save();

      // Database legacy clean up and new inserts.
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'spammaster-registration', '=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'spammaster-comment', '=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'spammaster-contact', '=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'spammaster-recaptcha', '=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'spammaster-buffer', '=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'spammaster-cleanup', '=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'SPAMMASTER-DB-VERSION', '=')
        ->execute();
      // Updated the db version.
      $spdbversion = '221';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_221 = $this->state->set('spammaster.db_install_version_221', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 223.
    if ('1' !== $db_install_version_223) {

      // Database clear exempt keys.
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'exempt-needle', '=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'exempt-key', '=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'exempt-value', '=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'exempt-needle-straw', '=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'exempt-needle-sig-hide', '=')
        ->execute();
      $this->connection->delete('spammaster_keys')
        ->condition('spamkey', 'exempt-needle-sig-show', '=')
        ->execute();
      // Check if exempt keys exist.
      $spammaster_exempt = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-needle',
        ':value' => 'search',
      ]);
      $spammaster_exempt_result = $spammaster_exempt->fetchField();
      if (empty($spammaster_exempt_result)) {
        $spammaster_values = [
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-needle',
            'spamvalue' => 'cart',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-needle',
            'spamvalue' => 'commerce_checkout_flow_multistep_default',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-needle',
            'spamvalue' => 'commerce_wishlist',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-needle',
            'spamvalue' => 'lang_dropdown_form',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-needle',
            'spamvalue' => 'mailchimp_',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-needle',
            'spamvalue' => 'search',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-key',
            'spamvalue' => 'delete-order-item',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-key',
            'spamvalue' => 'edit_quantity',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-key',
            'spamvalue' => 'lang_dropdown_select',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-key',
            'spamvalue' => 'purchased_entity',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-value',
            'spamvalue' => 'Add+to+cart',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-value',
            'spamvalue' => 'Add to cart',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-value',
            'spamvalue' => 'Add to wishlist',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-value',
            'spamvalue' => 'Aggiungi al carrello',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-value',
            'spamvalue' => 'Ajouter au panier',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-value',
            'spamvalue' => 'commerce_wishlist',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-value',
            'spamvalue' => 'Pay+and+complete+purchase',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-value',
            'spamvalue' => 'Pay and complete purchase',
          ],
          [
            'date' => $spam_timenow,
            'spamkey' => 'exempt-value',
            'spamvalue' => 'Proceed to checkout',
          ],
        ];
        $spammaster_query = $this->connection->insert('spammaster_keys')->fields(
          [
            'date',
            'spamkey',
            'spamvalue',
          ]
        );
        foreach ($spammaster_values as $exempt) {
          $spammaster_query->values($exempt);
        }
        $spammaster_query->execute();
      }
      // Updated the db version.
      $spdbversion = '223';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_223 = $this->state->set('spammaster.db_install_version_223', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 227.
    if ('1' !== $db_install_version_227) {

      // Check if exempt keys exist one by one and insert.
      $views_exposed_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-needle',
        ':value' => 'views_exposed_form',
      ])->fetchField();
      if (empty($views_exposed_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-needle',
          'spamvalue' => 'views_exposed_form',
        ])->execute();
      }

      // Updated the db version.
      $spdbversion = '227';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_227 = $this->state->set('spammaster.db_install_version_227', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 229.
    if ('1' !== $db_install_version_229) {

      // Check if exempt value exists and insert.
      $votingapi_reaction = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-value',
        ':value' => 'votingapi_reaction',
      ])->fetchField();
      if (empty($votingapi_reaction)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-value',
          'spamvalue' => 'votingapi_reaction',
        ])->execute();
      }

      // Updated the db version.
      $spdbversion = '229';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_229 = $this->state->set('spammaster.db_install_version_229', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 231.
    if ('1' !== $db_install_version_231) {

      // Change stats email default setting.
      $spammaster_settings_protection = $this->configFactory->getEditable('spammaster.settings_protection');
      $spammaster_settings_protection
        ->set('spammaster.email_improve', '0')
        ->save();

      // Updated the db version.
      $spdbversion = '231';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_231 = $this->state->set('spammaster.db_install_version_231', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 238.
    if ('1' !== $db_install_version_238) {

      // Change stats email default setting.
      $spammaster_settings_protection = $this->configFactory->getEditable('spammaster.settings_protection');
      $spammaster_settings_protection
        ->set('spammaster.cleanup_firewall', '15')
        ->set('spammaster.cleanup_honeypot', '15')
        ->set('spammaster.cleanup_whitelist', '15')
        ->set('spammaster.cleanup_system', '15')
        ->set('spammaster.cleanup_mail', '15')
        ->set('spammaster.cleanup_cron', '15')
        ->save();

      // Updated the db version.
      $spdbversion = '238';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_238 = $this->state->set('spammaster.db_install_version_238', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 242.
    if ('1' !== $db_install_version_242) {

      // Check if exempt value exists and insert.
      $views_form_media_library_widget_image = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-needle',
        ':value' => 'views_form_media_library_widget_image',
      ])->fetchField();
      if (empty($views_form_media_library_widget_image)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-needle',
          'spamvalue' => 'views_form_media_library_widget_image',
        ])->execute();
      }
      $views_form_media_library_widget_video = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-needle',
        ':value' => 'views_form_media_library_widget_video',
      ])->fetchField();
      if (empty($views_form_media_library_widget_video)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-needle',
          'spamvalue' => 'views_form_media_library_widget_video',
        ])->execute();
      }
      $views_form_media_library_widget_remote_audio = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-needle',
        ':value' => 'views_form_media_library_widget_remote_audio',
      ])->fetchField();
      if (empty($views_form_media_library_widget_remote_audio)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-needle',
          'spamvalue' => 'views_form_media_library_widget_remote_audio',
        ])->execute();
      }
      $views_form_media_library_widget_remote_video = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-needle',
        ':value' => 'views_form_media_library_widget_remote_video',
      ])->fetchField();
      if (empty($views_form_media_library_widget_remote_video)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-needle',
          'spamvalue' => 'views_form_media_library_widget_remote_video',
        ])->execute();
      }
      $entity_view_display_layout_builder_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-needle',
        ':value' => 'entity_view_display_layout_builder_form',
      ])->fetchField();
      if (empty($entity_view_display_layout_builder_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-needle',
          'spamvalue' => 'entity_view_display_layout_builder_form',
        ])->execute();
      }
      $editor_link_dialog = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-needle',
        ':value' => 'editor_link_dialog',
      ])->fetchField();
      if (empty($editor_link_dialog)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-needle',
          'spamvalue' => 'editor_link_dialog',
        ])->execute();
      }
      $editor_image_dialog = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-needle',
        ':value' => 'editor_image_dialog',
      ])->fetchField();
      if (empty($editor_image_dialog)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-needle',
          'spamvalue' => 'editor_image_dialog',
        ])->execute();
      }
      $media_library_add_form_upload = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-needle',
        ':value' => 'media_library_add_form_upload',
      ])->fetchField();
      if (empty($media_library_add_form_upload)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-needle',
          'spamvalue' => 'media_library_add_form_upload',
        ])->execute();
      }
      $commerce_order_item_pado_add_to_cart_form_commerce = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-value',
        ':value' => 'commerce_order_item_pado_add_to_cart_form_commerce',
      ])->fetchField();
      if (empty($commerce_order_item_pado_add_to_cart_form_commerce)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-value',
          'spamvalue' => 'commerce_order_item_pado_add_to_cart_form_commerce',
        ])->execute();
      }

      // Updated the db version.
      $spdbversion = '242';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_242 = $this->state->set('spammaster.db_install_version_242', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 243.
    if ('1' !== $db_install_version_243) {

      // Change default firewall rule setting.
      $spammaster_settings_protection = $this->configFactory->getEditable('spammaster.settings_protection');
      $spammaster_settings_protection
        ->set('spammaster.basic_firewall_rules', '1')
        ->save();

      // Updated the db version.
      $spdbversion = '243';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_243 = $this->state->set('spammaster.db_install_version_243', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 244.
    if ('1' !== $db_install_version_244) {

      // Change default firewall rule set.
      $this->state->set('spammaster.basic_firewall_rules_set', '0');

      // Updated the db version.
      $spdbversion = '244';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_244 = $this->state->set('spammaster.db_install_version_244', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 246.
    if ('1' !== $db_install_version_246) {

      $ajax_page_state = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'ajax_page_state',
      ])->fetchField();
      if (!empty($ajax_page_state)) {
        $this->connection->delete('spammaster_keys')
          ->condition('spamvalue', 'ajax_page_state', '=')
          ->execute();
      }
      $simplenews_subscri = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamvalue = :value", [
        ':value' => 'simplenews_subscri',
      ])->fetchField();
      if (!empty($simplenews_subscri)) {
        $this->connection->delete('spammaster_keys')
          ->condition('spamvalue', 'simplenews_subscri', '=')
          ->execute();
      }

      // Updated the db version.
      $spdbversion = '246';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_246 = $this->state->set('spammaster.db_install_version_246', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 249.
    if ('1' !== $db_install_version_249) {

      $search_key = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'search',
      ])->fetchField();
      if (!empty($search_key)) {
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'exempt-key', '=')
          ->condition('spamvalue', 'search', '=')
          ->execute();
      }
      $search_value = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-value',
        ':value' => 'search',
      ])->fetchField();
      if (!empty($search_value)) {
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'exempt-value', '=')
          ->condition('spamvalue', 'search', '=')
          ->execute();
      }

      // Updated the db version.
      $spdbversion = '249';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_249 = $this->state->set('spammaster.db_install_version_249', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 253.
    if ('1' !== $db_install_version_253) {

      // Multiple update state dates and notifications.
      $dats_nots = [
        'spammaster.malfunction3_date' => '1970-01-01',
        'spammaster.malfunction4_date' => '1970-01-01',
        'spammaster.malfunction5_date' => '1970-01-01',
        'spammaster.malfunction6_date' => '1970-01-01',
        'spammaster.malfunction8_date' => '1970-01-01',
        'spammaster.high_volume_date' => '1970-01-01',
        'spammaster.unstable_date' => '1970-01-01',
        'spammaster.malfunction3_notice' => '0',
        'spammaster.malfunction4_notice' => '0',
        'spammaster.malfunction5_notice' => '0',
        'spammaster.malfunction6_notice' => '0',
        'spammaster.malfunction8_notice' => '0',
        'spammaster.high_volume_notice' => '0',
        'spammaster.unstable_notice' => '0',
      ];
      $this->state->setMultiple($dats_nots);

      // Updated the db version.
      $spdbversion = '253';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_253 = $this->state->set('spammaster.db_install_version_253', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 254.
    if ('1' !== $db_install_version_254) {

      // Check if exempt keys exist one by one and insert.
      $node_node = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node:node',
      ])->fetchField();
      if (empty($node_node)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node:node',
        ])->execute();
      }
      $block_block = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'block:block',
      ])->fetchField();
      if (empty($block_block)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'block:block',
        ])->execute();
      }
      $media_media = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'media:media',
      ])->fetchField();
      if (empty($media_media)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'media:media',
        ])->execute();
      }
      $entity_view_edit = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'entity.view.edit',
      ])->fetchField();
      if (empty($entity_view_edit)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'entity.view.edit',
        ])->execute();
      }
      $relay_state = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'RelayState',
      ])->fetchField();
      if (empty($relay_state)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'RelayState',
        ])->execute();
      }
      // Delete exempts.
      $draggableviews = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'draggableviews.view.order_view',
      ])->fetchField();
      if (!empty($draggableviews)) {
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'exempt-key', '=')
          ->condition('spamvalue', 'draggableviews.view.order_view', '=')
          ->execute();
      }
      $libraries = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'libraries',
      ])->fetchField();
      if (!empty($libraries)) {
        $this->connection->delete('spammaster_keys')
          ->condition('spamvalue', 'libraries', '=')
          ->execute();
      }

      // Updated the db version.
      $spdbversion = '254';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_254 = $this->state->set('spammaster.db_install_version_254', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 255.
    if ('1' !== $db_install_version_255) {

      // Check if exempt keys exist one by one and insert.
      $node_article_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_article_edit_form',
      ])->fetchField();
      if (empty($node_article_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_article_edit_form',
        ])->execute();
      }
      $node_page_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_page_edit_form',
      ])->fetchField();
      if (empty($node_page_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_page_edit_form',
        ])->execute();
      }
      $node_news_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_news_edit_form',
      ])->fetchField();
      if (empty($node_news_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_news_edit_form',
        ])->execute();
      }
      $node_downloads_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_downloads_edit_form',
      ])->fetchField();
      if (empty($node_downloads_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_downloads_edit_form',
        ])->execute();
      }
      $node_event_homepage_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_event_homepage_edit_form',
      ])->fetchField();
      if (empty($node_event_homepage_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_event_homepage_edit_form',
        ])->execute();
      }
      $node_landing_page_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_landing_page_edit_form',
      ])->fetchField();
      if (empty($node_landing_page_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_landing_page_edit_form',
        ])->execute();
      }
      $node_testimonial_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_testimonial_edit_form',
      ])->fetchField();
      if (empty($node_testimonial_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_testimonial_edit_form',
        ])->execute();
      }
      $node_article_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_article_edit_form',
      ])->fetchField();
      if (empty($node_article_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_article_edit_form',
        ])->execute();
      }
      $media_image_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'media_image_edit_form',
      ])->fetchField();
      if (empty($media_image_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'media_image_edit_form',
        ])->execute();
      }
      $media_file_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'media_file_edit_form',
      ])->fetchField();
      if (empty($media_file_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'media_file_edit_form',
        ])->execute();
      }
      $taxonomy_term_events = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'taxonomy_term_events',
      ])->fetchField();
      if (empty($taxonomy_term_events)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'taxonomy_term_events',
        ])->execute();
      }

      // Updated the db version.
      $spdbversion = '255';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_255 = $this->state->set('spammaster.db_install_version_255', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 256.
    if ('1' !== $db_install_version_256) {

      // Change default website sub type setting, production or development.
      $spammaster_config = $this->configFactory->getEditable('spammaster.settings');
      $spammaster_config
        ->set('spammaster.subtype', 'prod')
        ->save();

      // Check if exempt keys exist one by one and insert.
      $node_article_delete_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_article_delete_form',
      ])->fetchField();
      if (empty($node_article_delete_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_article_delete_form',
        ])->execute();
      }
      $node_event_instance_delete_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_event_instance_delete_form',
      ])->fetchField();
      if (empty($node_event_instance_delete_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_event_instance_delete_form',
        ])->execute();
      }
      $node_layout_page_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_layout_page_edit_form',
      ])->fetchField();
      if (empty($node_layout_page_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_layout_page_edit_form',
        ])->execute();
      }
      $node_session_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_session_edit_form',
      ])->fetchField();
      if (empty($node_session_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_session_edit_form',
        ])->execute();
      }

      // Updated the db version.
      $spdbversion = '256';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_256 = $this->state->set('spammaster.db_install_version_256', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Process 256.
    if ('1' !== $db_install_version_257) {

      // Insert db protection hash date time for further protection.
      $this->state->set('spammaster.spam_master_db_protection_hash_dt', $spam_timenow);

      // Check if exempt keys exist one by one and insert.
      $node_application_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_application_edit_form',
      ])->fetchField();
      if (empty($node_application_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_application_edit_form',
        ])->execute();
      }
      $node_basic_page_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_basic_page_edit_form',
      ])->fetchField();
      if (empty($node_basic_page_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_basic_page_edit_form',
        ])->execute();
      }
      $node_vacature_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'node_vacature_edit_form',
      ])->fetchField();
      if (empty($node_vacature_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'node_vacature_edit_form',
        ])->execute();
      }
      $config_pages_type_edit_form = $this->connection->query("SELECT spamvalue FROM {spammaster_keys} WHERE spamkey = :key AND spamvalue = :value", [
        ':key' => 'exempt-key',
        ':value' => 'config_pages_type_edit_form',
      ])->fetchField();
      if (empty($config_pages_type_edit_form)) {
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spam_timenow,
          'spamkey' => 'exempt-key',
          'spamvalue' => 'config_pages_type_edit_form',
        ])->execute();
      }

      // Updated the db version.
      $spdbversion = '257';
      // Update installed version.
      $this->state->set('spammaster.db_install_version', $spdbversion);
      // Set updater as run 1.
      $db_install_version_257 = $this->state->set('spammaster.db_install_version_257', '1');
      // Log db update.
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spam_timenow,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Updater service -> Success, updated db version to: ' . $spdbversion,
      ])->execute();
      // Set to flush.
      $spam_master_flush = '1';
    }

    // Insert updates above this line and set spam_master_flush to 1.
    if ('1' === $spam_master_flush) {
      drupal_flush_all_caches();
    }
  }

}
