<?php

namespace Drupal\spammaster\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\Client;

/**
 * Class controller.
 */
class SpamMasterWhiteForm extends ConfigFormBase {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, MessengerInterface $messenger, StateInterface $state, Client $httpClient) {
    $this->connection = $connection;
    $this->messenger = $messenger;
    $this->state = $state;
    $this->httpClient = $httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('messenger'),
      $container->get('state'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spammaster_settings_white_form';
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterDeleteWhite($form, &$form_state) {
    $white_form_delete = $form_state->getValue('white_header')['table_white'];
    $spammaster_white_date = date("Y-m-d H:i:s");
    foreach ($white_form_delete as $white_row_delete) {
      if (!empty($white_row_delete)) {
        $this->connection->delete('spammaster_white')
          ->condition('id', $white_row_delete, '=')
          ->execute();
        $this->messenger->addMessage($this->t('Saved Whitelist deletion.'));
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_white_date,
          'spamkey' => 'spammaster-whitelist',
          'spamvalue' => 'Spam Master: whitelist deletion, Id: ' . $white_row_delete,
        ])->execute();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    // Default settings.
    $config = $this->config('spammaster.white');

    // First table for Ips and emails.
    $form['white_header'] = [
      '#type' => 'details',
      '#title' => $this->t('<h3>Whitelist Ips, Emails & Forms</h3>'),
      '#tree' => TRUE,
      '#open' => TRUE,
      '#attached' => [
        'library' => [
          'spammaster/spammaster-styles',
        ],
      ],
    ];
    $form['white_header']['table_1'] = [
      '#type' => 'table',
      '#header' => [
          [
            'data' => $this->t('Insert Ips, emails or form ids to exempt them from scans.'),
            'colspan' => 4,
          ],
      ],
    ];
    // Insert Whitelist field.
    $form['white_header']['table_1']['addrow']['white_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Insert Ip, email or form_id:'),
      '#default_value' => $config->get('spammaster.white_key'),
      '#description' => $this->t('Insert user ips, emails or form ids. Whitelisting ips or emails also deletes any Spam Buffer entry. Example of form id: openid_connect_login_form.'),
      '#attributes' => [
        'class' => [
          'spammaster-responsive-49',
        ],
      ],
    ];

    // Insert Whitelist selector.
    $form['white_header']['table_1']['addrow']['white_selection'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Whitelist Type:'),
      '#options' => [
        'none' => '',
        'ip' => $this->t('IP'),
        'email' => $this->t('Email'),
        'formid' => $this->t('FormId'),
      ],
      '#default_value' => 0,
      '#description' => $this->t('Select whitelisting type.'),
      '#attributes' => [
        'class' => [
          'spammaster-responsive-49',
        ],
      ],
    ];

    // Construct header.
    $header = [
      'id' => [
        'data' => $this->t('ID'),
        'field'  => 'id',
        'specifier' => 'id',
        'sort' => 'desc',
      ],
      'date' => [
        'data' => $this->t('Date'),
        'field'  => 'date',
        'specifier' => 'date',
        'sort' => 'desc',
      ],
      'white' => [
        'data' => $this->t('Whitelisted'),
        'field'  => 'white',
        'specifier' => 'white',
        'sort' => 'desc',
      ],
    ];
    // Get table spammaster_white data.
    $spammaster_spam_white = $this->connection->select('spammaster_white', 'u')
      ->fields('u', ['id', 'date', 'white'])
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(20)
      ->execute()->fetchAll();

    $output = [];
    foreach ($spammaster_spam_white as $results) {
      if (!empty($results)) {
        $output[$results->id] = [
          'id' => $results->id,
          'date' => $results->date,
          'white' => $results->white,
        ];
      }
    }
    // Get White Size.
    $spammaster_white_size = $this->connection->select('spammaster_white', 'u');
    $spammaster_white_size->fields('u', ['white']);
    $spammaster_white_size_result = $spammaster_white_size->countQuery()->execute()->fetchField();
    $form['white_header']['total_white'] = [
      '#markup' => $this->t('<h2>Whitelist Size: <span class="spam-master-admin-green spam-master-top-admin-shadow-offline">@white_size</span></h2>', ['@white_size' => $spammaster_white_size_result]),
      '#attributes' => [
        'class' => [
          'spam-master-admin-green',
          'spam-master-top-admin-shadow-offline',
        ],
      ],
    ];

    // Whitelist Description.
    $form['white_header']['header_description'] = [
      '#markup' => $this->t('<p>Spam Master Whitelisting excludes spam checks from safe Ips, Emails or Form Ids.</p>'),
    ];

    // Display table.
    $form['white_header']['table_white'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $output,
      '#empty' => $this->t('No Entries found'),
    ];
    // Delete button at end of table, calls spammasterdeletewhite function.
    $form['white_header']['submit'] = [
      '#type' => 'submit',
      '#attributes' => [
        'class' => ['button button--danger'],
      ],
      '#value' => $this->t('Delete Whitelist Entry'),
      '#submit' => ['::spamMasterDeleteWhite'],
    ];

    // Form pager if ore than 25 entries.
    $form['white_header']['pager'] = [
      '#type' => 'pager',
    ];

    // Whitelist Description.
    $form['white_header']['footer_description'] = [
      '#markup' => $this->t('<p>Add frequent user emails or ips to exclude from spam checks.</p>'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $is_deletion = FALSE;
    $white_form_delete = $form_state->getValue('white_header')['table_white'];
    foreach ($white_form_delete as $white_row_delete) {
      if (!empty($white_row_delete)) {
        $is_deletion = TRUE;
      }
    }
    if (FALSE === $is_deletion) {
      if (!empty('none' !== $form_state->getValue('white_header')['table_1']['addrow']['white_selection']) && 'none' !== $form_state->getValue('white_header')['table_1']['addrow']['white_selection']) {
        if ('ip' === $form_state->getValue('white_header')['table_1']['addrow']['white_selection']) {
          if (empty($form_state->getValue('white_header')['table_1']['addrow']['white_key'])) {
            $form_state->setErrorByName('white_header', $this->t('Please insert a valid Ip.'));
          }
          else {
            if (!filter_var($form_state->getValue('white_header')['table_1']['addrow']['white_key'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) && !filter_var($form_state->getValue('white_header')['table_1']['addrow']['white_key'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
              $form_state->setErrorByName('white_header', $this->t('Validation of Ip Failed. Please insert a valid Ip.'));
            }
          }
        }
        if ('email' === $form_state->getValue('white_header')['table_1']['addrow']['white_selection']) {
          if (empty($form_state->getValue('white_header')['table_1']['addrow']['white_key'])) {
            $form_state->setErrorByName('white_header', $this->t('Please insert a valid Email.'));
          }
          else {
            if (!filter_var($form_state->getValue('white_header')['table_1']['addrow']['white_key'], FILTER_VALIDATE_EMAIL)) {
              $form_state->setErrorByName('white_header', $this->t('Validation of Email Failed. Please insert a valid Email.'));
            }
          }
        }
        if ('formid' === $form_state->getValue('white_header')['table_1']['addrow']['white_selection']) {
          if (empty($form_state->getValue('white_header')['table_1']['addrow']['white_key'])) {
            $form_state->setErrorByName('white_header', $this->t('Please insert a valid form_id.'));
          }
        }
      }
      else {
        $form_state->setErrorByName('white_header', $this->t('Selection type is empty.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get editable settings.
    $spammaster_settings = $this->configFactory->getEditable('spammaster.settings');
    $spammaster_license = $spammaster_settings->get('spammaster.license_key');
    // Get state settings.
    $spam_master_db_protection_hash = $this->state->get('spammaster.spam_master_db_protection_hash');

    $white_to_insert = $form_state->getValue('white_header')['table_1']['addrow']['white_key'];
    if (!empty($white_to_insert)) {
      // Insert whitelist db.
      $spammaster_db_ip = $this->connection->query("SELECT white FROM {spammaster_white} WHERE white = :ip", [
        ':ip' => $white_to_insert,
      ])->fetchField();
      if (empty($spammaster_db_ip)) {
        $spammaster_date = date("Y-m-d H:i:s");
        $this->connection->insert('spammaster_white')->fields([
          'date' => $spammaster_date,
          'white' => $white_to_insert,
        ])->execute();
        // Log message.
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_date,
          'spamkey' => 'spammaster-whitelist',
          'spamvalue' => 'Spam Master: Whitelist insertion successful for: ' . $white_to_insert,
        ])->execute();
        // Delete from buffer if exists.
        $spammaster_check_buffer = $this->connection->query("SELECT threat FROM {spammaster_threats} WHERE threat = :ip", [
          ':ip' => $white_to_insert,
        ])->fetchField();
        $spam_master_is_buffer = 'NO';
        if (!empty($spammaster_check_buffer)) {
          $this->connection->delete('spammaster_threats')
            ->condition('threat', $white_to_insert, '=')
            ->execute();
          // Log message.
          $this->connection->insert('spammaster_keys')->fields([
            'date' => $spammaster_date,
            'spamkey' => 'spammaster-whitelist',
            'spamvalue' => 'Spam Master: Whitelist buffer deletion successful for: ' . $white_to_insert,
          ])->execute();
          $spam_master_is_buffer = 'YES';
        }
        // Report to rbl servers. Speed up not processing response body.
        $spammaster_white_url = 'https://www.spammaster.org/core/white/get_white.php';
        // Call drupal hhtpclient.
        $client = $this->httpClient;
        // Post data.
        $client->post($spammaster_white_url, [
          'form_params' => [
            'spam_license_key' => $spammaster_license,
            'spam_master_db' => $spam_master_db_protection_hash,
            'spam_master_white' => $white_to_insert,
            'spam_master_is_buffer' => $spam_master_is_buffer,
          ],
        ]);
      }
    }
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'spammaster.settings_white',
    ];
  }

}
