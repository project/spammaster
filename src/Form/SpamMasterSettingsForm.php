<?php

namespace Drupal\spammaster\Form;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\State\StateInterface;
use Drupal\spammaster\SpamMasterLicService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * SpamMaster settings form.
 */
class SpamMasterSettingsForm extends ConfigFormBase {

  /**
   * The SpamMasterLicService service.
   *
   * @var \Drupal\spammaster\SpamMasterLicService
   */
  protected $manualLic;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The module list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleList;

  /**
   * List of modules that can be disabled to improve performance.
   *
   * @var array
   */
  protected $unneededModules = [
    'antibot',
    'recaptcha',
    'recaptcha_v3',
    'honeypot',
    'captcha',
    'riddler',
    'cleantalk',
    'hcaptcha',
    'google_recaptcha',
    'spamicide',
    'spambot',
    'protect_form_flood_control',
    'spamaway',
    'cloudfilt',
    'zencaptcha',
    'akismet',
    'antispamban',
    'mosparo_integration',
    'custom_captcha',
    'captcha_questions',
    'email_validate',
    'mtcaptcha',
    'botbattler',
    'abuseipdb',
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct(SpamMasterLicService $manualLic, StateInterface $state, ModuleHandlerInterface $module_handler, ModuleExtensionList $module_list) {
    $this->manualLic = $manualLic;
    $this->state = $state;
    $this->moduleHandler = $module_handler;
    $this->moduleList = $module_list;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('spammaster.lic_service'),
      $container->get('state'),
      $container->get('module_handler'),
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spammaster_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Default settings.
    $config = $this->config('spammaster.settings');
    $response_key = $this->state->get('spammaster.license_status');
    if (empty($response_key)) {
      $response_key = 'INACTIVE';
    }

    $serviceInfo = $this->getServiceInfo($response_key);
    $form['license_header'] = [
      '#type' => 'details',
      '#title' => $this->t('<h3>Spam Master <span class="@class">@type</span> Version: @version</h3>', [
        '@class'   => $serviceInfo['license_type']['license_class'],
        '@type'    => $serviceInfo['license_type']['spam_type'],
        '@version' => $this->state->get('spammaster.version'),
      ]),
      '#tree' => TRUE,
      '#open' => TRUE,
    ];
    $form['license_header']['basic_option'] = [
      '#type' => 'table',
      '#header' => [
          [
            'data' => $this->t('License key and implementation type.'),
            'colspan' => 4,
          ],
      ],
    ];
    // Insert license key field.
    $form['license_header']['basic_option']['addrow']['license_key'] = [
      '#type' => 'textfield',
      '#title'  => $this->t('Insert license key number:'),
      '#default_value' => $config->get('spammaster.license_key'),
      '#description'   => $this->t('Insert your license key number.&nbsp;') . $serviceInfo['license_status']['email_attached'] . $this->t('<a href="@spammaster_url" target="@spammaster_target">Get PRO rbl license for peanuts</a>.', [
        '@spammaster_url'    => 'https://www.techgasp.com/downloads/spam-master-license/',
        '@spammaster_target' => '_blank',
      ]),
      '#attributes' => [
        'class' => [
          'spammaster-responsive-49',
        ],
      ],
    ];

    // Insert license key field.
    $form['license_header']['basic_option']['addrow']['subtype'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Website Type:'),
      '#options' => [
        'prod' => $this->t('Live - Production'),
        'deve' => $this->t('Test - Development'),
      ],
      '#default_value' => $config->get('spammaster.subtype'),
      '#description' => $this->t('Select Spam Master type of implementation. Live Production website or Test Development website.'),
      '#attributes' => [
        'class' => [
          'spammaster-responsive-49',
        ],
      ],
    ];

    $lic_call = $this->manualLic;
    if ('VALID' !== $response_key) {
      $form['license_header']['submit'] = [
        '#type' => 'submit',
        '#attributes' => [
          'class' => ['button button--danger'],
        ],
        '#value' => $this->t('RE-SYNCHRONIZE CONNECTION') . ' ' . $response_key,
        '#submit' => [
          '::validateForm',
          '::submitForm',
          [$lic_call, 'spamMasterLicManualCreation'],
        ],
      ];
      $form['license_header']['submit']['more'] = [
        '#type' => 'item',
        '#markup' => $this->t('<a href="@spammaster_docs_url" target="@spammaster_docs_target">Help about Statuses</a>.', [
          '@spammaster_docs_url' => 'https://www.spammaster.org/documentation/#status',
          '@spammaster_docs_target' => '_blank',
        ]),
        '#required' => FALSE,
        '#weight' => 999,
      ];
    }
    else {
      $form['license_header']['submit'] = [
        '#type' => 'submit',
        '#attributes' => [
          'class' => ['button button--primary'],
        ],
        '#value' => $this->t('Refresh License'),
        '#submit' => [
          '::validateForm',
          '::submitForm',
          [$lic_call, 'spamMasterLicManualCreation'],
        ],
      ];
    }

    // Insert license table inside tree.
    $form['license_header']['license'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attached' => [
        'library' => [
          'spammaster/spammaster-styles',
        ],
      ],
    ];
    // Insert addrow license status field.
    $form['license_header']['license']['addrow']['license_type'] = [
      '#required' => FALSE,
      '#type' => 'item',
      '#title' => $this->t('Your licence type:'),
      '#description' => $this->t('<strong><span class="@lic_class">@lic_type</span></strong> @lic_upgrade', [
        '@lic_class'   => $serviceInfo['license_type']['license_class'],
        '@lic_type'    => $serviceInfo['license_type']['license_type'],
        '@lic_upgrade' => $serviceInfo['license_type']['license_type_upgrade'],
      ]),
    ];
    $form['license_header']['license']['addrow']['status'] = [
      '#required' => FALSE,
      '#type' => 'item',
      '#title' => $this->t('Your licence status:'),
      '#description' => $this->t('<strong><span class="@lic_colour">@lic_status</span></strong>. Your license status should always be <strong><span class="spam-master-top-admin-shadow-green">VALID</span></strong>. <a href="@spammaster_url" target="@spammaster_target">About Statuses</a>.', [
        '@lic_colour'        => $serviceInfo['license_status']['colour'],
        '@lic_status'        => $serviceInfo['license_status']['status_text'],
        '@spammaster_url'    => 'https://www.spammaster.org/documentation/#status',
        '@spammaster_target' => '_blank',
      ]),
    ];
    // Insert addrow protection field.
    $form['license_header']['license']['addrow1']['license_db_hash'] = [
      '#required' => FALSE,
      '#type' => 'item',
      '#title' => $this->t('Your database protection hash:'),
      '#description' => $this->t('<strong><span class="@hash_colour">@hash</span></strong> -> Ensures protection using unique license key and database hash.', [
        '@hash_colour' => 'spam-master-top-admin-shadow-green',
        '@hash'        => $this->state->get('spammaster.spam_master_db_protection_hash'),
      ]),
    ];
    // Insert addrow alert level field.
    $form['license_header']['license']['addrow1']['license_alert_level'] = [
      '#required' => FALSE,
      '#type' => 'item',
      '#title' => $this->t('Your alert level:'),
      '#description' => $this->t('<strong><span class="@alert_colour">@alert_label</span></strong> @alert_text <a href="@spammaster_url" target="@spammaster_target">About Alert Levels</a>.', [
        '@alert_colour'      => $serviceInfo['alert_level']['level_color'],
        '@alert_label'       => $serviceInfo['alert_level']['level_label'],
        '@alert_text'        => $serviceInfo['alert_level']['level_text'],
        '@spammaster_url'    => 'https://www.spammaster.org/documentation/#alert',
        '@spammaster_target' => '_blank',
      ]),
    ];
    // Insert addrow license status field.
    $form['license_header']['license']['addrow2']['license_protection'] = [
      '#required' => FALSE,
      '#type' => 'item',
      '#title' => $this->t('Spam Master servers real-time protection count (Threats & Exploits protection number):'),
      '#description' => $serviceInfo['license_status']['protection_total_number_text'],
    ];
    // Insert addrow alert level field.
    $form['license_header']['license']['addrow2']['license_probability'] = [
      '#required' => FALSE,
      '#type' => 'item',
      '#title' => $this->t('Your spam probability:'),
      '#description' => $this->t('<strong><span class="@spam_prob_colour">@spam_prob</span></strong>@spam_prob_alert', [
        '@spam_prob'        => $this->state->get('spammaster.license_probability') . '% ',
        '@spam_prob_colour' => $serviceInfo['alert_level']['level_color'],
        '@spam_prob_alert'  => $serviceInfo['alert_level']['level_p_label'],
      ]),
    ];
    $has_improvements = !empty($serviceInfo['improvements']);
    if ($has_improvements) {
      $improve_value = count($serviceInfo['improvements']);
      $improve_text = $this->t('You can improve your website performance and avoid validation errors by disabling modules that Spam Master replaces. Speed up your website page load times and SEO performance by decreasing CPU, Memory and Disk space used by these modules. Provide a better front-end experience for legitimate users while blocking spam and exploits.');
      $improve_list_text = Markup::create('<ul class="spam-master-admin-red spam-master-top-admin-shadow-offline"><li>' . implode('</li><li>', $serviceInfo['improvements']) . '</li></ul>');
      $improve_class = 'spam-master-admin-red spam-master-top-admin-shadow-offline';
    }
    else {
      $improve_value = '0';
      $improve_list_text = $this->t('Congratulations no improvements at this point.');
      $improve_class = 'spam-master-admin-green spam-master-top-admin-shadow-offline';
      if ('PRO' === $serviceInfo['license_type']['spam_type']) {
        $improve_text = $this->t('Congratulations no improvements found.');
      }
      else {
        $improve_text = $this->t('Congratulations no improvements found. You can still boost your connection to Spam Master RBL business servers with a PRO key.');
      }
    }
    // Insert addrow module handler field.
    $form['license_header']['license']['addrow3']['issues'] = [
      '#required' => FALSE,
      '#type' => 'item',
      '#title' => $this->t('Improvements found: <span class="@improve_class">@improve_value</span>', [
        '@improve_class' => $improve_class,
        '@improve_value' => $improve_value,
      ]),
      '#description' => $improve_text,
    ];
    $form['license_header']['license']['addrow3']['issues_list'] = [
      '#required' => FALSE,
      '#type' => 'item',
      '#title' => $this->t('Improvements list: <span class="@improve_class">@improve_value</span>', [
        '@improve_class' => $improve_class,
        '@improve_value' => $improve_value,
      ]),
      '#description' => $improve_list_text,
    ];
    // Insert addrow help field.
    $form['license_header']['license']['addrow4']['help'] = [
      '#required' => FALSE,
      '#type' => 'item',
      '#title' => $this->t('Support:'),
      '#description' => $this->t('Drop us an <a href="@mailto?subject=@mailsub&body=@mailbody">email</a>.', [
        '@mailto' => 'mailto:info@spammaster.org',
        '@mailsub' => 'Module support',
        '@mailbody' => $config->get('spammaster.license_key') . ' *** WRITE BELOW THIS LINE ***',
      ]),
    ];
    // Insert addrow help field.
    $form['license_header']['license']['addrow4']['documentation'] = [
      '#required' => FALSE,
      '#type' => 'item',
      '#title' => $this->t('Documentation:'),
      '#description' => $this->t('Visit Spam Master <a href="@spammaster_url" target="@spammaster_target">Documentation</a>.', [
        '@spammaster_url' => 'https://www.spammaster.org/documentation/',
        '@spammaster_target' => '_blank',
      ]),
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * Get SpamMaster service info.
   *
   * This includes license info and alert level.
   *
   * @param string $response_key
   *   The SpamMaster service response key.
   *
   * @return array
   *   SpamMaster Service information.
   */
  private function getServiceInfo($response_key): array {
    return [
      'license_type'   => $this->getLicenseTypeInfo(),
      'license_status' => $this->getLicenseStatus($response_key),
      'alert_level'    => $this->getAlertLevelInfo(),
      'improvements'   => $this->getImprovements($response_key),
    ];
  }

  /**
   * Get SpamMaster License Type info to display.
   *
   * @return array
   *   License type information.
   */
  private function getLicenseTypeInfo(): array {
    $license_type = $this->state->get('spammaster.type');
    // Assume free service by default.
    $license_info = [
      'spam_type'            => $license_type,
      'license_type'         => $this->t('@license_type -> Connected to Spam Master Free Servers.', [
        '@license_type' => $license_type,
      ]),
      'license_class'        => 'spam-master-admin-orangina spam-master-top-admin-shadow-red',
      'license_type_upgrade' => $this->t('You can improve your connection to our business class servers with a PRO key. <a href="@spammaster_serverstatus" target="@spammaster_target">Spam Master Service > Servers Status</a>.', [
        '@spammaster_serverstatus' => 'https://www.spammaster.org/rbl-servers-status/',
        '@spammaster_target' => '_blank',
      ]),
    ];
    if ('FULL' === $license_type) {
      $license_info = [
        'spam_type'            => 'PRO',
        'license_type'         => $this->t('PRO -> Connected to Spam Master Business Class Servers.'),
        'license_class'        => 'spam-master-admin-green spam-master-top-admin-shadow-offline',
        'license_type_upgrade' => $this->t('Congratulations, you have a PRO connection to our business class servers.'),
      ];
    }
    return $license_info;
  }

  /**
   * Get SpamMaster License Status.
   *
   * @param string $response_key
   *   The SpamMaster service response key.
   *
   * @return array
   *   License status information.
   */
  private function getLicenseStatus($response_key): array {
    // Statuses  Settings.
    $spammaster_protection_total_number = $this->state->get('spammaster.license_protection');
    $spammaster_attached = $this->state->get('spammaster.spam_master_attached');
    if (empty($spammaster_attached) || is_array($spammaster_attached)) {
      $spammaster_attached = 'ERROR detecting email';
    }

    $licenseStatusInfo = [
      'status_text'                  => '',
      'colour'                       => '',
      'protection_total_number_text' => $this->t('You are protected against <strong><span class="spam-master-top-admin-shadow-green">@count</span></strong> Million Threats & Exploits.', [
        '@count' => number_format($spammaster_protection_total_number),
      ]),
      'email_attached' => $this->t('Your license key is attached to email:&nbsp;<strong>@email</strong>&nbsp;', ['@email' => $spammaster_attached]),
    ];
    switch ($response_key) {
      case 'VALID':
        $licenseStatusInfo['status_text'] = 'VALID LICENSE';
        $licenseStatusInfo['colour'] = 'spam-master-admin-green spam-master-top-admin-shadow-offline';
        break;

      case 'EXPIRED':
        $licenseStatusInfo['status_text'] = 'EXPIRED LICENSE';
        $licenseStatusInfo['colour'] = 'spam-master-admin-red spam-master-top-admin-shadow-offline';
        $licenseStatusInfo['protection_total_number_text'] = $this->t('WARNING protected against 0 Threats & Exploits - EXPIRED OFFLINE');
        break;

      case 'MALFUNCTION_1':
      case 'MALFUNCTION_2':
        $licenseStatusInfo['status_text'] = 'VALID LICENSE';
        $licenseStatusInfo['colour'] = 'spam-master-admin-yellow spam-master-top-admin-shadow-offline';
        break;

      case 'MALFUNCTION_3':
      case 'MALFUNCTION_4':
      case 'MALFUNCTION_5':
      case 'MALFUNCTION_6':
      case 'MALFUNCTION_7':
      case 'MALFUNCTION_8':
      case 'CODE_TAMPER_1':
      case 'CODE_TAMPER_2':
      case 'DISCONNECTED':
        $licenseStatusInfo['status_text'] = $response_key . ' OFFLINE';
        $licenseStatusInfo['colour'] = 'spam-master-admin-red spam-master-top-admin-shadow-offline';
        $licenseStatusInfo['protection_total_number_text'] = $this->t('WARNING protected against 0 Threats & Exploits - @response_key OFFLINE', [
          '@response_key' => $response_key,
        ]);
        break;

      case 'UNSTABLE':
      case 'HIGH_VOLUME':
        $licenseStatusInfo['status_text'] = $response_key . ' OFFLINE';
        $licenseStatusInfo['colour'] = 'spam-master-admin-red spam-master-top-admin-shadow-offline';
        $licenseStatusInfo['protection_total_number_text'] = $this->t('WARNING protected against 0 Threats & Exploits - Upgrade to PRO');
        break;

      case 'INACTIVE':
        $licenseStatusInfo['status_text'] = 'INACTIVE LICENSE';
        $licenseStatusInfo['colour'] = 'spam-master-admin-yellow spam-master-top-admin-shadow-offline';
        $licenseStatusInfo['protection_total_number_text'] = $this->t('WARNING protected against 0 Threats & Exploits - INACTIVE OFFLINE');
        break;
    }
    return $licenseStatusInfo;
  }

  /**
   * Get SpamMaster Alert Level info to display.
   *
   * @return array
   *   Alert level information.
   */
  private function getAlertLevelInfo(): array {
    // Alert Level Settings.
    $spammaster_alert_level = $this->state->get('spammaster.license_alert_level');
    $alert_colours = [
      'ALERT_0' => 'spam-master-admin-green spam-master-top-admin-shadow-offline',
      'ALERT_1' => 'spam-master-admin-yellow spam-master-top-admin-shadow-offline',
      'ALERT_2' => 'spam-master-admin-orangina spam-master-top-admin-shadow-offline',
      'ALERT_3' => 'spam-master-admin-red spam-master-top-admin-shadow-offline',
    ];
    $alert_texts = [
      'ALERT_0' => $this->t('-> Low level of spam and threats. Your website is mainly being visited by occasional harvester bots.'),
      'ALERT_1' => $this->t('-> Low level of spam and threats. Your website is mainly being visited by occasional human spammers and harvester bots.'),
      'ALERT_2' => $this->t('-> Medium level of spam and threats. Spam Master is actively fighting constant attempts of spam and threats by machine bots.'),
      'ALERT_3' => $this->t('-> WARNING! High level of spam and threats, flood detected. Spam Master is fighting an array of human spammers and bot networks which include exploit attempts.'),
    ];
    $alertLevelInfo = [
      'level_label'   => '',
      'level_text'    => $this->t('Empty data.'),
      'level_color'   => $alert_colours['ALERT_3'],
      'level_p_label' => ' ' . $this->t('Empty data.'),
    ];
    if (!empty($spammaster_alert_level)) {
      $alertLevelInfo['level_label'] = $spammaster_alert_level . ' ';
      switch ($spammaster_alert_level) {
        case 'MALFUNCTION_3':
        case 'MALFUNCTION_4':
        case 'MALFUNCTION_5':
        case 'MALFUNCTION_6':
        case 'MALFUNCTION_7':
        case 'MALFUNCTION_8':
        case 'UNSTABLE':
        case 'HIGH_VOLUME':
        case 'DISCONNECTED':
          $alertLevelInfo['level_color']   = $alert_colours['ALERT_3'];
          $alertLevelInfo['level_text']    = $this->t('-> No RBL (real-time blacklist) Server Sync.');
          $alertLevelInfo['level_p_label'] = "";
          break;

        case 'ALERT_0':
        case 'ALERT_1':
        case 'ALERT_2':
        case 'ALERT_3':
          // Convert 'ALERT_N' to 'Alert N'.
          $alertLevelInfo['level_label']   = $this->t('Alert @level', [
            '@level' => substr($spammaster_alert_level, -1),
          ]);
          $alertLevelInfo['level_color']   = $alert_colours[$spammaster_alert_level];
          $alertLevelInfo['level_p_label'] = ' ' . $this->t('percent probability.');
          $alertLevelInfo['level_text']    = $alert_texts[$spammaster_alert_level];
          break;

      }
    }
    return $alertLevelInfo;
  }

  /**
   * Get list of recommended improvements.
   *
   * This includes modules that should be disabled because they
   * are replaced by Spam Master.
   *
   * @param string $response_key
   *   The SpamMaster service response key.
   *
   * @return array
   *   Improvements information.
   */
  private function getImprovements($response_key): array {
    $improvements = [];
    if ('VALID' !== $response_key) {
      $improvements[] = $this->t('Check Spam Master documentation to address the issue or malfunction.');
    }
    foreach ($this->unneededModules as $module) {
      if ($this->moduleHandler->moduleExists($module)) {
        // Get the module's human-readable name.
        $module_info = $this->moduleList->getExtensionInfo($module);
        $improvements[] = $this->t('Uninstall the %module_name module.', [
          '%module_name' => $module_info['name'],
        ]);
      }
    }
    return $improvements;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('license_header')['basic_option']['addrow']['license_key'])) {
      $form_state->setErrorByName('license_header', $this->t('License key can not be empty.'));
    }
    if (empty($form_state->getValue('license_header')['basic_option']['addrow']['subtype'])) {
      $form_state->setErrorByName('license_header', $this->t('Website implementation type can not be empty.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('spammaster.settings');
    $config->set('spammaster.license_key', $form_state->getValue('license_header')['basic_option']['addrow']['license_key']);
    $config->set('spammaster.subtype', $form_state->getValue('license_header')['basic_option']['addrow']['subtype']);
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'spammaster.settings',
    ];
  }

}
