<?php

namespace Drupal\spammaster\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Routing\TrustedRedirectResponse;

/**
 * Class controller.
 */
class SpamMasterLogForm extends ConfigFormBase {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, MessengerInterface $messenger, StateInterface $state, RequestStack $requestStack) {
    $this->connection = $connection;
    $this->messenger = $messenger;
    $this->state = $state;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('messenger'),
      $container->get('state'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spammaster_settings_log_form';
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterStatisticsPage($form, &$form_state) {
    $spam_get_statistics = $form_state->getValue('statistics_header')['buttons']['addrow']['statistics'];
    if (!empty($spam_get_statistics)) {
      $spammaster_build_statistics_url = 'https://' . $_SERVER['SERVER_NAME'] . '/statistics';
      $spammaster_statistics_url = Url::fromUri($spammaster_build_statistics_url);
      $form_state->setRedirectUrl($spammaster_statistics_url);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterStatisticsOnline($form, &$form_state) {
    $spam_get_online = $form_state->getValue('statistics_header')['buttons']['addrow']['online'];
    if (!empty($spam_get_online)) {
      $online_statistics_url = 'https://www.spammaster.org/websites-stats/';
      $redirect_response = new TrustedRedirectResponse($online_statistics_url);
      $form_state->setResponse($redirect_response, 302);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterFirewallPage($form, &$form_state) {
    $spam_get_firewall = $form_state->getValue('statistics_header')['buttons']['addrow']['firewall'];
    if (!empty($spam_get_firewall)) {
      $spammaster_build_firewall_url = 'https://' . $_SERVER['SERVER_NAME'] . '/firewall';
      $spammaster_firewall_url = Url::fromUri($spammaster_build_firewall_url);
      $form_state->setRedirectUrl($spammaster_firewall_url);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterDeleteKey($form, &$form_state) {
    $spam_form_key_delete = $form_state->getValue('statistics_header')['table_key'];
    $spammaster_key_date = date("Y-m-d H:i:s");
    foreach ($spam_form_key_delete as $spam_key_delete) {
      if (!empty($spam_key_delete)) {
        $this->connection->delete('spammaster_keys')
          ->condition('spamkey', 'spammaster', '=')
          ->condition('id', $spam_key_delete, '=')
          ->execute();
        $this->messenger->addMessage($this->t('Saved Spam Master Log deletion.'));
        $this->connection->insert('spammaster_keys')->fields([
          'date' => $spammaster_key_date,
          'spamkey' => 'spammaster-log',
          'spamvalue' => 'Spam Master: log deletion, Id: ' . $spam_key_delete,
        ])->execute();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterDeleteKeysAll() {
    $this->state->set('spammaster.total_block_count', '0');
    $this->connection->delete('spammaster_keys')
      ->condition('spamkey', 'exempt-needle', '!=')
      ->condition('spamkey', 'exempt-needle-straw', '!=')
      ->condition('spamkey', 'exempt-needle-sig-hide', '!=')
      ->condition('spamkey', 'exempt-needle-sig-show', '!=')
      ->condition('spamkey', 'exempt-key', '!=')
      ->condition('spamkey', 'exempt-value', '!=')
      ->condition('spamkey', 'white-transient-haf', '!=')
      ->condition('spamkey', 'white-transient-form', '!=')
      ->execute();
    $this->messenger->addMessage($this->t('Saved Spam Master Statistics & Logs full deletion.'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    $form['statistics_header'] = [
      '#type' => 'details',
      '#title' => $this->t('<h3>Statistics & Logging</h3>'),
      '#tree' => TRUE,
      '#open' => TRUE,
      '#attached' => [
        'library' => [
          'spammaster/spammaster-styles',
        ],
      ],
    ];

    $spam_master_type = $this->state->get('spammaster.type');
    $spam_master_attached = $this->state->get('spammaster.spam_master_attached');
    $spammaster_protection_total_number = $this->state->get('spammaster.license_protection');
    if ('FULL' === $spam_master_type) {

      // Form description.
      $form['statistics_header']['header_description'] = [
        '#markup' => $this->t('<p>Click the button below to check your local statistics. In order to access your online statistics please create an account and login at <a href="@spammaster_base_url" target="@spammaster_target"><b>www.spammaster.org</b></a> website with the license attached email <b>@spam_master_attached</b>.</p>', [
          '@spammaster_base_url' => 'https://www.spammaster.org',
          '@spammaster_target' => '_blank',
          '@spam_master_attached' => $spam_master_attached,
        ]),
      ];

      // Create buttons table.
      $form['statistics_header']['buttons'] = [
        '#type' => 'table',
        '#header' => [],
      ];
      // Insert addrow statistics button.
      $form['statistics_header']['buttons']['addrow']['statistics'] = [
        '#type' => 'submit',
        '#attributes' => [
          'class' => ['button button--primary'],
        ],
        '#value' => $this->t('Visit Local Statistics Page'),
        '#submit' => ['::spamMasterStatisticsPage'],
      ];
      // Insert addrow statistics online button.
      $form['statistics_header']['buttons']['addrow']['online'] = [
        '#type' => 'submit',
        '#attributes' => [
          'class' => ['button button--primary'],
        ],
        '#value' => $this->t('Visit Online Statistics Page'),
        '#submit' => ['::spamMasterStatisticsOnline'],
      ];
      // Insert addrow firewall button.
      $form['statistics_header']['buttons']['addrow']['firewall'] = [
        '#type' => 'submit',
        '#attributes' => [
          'class' => ['button button--primary'],
        ],
        '#value' => $this->t('Visit your Firewall Page'),
        '#submit' => ['::spamMasterFirewallPage'],
      ];

      $spammaster_total_block_count = $this->state->get('spammaster.total_block_count');
      if (empty($spammaster_total_block_count)) {
        $spammaster_total_block_count = '0';
      }
      // Insert statistics table inside tree.
      $form['statistics_header']['total_block_count'] = [
        '#markup' => $this->t('<h2>Total Firewall Blocks: <span class="spam-master-admin-red spam-master-top-admin-shadow-offline">@total_blocks</span></h2>', ['@total_blocks' => $spammaster_total_block_count]),
        '#attributes' => [
          'class' => [
            'spam-master-admin-red',
            'spam-master-top-admin-shadow-offline',
          ],
        ],
      ];

      $form['statistics_header']['statistics'] = [
        '#type' => 'table',
        '#header' => [
          'logs' => $this->t('All Logs'),
          'system_logs' => $this->t('System Logs'),
          'firewall' => $this->t('Firewall'),
          'honeypot' => $this->t('Honeypot'),
        ],
      ];
      // Set wide dates.
      $time = date('Y-m-d H:i:s');
      $time_expires_1_day = date('Y-m-d H:i:s', strtotime($time . '-1 days'));
      $time_expires_7_days = date('Y-m-d H:i:s', strtotime($time . '-7 days'));
      $time_expires_31_days = date('Y-m-d H:i:s', strtotime($time . '-31 days'));

      // Generate all logs stats 1 day.
      $spammaster_all_logs_1 = $this->connection->select('spammaster_keys', 'u');
      $spammaster_all_logs_1->fields('u', ['spamkey']);
      $spammaster_all_logs_1->condition('u.date', [$time_expires_1_day, $time], 'BETWEEN');
      $spammaster_all_logs_1->condition('u.spamkey', 'exempt-needle', '!=');
      $spammaster_all_logs_1->condition('u.spamkey', 'exempt-needle-straw', '!=');
      $spammaster_all_logs_1->condition('u.spamkey', 'exempt-needle-sig-hide', '!=');
      $spammaster_all_logs_1->condition('u.spamkey', 'exempt-needle-sig-show', '!=');
      $spammaster_all_logs_1->condition('u.spamkey', 'exempt-key', '!=');
      $spammaster_all_logs_1->condition('u.spamkey', 'exempt-value', '!=');
      $spammaster_all_logs_1->condition('u.spamkey', 'white-transient-haf', '!=');
      $spammaster_all_logs_1->condition('u.spamkey', 'white-transient-form', '!=');
      $spammaster_all_logs_1_result = $spammaster_all_logs_1->countQuery()->execute()->fetchField();
      // Generate all logs stats 7 days.
      $spammaster_all_logs_7 = $this->connection->select('spammaster_keys', 'u');
      $spammaster_all_logs_7->fields('u', ['spamkey']);
      $spammaster_all_logs_7->condition('u.date', [$time_expires_7_days, $time], 'BETWEEN');
      $spammaster_all_logs_7->condition('u.spamkey', 'exempt-needle', '!=');
      $spammaster_all_logs_7->condition('u.spamkey', 'exempt-needle-straw', '!=');
      $spammaster_all_logs_7->condition('u.spamkey', 'exempt-needle-sig-hide', '!=');
      $spammaster_all_logs_7->condition('u.spamkey', 'exempt-needle-sig-show', '!=');
      $spammaster_all_logs_7->condition('u.spamkey', 'exempt-key', '!=');
      $spammaster_all_logs_7->condition('u.spamkey', 'exempt-value', '!=');
      $spammaster_all_logs_7->condition('u.spamkey', 'white-transient-haf', '!=');
      $spammaster_all_logs_7->condition('u.spamkey', 'white-transient-form', '!=');
      $spammaster_all_logs_7_result = $spammaster_all_logs_7->countQuery()->execute()->fetchField();
      // Generate all logs stats 31 days.
      $spammaster_all_logs_31 = $this->connection->select('spammaster_keys', 'u');
      $spammaster_all_logs_31->fields('u', ['spamkey']);
      $spammaster_all_logs_31->condition('u.date', [
        $time_expires_31_days,
        $time,
      ], 'BETWEEN');
      $spammaster_all_logs_31->condition('u.spamkey', 'exempt-needle', '!=');
      $spammaster_all_logs_31->condition('u.spamkey', 'exempt-needle-straw', '!=');
      $spammaster_all_logs_31->condition('u.spamkey', 'exempt-needle-sig-hide', '!=');
      $spammaster_all_logs_31->condition('u.spamkey', 'exempt-needle-sig-show', '!=');
      $spammaster_all_logs_31->condition('u.spamkey', 'exempt-key', '!=');
      $spammaster_all_logs_31->condition('u.spamkey', 'exempt-value', '!=');
      $spammaster_all_logs_31->condition('u.spamkey', 'white-transient-haf', '!=');
      $spammaster_all_logs_31->condition('u.spamkey', 'white-transient-form', '!=');
      $spammaster_all_logs_31_result = $spammaster_all_logs_31->countQuery()->execute()->fetchField();
      // Generate all logs stats total.
      $spammaster_all_logs = $this->connection->select('spammaster_keys', 'u');
      $spammaster_all_logs->fields('u', ['spamkey']);
      $spammaster_all_logs->condition('u.spamkey', 'exempt-needle', '!=');
      $spammaster_all_logs->condition('u.spamkey', 'exempt-needle-straw', '!=');
      $spammaster_all_logs->condition('u.spamkey', 'exempt-needle-sig-hide', '!=');
      $spammaster_all_logs->condition('u.spamkey', 'exempt-needle-sig-show', '!=');
      $spammaster_all_logs->condition('u.spamkey', 'exempt-key', '!=');
      $spammaster_all_logs->condition('u.spamkey', 'exempt-value', '!=');
      $spammaster_all_logs->condition('u.spamkey', 'white-transient-haf', '!=');
      $spammaster_all_logs->condition('u.spamkey', 'white-transient-form', '!=');
      $spammaster_all_logs_result = $spammaster_all_logs->countQuery()->execute()->fetchField();
      $form['statistics_header']['statistics']['addrow']['logs'] = [
        '#markup' => $this->t('<p>Daily Entries: <b>@logs_1</b></p><p>Weekly Entries: <b>@logs_7</b></p><p>Monthly Entries: <b>@logs_31</b></p><p>Total Entries: <b>@logs_total</b></p>', [
          '@logs_1' => $spammaster_all_logs_1_result,
          '@logs_7' => $spammaster_all_logs_7_result,
          '@logs_31' => $spammaster_all_logs_31_result,
          '@logs_total' => $spammaster_all_logs_result,
        ]),
      ];

      // Generate system logs stats 1 day.
      $spammaster_system_logs_1 = $this->connection->select('spammaster_keys', 'u');
      $spammaster_system_logs_1->fields('u', ['spamkey']);
      $spammaster_system_logs_1->condition('u.date', [
        $time_expires_1_day,
        $time,
      ], 'BETWEEN');
      $spammaster_system_logs_1->condition('u.spamkey', 'exempt-needle', '!=');
      $spammaster_system_logs_1->condition('u.spamkey', 'exempt-needle-straw', '!=');
      $spammaster_system_logs_1->condition('u.spamkey', 'exempt-needle-sig-hide', '!=');
      $spammaster_system_logs_1->condition('u.spamkey', 'exempt-needle-sig-show', '!=');
      $spammaster_system_logs_1->condition('u.spamkey', 'exempt-key', '!=');
      $spammaster_system_logs_1->condition('u.spamkey', 'exempt-value', '!=');
      $spammaster_system_logs_1->condition('u.spamkey', 'white-transient-haf', '!=');
      $spammaster_system_logs_1->condition('u.spamkey', 'white-transient-form', '!=');
      $spammaster_system_logs_1->condition('u.spamkey', 'spammaster-firewall', '!=');
      $spammaster_system_logs_1->condition('u.spamkey', 'spammaster-honeypot', '!=');
      $spammaster_system_logs_1_result = $spammaster_system_logs_1->countQuery()->execute()->fetchField();
      // Generate system logs stats 7 days.
      $spammaster_system_logs_7 = $this->connection->select('spammaster_keys', 'u');
      $spammaster_system_logs_7->fields('u', ['spamkey']);
      $spammaster_system_logs_7->condition('u.date', [
        $time_expires_7_days,
        $time,
      ], 'BETWEEN');
      $spammaster_system_logs_7->condition('u.spamkey', 'exempt-needle', '!=');
      $spammaster_system_logs_7->condition('u.spamkey', 'exempt-needle-straw', '!=');
      $spammaster_system_logs_7->condition('u.spamkey', 'exempt-needle-sig-hide', '!=');
      $spammaster_system_logs_7->condition('u.spamkey', 'exempt-needle-sig-show', '!=');
      $spammaster_system_logs_7->condition('u.spamkey', 'exempt-key', '!=');
      $spammaster_system_logs_7->condition('u.spamkey', 'exempt-value', '!=');
      $spammaster_system_logs_7->condition('u.spamkey', 'white-transient-haf', '!=');
      $spammaster_system_logs_7->condition('u.spamkey', 'white-transient-form', '!=');
      $spammaster_system_logs_7->condition('u.spamkey', 'spammaster-firewall', '!=');
      $spammaster_system_logs_7->condition('u.spamkey', 'spammaster-honeypot', '!=');
      $spammaster_system_logs_7_result = $spammaster_system_logs_7->countQuery()->execute()->fetchField();
      // Generate system logs stats 31 days.
      $spammaster_system_logs_31 = $this->connection->select('spammaster_keys', 'u');
      $spammaster_system_logs_31->fields('u', ['spamkey']);
      $spammaster_system_logs_31->condition('u.date', [
        $time_expires_31_days,
        $time,
      ], 'BETWEEN');
      $spammaster_system_logs_31->condition('u.spamkey', 'exempt-needle', '!=');
      $spammaster_system_logs_31->condition('u.spamkey', 'exempt-needle-straw', '!=');
      $spammaster_system_logs_31->condition('u.spamkey', 'exempt-needle-sig-hide', '!=');
      $spammaster_system_logs_31->condition('u.spamkey', 'exempt-needle-sig-show', '!=');
      $spammaster_system_logs_31->condition('u.spamkey', 'exempt-key', '!=');
      $spammaster_system_logs_31->condition('u.spamkey', 'exempt-value', '!=');
      $spammaster_system_logs_31->condition('u.spamkey', 'white-transient-haf', '!=');
      $spammaster_system_logs_31->condition('u.spamkey', 'white-transient-form', '!=');
      $spammaster_system_logs_31->condition('u.spamkey', 'spammaster-firewall', '!=');
      $spammaster_system_logs_31->condition('u.spamkey', 'spammaster-honeypot', '!=');
      $spammaster_system_logs_31_result = $spammaster_system_logs_31->countQuery()->execute()->fetchField();
      // Generate system logs stats total.
      $spammaster_system_logs = $this->connection->select('spammaster_keys', 'u');
      $spammaster_system_logs->fields('u', ['spamkey']);
      $spammaster_system_logs->condition('u.spamkey', 'exempt-needle', '!=');
      $spammaster_system_logs->condition('u.spamkey', 'exempt-needle-straw', '!=');
      $spammaster_system_logs->condition('u.spamkey', 'exempt-needle-sig-hide', '!=');
      $spammaster_system_logs->condition('u.spamkey', 'exempt-needle-sig-show', '!=');
      $spammaster_system_logs->condition('u.spamkey', 'exempt-key', '!=');
      $spammaster_system_logs->condition('u.spamkey', 'exempt-value', '!=');
      $spammaster_system_logs->condition('u.spamkey', 'white-transient-haf', '!=');
      $spammaster_system_logs->condition('u.spamkey', 'white-transient-form', '!=');
      $spammaster_system_logs->condition('u.spamkey', 'spammaster-firewall', '!=');
      $spammaster_system_logs->condition('u.spamkey', 'spammaster-honeypot', '!=');
      $spammaster_system_logs_result = $spammaster_system_logs->countQuery()->execute()->fetchField();
      $form['statistics_header']['statistics']['addrow']['system_logs'] = [
        '#markup' => $this->t('<p>Daily Entries: <b>@logs_system_1</b></p><p>Weekly Entries: <b>@logs_system_7</b></p><p>Monthly Entries: <b>@logs_system_31</b></p><p>Total Entries: <b>@logs_system_total</b></p>', [
          '@logs_system_1' => $spammaster_system_logs_1_result,
          '@logs_system_7' => $spammaster_system_logs_7_result,
          '@logs_system_31' => $spammaster_system_logs_31_result,
          '@logs_system_total' => $spammaster_system_logs_result,
        ]),
      ];

      // Generate firewall stats 1 day.
      $spammaster_firewall_1 = $this->connection->select('spammaster_keys', 'u');
      $spammaster_firewall_1->fields('u', ['spamkey']);
      $spammaster_firewall_1->condition('u.date', [
        $time_expires_1_day,
        $time,
      ], 'BETWEEN');
      $spammaster_firewall_1->condition('u.spamkey', 'spammaster-firewall', '=');
      $spammaster_firewall_1_result = $spammaster_firewall_1->countQuery()->execute()->fetchField();
      // Generate firewall stats 7 days.
      $spammaster_firewall_7 = $this->connection->select('spammaster_keys', 'u');
      $spammaster_firewall_7->fields('u', ['spamkey']);
      $spammaster_firewall_7->condition('u.date', [
        $time_expires_7_days,
        $time,
      ], 'BETWEEN');
      $spammaster_firewall_7->condition('u.spamkey', 'spammaster-firewall', '=');
      $spammaster_firewall_7_result = $spammaster_firewall_7->countQuery()->execute()->fetchField();
      // Generate firewall stats 31 days.
      $spammaster_firewall_31 = $this->connection->select('spammaster_keys', 'u');
      $spammaster_firewall_31->fields('u', ['spamkey']);
      $spammaster_firewall_31->condition('u.date', [
        $time_expires_31_days,
        $time,
      ], 'BETWEEN');
      $spammaster_firewall_31->condition('u.spamkey', 'spammaster-firewall', '=');
      $spammaster_firewall_31_result = $spammaster_firewall_31->countQuery()->execute()->fetchField();
      // Generate firewall stats total.
      $spammaster_firewall = $this->connection->select('spammaster_keys', 'u');
      $spammaster_firewall->fields('u', ['spamkey']);
      $spammaster_firewall->condition('u.spamkey', 'spammaster-firewall', '=');
      $spammaster_firewall_result = $spammaster_firewall->countQuery()->execute()->fetchField();
      $form['statistics_header']['statistics']['addrow']['firewall'] = [
        '#markup' => $this->t('<p>Daily Entries: <b>@firewall_1</b></p><p>Weekly Entries: <b>@firewall_7</b></p><p>Monthly Entries: <b>@firewall_31</b></p><p>Total Entries: <b>@firewall_total</b></p>', [
          '@firewall_1' => $spammaster_firewall_1_result,
          '@firewall_7' => $spammaster_firewall_7_result,
          '@firewall_31' => $spammaster_firewall_31_result,
          '@firewall_total' => $spammaster_firewall_result,
        ]),
      ];

      // Generate honeypot stats 1 day.
      $spammaster_honeypot_1 = $this->connection->select('spammaster_keys', 'u');
      $spammaster_honeypot_1->fields('u', ['spamkey']);
      $spammaster_honeypot_1->condition('u.date', [
        $time_expires_1_day,
        $time,
      ], 'BETWEEN');
      $spammaster_honeypot_1->condition('u.spamkey', 'spammaster-honeypot', '=');
      $spammaster_honeypot_1_result = $spammaster_honeypot_1->countQuery()->execute()->fetchField();
      // Generate honeypot stats 7 days.
      $spammaster_honeypot_7 = $this->connection->select('spammaster_keys', 'u');
      $spammaster_honeypot_7->fields('u', ['spamkey']);
      $spammaster_honeypot_7->condition('u.date', [
        $time_expires_7_days,
        $time,
      ], 'BETWEEN');
      $spammaster_honeypot_7->condition('u.spamkey', 'spammaster-honeypot', '=');
      $spammaster_honeypot_7_result = $spammaster_honeypot_7->countQuery()->execute()->fetchField();
      // Generate honeypot stats 31 days.
      $spammaster_honeypot_31 = $this->connection->select('spammaster_keys', 'u');
      $spammaster_honeypot_31->fields('u', ['spamkey']);
      $spammaster_honeypot_31->condition('u.date', [
        $time_expires_31_days,
        $time,
      ], 'BETWEEN');
      $spammaster_honeypot_31->condition('u.spamkey', 'spammaster-honeypot', '=');
      $spammaster_honeypot_31_result = $spammaster_honeypot_31->countQuery()->execute()->fetchField();
      // Generate honeypot stats total.
      $spammaster_honeypot = $this->connection->select('spammaster_keys', 'u');
      $spammaster_honeypot->fields('u', ['spamkey']);
      $spammaster_honeypot->condition('u.spamkey', 'spammaster-honeypot', '=');
      $spammaster_honeypot_result = $spammaster_honeypot->countQuery()->execute()->fetchField();
      $form['statistics_header']['statistics']['addrow']['honeypot'] = [
        '#markup' => $this->t('<p>Daily Entries: <b>@honeypot_1</b></p><p>Weekly Entries: <b>@honeypot_7</b></p><p>Monthly Entries: <b>@honeypot_31</b></p><p>Total Entries: <b>@honeypot_total</b></p>', [
          '@honeypot_1' => $spammaster_honeypot_1_result,
          '@honeypot_7' => $spammaster_honeypot_7_result,
          '@honeypot_31' => $spammaster_honeypot_31_result,
          '@honeypot_total' => $spammaster_honeypot_result,
        ]),
      ];

      // Construct header.
      $header_key = [
        'id' => [
          'data' => $this->t('ID'),
          'field'  => 'id',
          'specifier' => 'id',
          'sort' => 'desc',
        ],
        'date' => [
          'data' => $this->t('Date'),
          'field'  => 'date',
          'specifier' => 'date',
          'sort' => 'desc',
        ],
        'spamkey' => [
          'data' => $this->t('Type'),
          'field'  => 'spamkey',
          'specifier' => 'spamkey',
          'sort' => 'desc',
        ],
        'spamvalue' => [
          'data' => $this->t('Description'),
          'field'  => 'spamvalue',
          'specifier' => 'spamvalue',
          'sort' => 'desc',
        ],
      ];
      // Get table spammaster_keys data.
      $spammaster_spam_key = $this->connection->select('spammaster_keys', 'u')
        ->fields('u', ['id', 'date', 'spamkey', 'spamvalue'])
        ->condition('u.spamkey', 'exempt-needle', '!=')
        ->condition('u.spamkey', 'exempt-key', '!=')
        ->condition('u.spamkey', 'exempt-value', '!=')
        ->condition('u.spamkey', 'exempt-needle-straw', '!=')
        ->condition('u.spamkey', 'exempt-needle-sig-hide', '!=')
        ->condition('u.spamkey', 'exempt-needle-sig-show', '!=')
        ->condition('u.spamkey', 'white-transient-haf', '!=')
        ->condition('u.spamkey', 'white-transient-form', '!=')
        ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header_key)
        ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(20)
        ->execute()->fetchAll();

      $output_key = [];
      foreach ($spammaster_spam_key as $results_key) {
        if (!empty($results_key)) {
          $output_key[$results_key->id] = [
            'id' => $results_key->id,
            'date' => $results_key->date,
            'spamkey' => $results_key->spamkey,
            'spamvalue' => $results_key->spamvalue,
          ];
        }
      }
      // Display table.
      $form['statistics_header']['table_key'] = [
        '#type' => 'tableselect',
        '#header' => $header_key,
        '#options' => $output_key,
        '#empty' => $this->t('No log found'),
      ];
      // Spam log description.
      $form['statistics_header']['description'] = [
        '#markup' => $this->t('<p>Before deleting! Spam Master keeps local logs for a maximum time of 90 days (check Protection Tools Clean-up options). Older logs are automatically deleted via weekly cron to keep your website clean and fast.</p>'),
      ];
      // Delete button at end of table, calls spammasterdeletekey function.
      $form['statistics_header']['submit'] = [
        '#type' => 'submit',
        '#attributes' => [
          'class' => ['button button--danger'],
        ],
        '#value' => $this->t('Delete Log Entry'),
        '#submit' => ['::spamMasterDeleteKey'],
      ];
      // Delete button at end of table, calls spammasterdeletekeysall function.
      $form['statistics_header']['submit_all'] = [
        '#type' => 'submit',
        '#attributes' => [
          'class' => ['button button--danger'],
        ],
        '#value' => $this->t('Delete all Statistics & Logs -> Caution, no way back'),
        '#submit' => ['::spamMasterDeleteKeysAll'],
      ];
      // Form pager if ore than 25 entries.
      $form['statistics_header']['pager'] = [
        '#type' => 'pager',
      ];
    }
    else {
      // Form description.
      $image_check = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() . '/modules/spammaster/images/techgasp-checkmark-16.png';
      $spammaster_total_block_count = $this->state->get('spammaster.total_block_count');
      if (empty($spammaster_total_block_count)) {
        $spammaster_total_block_count = '0';
      }
      // Insert statistics table inside tree.
      $form['statistics_header']['total_block_count'] = [
        '#markup' => $this->t('<h2>Total Firewall Blocks: <span class="spam-master-admin-red spam-master-top-admin-shadow-offline">@total_blocks</span></h2>', ['@total_blocks' => $spammaster_total_block_count]),
        '#attributes' => [
          'class' => [
            'spam-master-admin-red',
            'spam-master-top-admin-shadow-offline',
          ],
        ],
      ];
      $form['statistics_header']['header_description'] = [
        '#markup' => $this->t('<p>Thank you for using Spam Master one of the top 5 world-wide, real-time spam and exploits databases with more than <b>@spammaster_protection_total_number</b> million threats and growing daily by the hundreds. We want to let you know that you can have free extensive detailed logging at our website <a href="@spammaster_base_url" target="@spammaster_target"><b>www.spammaster.org</b></a> if you register and login with your license attached email <b>@spam_master_attached</b>. If you want to view detailed logs here and support Spam Master project at the same time please consider a <a href="@spammaster_url" target="@spammaster_target"><img src="@image_check" alt="Get Pro Key"> <b>PRO KEY</b></a> it costs peanuts per year.</p>', [
          '@spammaster_protection_total_number' => number_format($spammaster_protection_total_number),
          '@spam_master_attached' => $spam_master_attached,
          '@spammaster_base_url' => 'https://www.spammaster.org',
          '@spammaster_url' => 'https://www.techgasp.com/downloads/spam-master-license/',
          '@spammaster_target' => '_blank',
          '@image_check' => $image_check,
        ]),
        '#allowed_tags' => ['p', 'img', 'b'],
      ];
      $form['statistics_header']['header_description']['table'] = [
        '#type' => 'table',
        '#attributes' => [
          'class' => [
            'spammaster-responsive',
          ],
        ],
      ];
      // Insert addrow teaser image.
      $image_teaser1 = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() . '/modules/spammaster/images/techgasp-logs.jpg';
      $form['statistics_header']['header_description']['table']['images']['teaser1'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<img src="@image_teaser1" alt="Get Spam Master Pro Key">', ['@image_teaser1' => $image_teaser1]),
        '#attributes' => [
          'class' => [
            'spammaster-responsive-49',
          ],
          '#allowed_tags' => ['img'],
        ],
        '#wrapper_attributes' => ['colspan' => 2],
      ];
      $image_teaser2 = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() . '/modules/spammaster/images/techgasp-statistics.jpg';
      $form['statistics_header']['header_description']['table']['images']['teaser2'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<img src="@image_teaser2" alt="Get Spam Master Pro Key">', ['@image_teaser2' => $image_teaser2]),
        '#attributes' => [
          'class' => [
            'spammaster-responsive-49',
          ],
          '#allowed_tags' => ['img'],
        ],
        '#wrapper_attributes' => ['colspan' => 2],
      ];

      $form['statistics_header']['header_description']['table_1'] = [
        '#type' => 'table',
        '#attributes' => [
          'class' => [
            'spammaster-responsive',
          ],
        ],
      ];
      $form['statistics_header']['header_description']['table_1']['text']['teaser1'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<img src="@image_check" alt="Get Pro Key">', ['@image_check' => $image_check]) . $this->t('&nbsp;All protection features, no extra modules'),
        '#allowed_tags' => ['img'],
      ];
      $form['statistics_header']['header_description']['table_1']['text']['teaser2'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<img src="@image_check" alt="Get Pro Key">', ['@image_check' => $image_check]) . $this->t('&nbsp;Connection to Business RBL Server Cluster'),
        '#allowed_tags' => ['img'],
      ];
      $form['statistics_header']['header_description']['table_1']['text']['teaser3'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<img src="@image_check" alt="Get Pro Key">', ['@image_check' => $image_check]) . $this->t('&nbsp;For heavy duty websites and e-commerce'),
        '#allowed_tags' => ['img'],
      ];
      $form['statistics_header']['header_description']['table_1']['text']['teaser4'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<img src="@image_check" alt="Get Pro Key">', ['@image_check' => $image_check]) . $this->t('&nbsp;Real-Time Firewall Management'),
        '#allowed_tags' => ['img'],
      ];
      $form['statistics_header']['header_description']['table_1']['text1']['teaser1'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<img src="@image_check" alt="Get Pro Key">', ['@image_check' => $image_check]) . $this->t('&nbsp;Central Management for all websites'),
        '#allowed_tags' => ['img'],
      ];
      $form['statistics_header']['header_description']['table_1']['text1']['teaser2'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<img src="@image_check" alt="Get Pro Key">', ['@image_check' => $image_check]) . $this->t('&nbsp;Private, ticketed email support'),
        '#allowed_tags' => ['img'],
      ];
      $form['statistics_header']['header_description']['table_1']['text1']['teaser3'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<img src="@image_check" alt="Get Pro Key">', ['@image_check' => $image_check]) . $this->t('&nbsp;1 year PRO key with full protection'),
        '#allowed_tags' => ['img'],
      ];
      $form['statistics_header']['header_description']['table_1']['text1']['teaser4'] = [
        '#type' => 'markup',
        '#markup' => $this->t('&nbsp;<a class="@spammaster_class" href="@spammaster_url" target="@spammaster_target"><img src="@image_check" alt="Get Pro Key"> Upgrade to PRO</a>', [
          '@spammaster_url' => 'https://www.techgasp.com/downloads/spam-master-license/',
          '@spammaster_target' => '_blank',
          '@spammaster_class' => 'button button--primary',
          '@image_check' => $image_check,
        ]),
        '#allowed_tags' => ['span', 'img', 'a'],
      ];
    }

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'spammaster.settings_log',
    ];
  }

}
