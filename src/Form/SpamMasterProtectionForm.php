<?php

namespace Drupal\spammaster\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Class controller.
 */
class SpamMasterProtectionForm extends ConfigFormBase {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, MessengerInterface $messenger, StateInterface $state) {
    $this->connection = $connection;
    $this->messenger = $messenger;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('messenger'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spammaster_settings_protection_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Default settings.
    $settings = $this->config('spammaster.settings');
    $config = $this->config('spammaster.settings_protection');
    $license_type = $this->state->get('spammaster.type');

    $form['protection_header'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('<h3>Protection Tools</h3>'),
      '#attached' => [
        'library' => [
          'spammaster/spammaster-styles',
        ],
      ],
    ];

    $form['message'] = [
      '#type' => 'details',
      '#title' => $this->t('Block Message'),
      '#group' => 'protection_header',
    ];

    // Insert license key field.
    $form['message']['block_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Change block message:'),
      '#default_value' => $config->get('spammaster.block_message'),
      '#description' => $this->t('Message to display to blocked spam users who are not allowed to register, contact, or comment in your Drupal site. Please keep it short.'),
      '#attributes' => [
        'class' => [
          'spammaster-responsive',
        ],
      ],
    ];

    // Insert basic tools table inside tree.
    $form['basic'] = [
      '#type' => 'details',
      '#title' => $this->t('Basic Tools'),
      '#group' => 'protection_header',
      '#attributes' => [
        'class' => [
          'spammaster-responsive-25',
        ],
      ],
    ];

    $form['basic']['table_1'] = [
      '#type' => 'table',
      '#header' => [
          [
            'data' => $this->t('Activate individual Basic Tools to implement Spam Master across your site.'),
            'colspan' => 4,
          ],
      ],
    ];
    $form['basic']['table_1']['addrow']['basic_firewall'] = [
      '#type' => 'select',
      '#title' => $this->t('HAF Firewall Scan'),
      '#options' => [
        1 => $this->t('Yes'),
      ],
      '#default_value' => $config->get('spammaster.basic_firewall'),
      '#description' => $this->t('High Availability Firewall aka HAF always set <em>Yes</em>. Firewall scan implemented across your site frontend. Greatly reduces server resources like CPU and Memory.'),
      '#attributes' => [
        'class' => [
          'spammaster-responsive-49',
        ],
      ],
    ];
    if ('FULL' !== $license_type) {
      $basic_firewall_rules_set = $this->state->get('spammaster.basic_firewall_rules_set');
      if ('1' === $basic_firewall_rules_set) {
        $set_text = $this->t('You may only change scan level value with a <strong>PRO</strong> connection key. Please visit Settings tab.') . ' ';
      }
      else {
        $set_text = FALSE;
      }
    }
    else {
      $set_text = FALSE;
    }
    $form['basic']['table_1']['addrow']['basic_firewall_rules'] = [
      '#type' => 'select',
      '#title' => $this->t('HAF Firewall Rules'),
      '#options' => [
        1 => $this->t('Normal'),
        2 => $this->t('Relaxed'),
        3 => $this->t('Super Relaxed'),
      ],
      '#default_value' => $config->get('spammaster.basic_firewall_rules'),
      '#description' => $set_text . $this->t('HAF Firewall scan level. <strong><em>Normal</em></strong>, active strict firewall stance for high levels of spam. <strong><em>Relaxed</em></strong>, active firewall stance for large corporate websites or local, state and federal government agencies. <strong><em>Super Relaxed</em></strong>, passive firewall stance with low footprint and for low spam levels.'),
      '#attributes' => [
        'class' => [
          'spammaster-responsive-49',
        ],
      ],
    ];
    // Insert extra tools table inside tree.
    $form['extra'] = [
      '#type' => 'details',
      '#title' => $this->t('Extra Tools'),
      '#group' => 'protection_header',
      '#attributes' => [
        'class' => [
          'spammaster-responsive-25',
        ],
      ],
    ];
    // Insert extra tools honeypot table.
    $form['extra']['table_2'] = [
      '#type' => 'table',
      '#header' => [
          ['data' => $this->t('Honeypot')],
      ],
    ];
    $form['extra']['table_2']['addrow']['extra_honeypot'] = [
      '#type' => 'select',
      '#title' => $this->t('Honeypot'),
      '#options' => [
        1 => $this->t('Yes'),
      ],
      '#default_value' => $config->get('spammaster.extra_honeypot'),
      '#description' => $this->t('Implements two Honeypot fields across your site frontend forms. Always set <em>Yes</em> since it does not affect human interaction or server resources.'),
    ];
    // Insert extra tools cloudflare table.
    $form['extra']['table_3'] = [
      '#type' => 'table',
      '#header' => [
          ['data' => $this->t('CDN and WAF'), 'colspan' => 1],
      ],
    ];
    $spam_master_is_cloudflare = $config->get('spammaster.spam_master_is_cloudflare');
    if (empty($spam_master_is_cloudflare) || NULL === $spam_master_is_cloudflare) {
      $spam_master_is_cloudflare = '0';
    }
    if ('FULL' === $license_type) {
      $form['extra']['table_3']['addrow']['spam_master_is_cloudflare'] = [
        '#type' => 'select',
        '#title' => $this->t('Activate for CDN and WAF integration'),
        '#options' => [
          0 => $this->t('No'),
          1 => $this->t('Yes'),
        ],
        '#default_value' => $spam_master_is_cloudflare,
        '#description' => $this->t('If you are using Cloudflare or Fastly this setting is usually set to <em>Yes</em>. Before activating please read the online documentation.'),
      ];
    }
    else {
      $form['extra']['table_3']['addrow']['spam_master_is_cloudflare'] = [
        '#disabled' => TRUE,
        '#type' => 'select',
        '#title' => $this->t('Activate for CDN and WAF integration'),
        '#options' => [
          0 => $this->t('No'),
        ],
        '#default_value' => $spam_master_is_cloudflare,
        '#description' => $this->t('If you are using Cloudflare or Fastly this setting is usually set to <em>Yes</em>. Before activating please read the online documentation.'),
      ];
      // Insert spam table inside tree.
      $form['extra']['table_3']['help'] = [
        '#type' => 'table',
      ];
      $form['extra']['table_3']['help']['addrow']['help_text'] = [
        '#type' => 'item',
        '#title' => $this->t('You may only change cloudflare value with a PRO connection key. Please visit Settings tab.'),
      ];
    }

    // Insert signatures tools table inside tree.
    $form['signature'] = [
      '#type' => 'details',
      '#title' => $this->t('Signatures'),
      '#group' => 'protection_header',
      '#attributes' => [
        'class' => [
          'spammaster-responsive-25',
        ],
      ],
    ];
    if ('FULL' === $license_type) {
      $form['signature']['table_4'] = [
        '#type' => 'table',
        '#header' => [
            [
              'data' => $this->t('Protected by Spam Master signatures are a huge deterrent against all forms of human spam.'),
              'colspan' => 4,
            ],
        ],
      ];
      $form['signature']['table_4']['addrow']['signature'] = [
        '#type' => 'select',
        '#title' => $this->t('Signature'),
        '#options' => [
          0 => $this->t('No'),
          1 => $this->t('Yes'),
        ],
        '#default_value' => $config->get('spammaster.signature'),
        '#description' => $this->t('Set this to <em>Yes</em> if you would like a Protection Signature to be displayed on frontend forms.'),
      ];
      $form['signature']['help']['addrow']['help_text'] = [
        '#type' => 'item',
        '#title' => $this->t('If you find a misplaced signature please <a href="@mailto?subject=@mailsub&body=@mailbody">email us</a> for a speedy fix.', [
          '@mailto' => 'mailto:info@spammaster.org',
          '@mailsub' => 'Module support misplaced signature',
          '@mailbody' => $settings->get('spammaster.license_key') . ' *** WRITE BELOW THIS LINE AND INSERT URL(s) OF MISPLACED SIGNATURE PAGES ***',
        ]),
      ];
    }
    else {
      $form['signature']['table_4'] = [
        '#type' => 'table',
        '#header' => [
            [
              'data' => $this->t('Protected by Spam Master signatures are a huge deterrent against all forms of human spam.'),
              'colspan' => 4,
            ],
        ],
      ];
      $form['signature']['table_4']['addrow']['signature'] = [
        '#disabled' => TRUE,
        '#type' => 'select',
        '#title' => $this->t('Signature'),
        '#options' => [
          0 => $this->t('No'),
        ],
        '#default_value' => $config->get('spammaster.signature'),
        '#description' => $this->t('Set this to <em>Yes</em> if you would like a Protection Signature to be displayed on frontend forms.'),
      ];
      // Insert spam table inside tree.
      $form['signature']['help'] = [
        '#type' => 'table',
      ];
      $form['signature']['help']['addrow']['help_text'] = [
        '#type' => 'item',
        '#title' => $this->t('You may only change these values with a PRO connection key, please visit the Settings tab. If you find a misplaced signature please <a href="@mailto?subject=@mailsub&body=@mailbody">email us</a> for a speedy fix.', [
          '@mailto' => 'mailto:info@spammaster.org',
          '@mailsub' => 'Module support misplaced signature',
          '@mailbody' => $settings->get('spammaster.license_key') . ' *** WRITE BELOW THIS LINE AND INSERT URL(s) OF MISPLACED SIGNATURE PAGES ***',
        ]),
      ];
    }
    // Insert email tools table inside tree.
    $form['email'] = [
      '#type' => 'details',
      '#title' => $this->t('Emails & Reports'),
      '#group' => 'protection_header',
      '#attributes' => [
        'class' => [
          'spammaster-responsive-25',
        ],
      ],
    ];
    $form['email']['table_5'] = [
      '#type' => 'table',
      '#header' => [
        [
          'data' => $this->t('An extra watchful eye over your drupal website security. Emails and reports are sent to the email address found in your drupal Configuration, Basic Site Settings.'),
          'colspan' => 4,
        ],
      ],
    ];
    $form['email']['table_5']['addrow']['email_alert_3'] = [
      '#type' => 'select',
      '#title' => $this->t('Alert 3 Warning Email'),
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
      '#default_value' => $config->get('spammaster.email_alert_3'),
      '#description' => $this->t('Set this to <em>Yes</em> to receive the alert 3 email. Only sent if your website has reached or is at a dangerous level.'),
    ];
    $form['email']['table_5']['addrow']['email_daily_report'] = [
      '#type' => 'select',
      '#title' => $this->t('Daily Report Email'),
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
      '#default_value' => $config->get('spammaster.email_daily_report'),
      '#description' => $this->t('Set this to <em>Yes</em> to receive the daily report for normal alert levels and spam probability percentage.'),
    ];
    $form['email']['table_5']['addrow']['email_weekly_report'] = [
      '#type' => 'select',
      '#title' => $this->t('Weekly Report Email'),
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
      '#default_value' => $config->get('spammaster.email_weekly_report'),
      '#description' => $this->t('Set this to <em>Yes</em> to receive the Weekly detailed email report.'),
    ];
    $form['email']['table_5']['addrow']['email_improve'] = [
      '#type' => 'select',
      '#title' => $this->t('Help us improve Spam Master'),
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
      '#default_value' => $config->get('spammaster.email_improve'),
      '#description' => $this->t('Set this to <em>Yes</em> to help us improve Spam Master with weekly statistical data, same as your weekly report.'),
    ];

    // Insert clean-up tools table inside tree.
    $form['cleanup'] = [
      '#type' => 'details',
      '#title' => $this->t('Clean-Up'),
      '#group' => 'protection_header',
      '#attributes' => [
        'class' => [
          'spammaster-responsive-25',
        ],
      ],
    ];
    $form['cleanup']['table_6'] = [
      '#type' => 'table',
      '#header' => [
        [
          'data' => $this->t('Clean-up allows to automatically delete logs via weekly cron but impacts statistics. Insert number of days, default is 15, minimum 1 and maximum 365.'),
          'colspan' => 5,
        ],
      ],
    ];
    $form['cleanup']['table_6']['addrow']['cleanup_system'] = [
      '#type' => 'number',
      '#title' => $this->t('System Logs:'),
      '#default_value' => $config->get('spammaster.cleanup_system'),
      '#description' => $this->t('Insert retention time in days (minimum 1, maximum 365).'),
      '#attributes' => [
        'class' => [
          'spammaster-responsive',
        ],
      ],
      '#required' => TRUE,
      '#maxlength' => 3,
    ];
    $form['cleanup']['table_6']['addrow']['cleanup_cron'] = [
      '#type' => 'number',
      '#title' => $this->t('Cron Logs:'),
      '#default_value' => $config->get('spammaster.cleanup_cron'),
      '#description' => $this->t('Insert retention time in days (minimum 1, maximum 365).'),
      '#attributes' => [
        'class' => [
          'spammaster-responsive',
        ],
      ],
      '#required' => TRUE,
      '#maxlength' => 3,
    ];
    $form['cleanup']['table_6']['addrow']['cleanup_mail'] = [
      '#type' => 'number',
      '#title' => $this->t('Mail Logs:'),
      '#default_value' => $config->get('spammaster.cleanup_mail'),
      '#description' => $this->t('Insert retention time in days (minimum 1, maximum 365).'),
      '#attributes' => [
        'class' => [
          'spammaster-responsive',
        ],
      ],
      '#required' => TRUE,
      '#maxlength' => 3,
    ];
    $form['cleanup']['table_6']['addrow1']['cleanup_whitelist'] = [
      '#type' => 'number',
      '#title' => $this->t('Whitelist Logs:'),
      '#default_value' => $config->get('spammaster.cleanup_whitelist'),
      '#description' => $this->t('Insert retention time in days (minimum 1, maximum 365).'),
      '#attributes' => [
        'class' => [
          'spammaster-responsive',
        ],
      ],
      '#required' => TRUE,
      '#maxlength' => 3,
    ];
    $form['cleanup']['table_6']['addrow1']['cleanup_firewall'] = [
      '#type' => 'number',
      '#title' => $this->t('Firewall Logs:'),
      '#default_value' => $config->get('spammaster.cleanup_firewall'),
      '#description' => $this->t('Insert retention time in days (minimum 1, maximum 365).'),
      '#attributes' => [
        'class' => [
          'spammaster-responsive',
        ],
      ],
      '#required' => TRUE,
      '#maxlength' => 3,
    ];
    $form['cleanup']['table_6']['addrow1']['cleanup_honeypot'] = [
      '#type' => 'number',
      '#title' => $this->t('Honeypot Logs:'),
      '#default_value' => $config->get('spammaster.cleanup_honeypot'),
      '#description' => $this->t('Insert retention time in days (minimum 1, maximum 365).'),
      '#attributes' => [
        'class' => [
          'spammaster-responsive',
        ],
      ],
      '#required' => TRUE,
      '#maxlength' => 3,
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('block_message'))) {
      $form_state->setErrorByName('protection_header', $this->t('Field can not be empty. Please insert your block message.'));
      // Log message.
      $spammaster_date = date("Y-m-d H:i:s");
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spammaster_date,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Protection Tools Validation Failed for block message. Reason: Empty.',
      ])->execute();
    }
    if (empty($form_state->getValue('table_6')['addrow']['cleanup_system']) || $form_state->getValue('table_6')['addrow']['cleanup_system'] <= '0' && $form_state->getValue('table_6')['addrow']['cleanup_system'] >= '366') {
      $form_state->setErrorByName('protection_header', $this->t('System Logs field can not be empty and requires a value between 1 and 365.'));
      // Log message.
      $spammaster_date = date("Y-m-d H:i:s");
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spammaster_date,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Protection Tools Validation Failed for System Logs. Reason: Either empty field, inferior to 1 or superior to 365.',
      ])->execute();
    }
    if (empty($form_state->getValue('table_6')['addrow']['cleanup_cron']) && $form_state->getValue('table_6')['addrow']['cleanup_cron'] <= '0' || $form_state->getValue('table_6')['addrow']['cleanup_cron'] >= '366') {
      $form_state->setErrorByName('protection_header', $this->t('Cron Logs field can not be empty and requires a value between 1 and 365.'));
      // Log message.
      $spammaster_date = date("Y-m-d H:i:s");
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spammaster_date,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Protection Tools Validation Failed for Cron Logs. Reason: Either empty field, inferior to 1 or superior to 365.',
      ])->execute();
    }
    if (empty($form_state->getValue('table_6')['addrow']['cleanup_mail']) && $form_state->getValue('table_6')['addrow']['cleanup_mail'] <= '0' || $form_state->getValue('table_6')['addrow']['cleanup_mail'] >= '366') {
      $form_state->setErrorByName('protection_header', $this->t('Mail Logs field can not be empty and requires a value between 1 and 365.'));
      // Log message.
      $spammaster_date = date("Y-m-d H:i:s");
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spammaster_date,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Protection Tools Validation Failed for Mail Logs. Reason: Either empty field, inferior to 1 or superior to 365.',
      ])->execute();
    }
    if (empty($form_state->getValue('table_6')['addrow1']['cleanup_whitelist']) && $form_state->getValue('table_6')['addrow1']['cleanup_whitelist'] <= '0' || $form_state->getValue('table_6')['addrow1']['cleanup_whitelist'] >= '366') {
      $form_state->setErrorByName('protection_header', $this->t('Whitelist Logs field can not be empty and requires a value between 1 and 365.'));
      // Log message.
      $spammaster_date = date("Y-m-d H:i:s");
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spammaster_date,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Protection Tools Validation Failed for Whitelist Logs. Reason: Either empty field, inferior to 1 or superior to 365.',
      ])->execute();
    }
    if (empty($form_state->getValue('table_6')['addrow1']['cleanup_firewall']) && $form_state->getValue('table_6')['addrow1']['cleanup_firewall'] <= '0' || $form_state->getValue('table_6')['addrow1']['cleanup_firewall'] >= '366') {
      $form_state->setErrorByName('protection_header', $this->t('Firewall Logs field can not be empty and requires a value between 1 and 365.'));
      // Log message.
      $spammaster_date = date("Y-m-d H:i:s");
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spammaster_date,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Protection Tools Validation Failed for Firewall Logs. Reason: Either empty field, inferior to 1 or superior to 365.',
      ])->execute();
    }
    if (empty($form_state->getValue('table_6')['addrow1']['cleanup_honeypot']) && $form_state->getValue('table_6')['addrow1']['cleanup_honeypot'] <= '0' || $form_state->getValue('table_6')['addrow1']['cleanup_honeypot'] >= '366') {
      $form_state->setErrorByName('protection_header', $this->t('Honeypot Logs field can not be empty and requires a value between 1 and 365.'));
      // Log message.
      $spammaster_date = date("Y-m-d H:i:s");
      $this->connection->insert('spammaster_keys')->fields([
        'date' => $spammaster_date,
        'spamkey' => 'spammaster',
        'spamvalue' => 'Spam Master: Protection Tools Validation Failed for Honeypot Logs. Reason: Either empty field, inferior to 1 or superior to 365.',
      ])->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('spammaster.settings_protection');
    $license_type = $this->state->get('spammaster.type');
    $basic_firewall_rules_set = $this->state->get('spammaster.basic_firewall_rules_set');
    $config->set('spammaster.block_message', $form_state->getValue('block_message'));
    $config->set('spammaster.basic_firewall', '1');
    $config->set('spammaster.extra_honeypot', '1');
    if ('FULL' === $license_type) {
      $config->set('spammaster.basic_firewall_rules', $form_state->getValue('table_1')['addrow']['basic_firewall_rules']);
      $config->set('spammaster.signature', $form_state->getValue('table_4')['addrow']['signature']);
      $config->set('spammaster.spam_master_is_cloudflare', $form_state->getValue('table_3')['addrow']['spam_master_is_cloudflare']);
    }
    else {
      if ('1' === $basic_firewall_rules_set) {
        $config->set('spammaster.basic_firewall_rules', '3');
      }
      else {
        $config->set('spammaster.basic_firewall_rules', $form_state->getValue('table_1')['addrow']['basic_firewall_rules']);
      }
      $config->set('spammaster.signature', '1');
      $config->set('spammaster.spam_master_is_cloudflare', '0');
    }
    $config->set('spammaster.email_alert_3', $form_state->getValue('table_5')['addrow']['email_alert_3']);
    $config->set('spammaster.email_daily_report', $form_state->getValue('table_5')['addrow']['email_daily_report']);
    $config->set('spammaster.email_weekly_report', $form_state->getValue('table_5')['addrow']['email_weekly_report']);
    $config->set('spammaster.email_improve', $form_state->getValue('table_5')['addrow']['email_improve']);
    $config->set('spammaster.cleanup_system', $form_state->getValue('table_6')['addrow']['cleanup_system']);
    $config->set('spammaster.cleanup_cron', $form_state->getValue('table_6')['addrow']['cleanup_cron']);
    $config->set('spammaster.cleanup_mail', $form_state->getValue('table_6')['addrow']['cleanup_mail']);
    $config->set('spammaster.cleanup_whitelist', $form_state->getValue('table_6')['addrow1']['cleanup_whitelist']);
    $config->set('spammaster.cleanup_firewall', $form_state->getValue('table_6')['addrow1']['cleanup_firewall']);
    $config->set('spammaster.cleanup_honeypot', $form_state->getValue('table_6')['addrow1']['cleanup_honeypot']);
    $config->save();
    // Log message.
    $spammaster_date = date("Y-m-d H:i:s");
    $this->connection->insert('spammaster_keys')->fields([
      'date' => $spammaster_date,
      'spamkey' => 'spammaster',
      'spamvalue' => 'Spam Master: Protection Tools page successful save.',
    ])->execute();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'spammaster.settings_protection',
    ];
  }

}
