<?php

namespace Drupal\spammaster;

use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class service.
 */
class SpamMasterKeyService {

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The SpamMasterLicService Service.
   *
   * @var \Drupal\spammaster\SpamMasterLicService
   */
  protected $licService;

  /**
   * The SpamMasterCleanUpService Service.
   *
   * @var \Drupal\spammaster\SpamMasterCleanUpService
   */
  protected $cleanService;

  /**
   * The SpamMasterKeyService service.
   *
   * @var \Drupal\spammmaster\SpamMasterKeyService
   */
  protected $spamKey;

  /**
   * The SpamMasterKeyService service.
   *
   * @var \Drupal\spammmaster\SpamMasterKeyService
   */
  protected $spamCron;

  /**
   * {@inheritdoc}
   */
  public function __construct(StateInterface $state, SpamMasterLicService $licService, SpamMasterCleanUpService $cleanService) {
    $this->state = $state;
    $this->licService = $licService;
    $this->cleanService = $cleanService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('spammaster.lic_service'),
      $container->get('spammaster.clean_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function spamMasterKeyLazy($spamKey, $spamCron) {
    $spammasterWhiteTransient = $this->state->get('spammaster.white_transient');
    // Call lic service.
    $spammaster_lic_service = $this->licService;
    $spammaster_lic_service->spamMasterLicDaily($spamKey, $spamCron);
    // Call clean-up service.
    $spammaster_cleanup_service = $this->cleanService;
    $spammaster_cleanup_service->spamMasterCleanUpTransients();
    $spammaster_cleanup_service->spamMasterCleanUpKeys();
    $spammaster_cleanup_service->spamMasterCleanUpBuffer();
    $spammaster_cleanup_service->spamMasterCleanUpLazy();
    if ('1' === $spammasterWhiteTransient) {
      $spammaster_cleanup_service->spamMasterCleanUpTransNow($spammasterWhiteTransient);
    }
  }

}
