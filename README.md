## Spam Master

The Spam Master real time Firewall and Protection module for drupal blocks
new user registrations, comments and threads using Real Time anti-spam
lists as Saas.

For a full description of the module visit:
https://www.drupal.org/project/spammaster

For a full information about Spam Master:
https://www.spammaster.org

### Required

- Drupal 8, 9 or 10
- Spam Master API Key/lic is required. Free or Pro.

### Required Modules

This module requires no modules outside of Drupal core.

### Installation/Setup

Require package/module:

```bash
composer require drupal/spammaster
```

Install and enable module via drush.

```bash
drush en spammaster
drush cr
```

Spam Master settings can be managed from here:
`admin/config/system/spammaster`

## Maintainers

Pedro Alves (pedro-alves)
https://www.drupal.org/u/pedro-alves
